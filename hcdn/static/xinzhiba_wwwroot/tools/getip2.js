public static string GetLoginIP(HttpRequestBase request)
        {
            string loginip = "";


            if (request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                //获取代理的服务器Ip地址  
                loginip = "HTTP_X_FORWARDED_FOR:" + request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (request.ServerVariables["REMOTE_ADDR"] != null) //判断发出请求的远程主机的ip地址是否为空  
            {
                //获取发出请求的远程主机的Ip地址  
                loginip = "REMOTE_ADDR:" + request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            else
            {
                //获取客户端IP  
                loginip = "request:" + request.UserHostAddress;
            }

            return loginip;
        }