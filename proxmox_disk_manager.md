



```
apt-get install lshw
```



```bash
root@pve:/var/lib/vz/template/iso# apt-get install lshw
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required:
  libboost-python1.67.0 python-blinker python-click python-colorama python-flask python-itsdangerous python-jinja2
Use 'apt autoremove' to remove them.
The following NEW packages will be installed:
  lshw
0 upgraded, 1 newly installed, 0 to remove and 73 not upgraded.
Need to get 254 kB of archives.
After this operation, 781 kB of additional disk space will be used.
Get:1 http://mirrors.bfsu.edu.cn/debian buster/main amd64 lshw amd64 02.18.85-0.1 [254 kB]
Fetched 254 kB in 2s (125 kB/s)
N: Ignoring file 'pve-enterprise.list_backup' in directory '/etc/apt/sources.list.d/' as it has an invalid filename extension
Selecting previously unselected package lshw.
(Reading database ... 48892 files and directories currently installed.)
Preparing to unpack .../lshw_02.18.85-0.1_amd64.deb ...
Unpacking lshw (02.18.85-0.1) ...
Setting up lshw (02.18.85-0.1) ...
Processing triggers for man-db (2.8.5-2) ...
root@pve:/var/lib/vz/template/iso# 
root@pve:/var/lib/vz/template/iso# lshw -C storage -C disk
  *-storage
       description: Non-Volatile memory controller
       product: Silicon Motion, Inc.
       vendor: Silicon Motion, Inc.
       physical id: 0
       bus info: pci@0000:02:00.0
       version: 03
       width: 64 bits
       clock: 33MHz
       capabilities: storage pm msi pciexpress msix nvm_express bus_master cap_list
       configuration: driver=nvme latency=0
       resources: irq:34 memory:fbe00000-fbe03fff
  *-sata
       description: SATA controller
       product: 7 Series/C210 Series Chipset Family 6-port SATA Controller [AHCI mode]
       vendor: Intel Corporation
       physical id: 1f.2
       bus info: pci@0000:00:1f.2
       logical name: scsi0
       logical name: scsi1
       logical name: scsi5
       version: 04
       width: 32 bits
       clock: 66MHz
       capabilities: sata msi pm ahci_1.0 bus_master cap_list emulated
       configuration: driver=ahci latency=0
       resources: irq:52 ioport:f070(size=8) ioport:f060(size=4) ioport:f050(size=8) ioport:f040(size=4) ioport:f020(size=32) memory:fbf05000-fbf057ff
     *-disk:0
          description: ATA Disk
          product: WDC WD5000AAKX-0
          vendor: Western Digital
          physical id: 0
          bus info: scsi@0:0.0.0
          logical name: /dev/sda
          version: 1H15
          serial: WD-WCAYUH260736
          size: 465GiB (500GB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: ansiversion=5 guid=9675ba85-1776-4675-91dc-e188ab21cecb logicalsectorsize=512 sectorsize=512
     *-disk:1
          description: ATA Disk
          product: ST3000VX010-2H91
          physical id: 1
          bus info: scsi@1:0.0.0
          logical name: /dev/sdb
          version: CV11
          serial: ZDH749QY
          size: 2794GiB (3TB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: ansiversion=5 guid=cae9ba8a-e668-4e43-af28-d8f18251d132 logicalsectorsize=512 sectorsize=4096
     *-disk:2
          description: ATA Disk
          product: HGST HTS721010A9
          physical id: 0.0.0
          bus info: scsi@5:0.0.0
          logical name: /dev/sdc
          version: A3J0
          serial: JR10046P2W6J5N
          size: 931GiB (1TB)
          capabilities: gpt-1.00 partitioned partitioned:gpt
          configuration: ansiversion=5 guid=63da44ec-70e2-4781-a400-f95a46cb7673 logicalsectorsize=512 sectorsize=4096
root@pve:/var/lib/vz/template/iso#

```

![image-20200814135951643](illustrations/image-20200814135951643.png)