 /*
	功能：生成博客目录的JS工具
	测试：IE8，火狐，google测试通过
	孤傲苍狼
	2014-5-11
	catsnyne@gmail.com 修改 2019-03-20 
			--->应用于word导出html后，添加此js与css
	
*/
var BlogDirectory = {
	/*
		获取元素位置，距浏览器左边界的距离（left）和距浏览器上边界的距离（top）
	*/
	getElementPosition:function (ele) {        
		var topPosition = 0;
		var leftPosition = 0;
		while (ele){              
			topPosition += ele.offsetTop;
			leftPosition += ele.offsetLeft;        
			ele = ele.offsetParent;     
		}  
		return {top:topPosition, left:leftPosition}; 
	},

	/*
	获取滚动条当前位置
	*/
	getScrollBarPosition:function () {
		var scrollBarPosition = document.body.scrollTop || document.documentElement.scrollTop;
		return  scrollBarPosition;
	},
	
	/*
	移动滚动条，finalPos 为目的位置，internal 为移动速度
	*/
	moveScrollBar:function(finalpos, interval) {

		//若不支持此方法，则退出
		if(!window.scrollTo) {
			return false;
		}

		//窗体滚动时，禁用鼠标滚轮
		window.onmousewheel = function(){
			return false;
		};
		  
		//清除计时
		if (document.body.movement) { 
			clearTimeout(document.body.movement); 
		} 

		var currentpos =BlogDirectory.getScrollBarPosition();//获取滚动条当前位置

		var dist = 0; 
		if (currentpos == finalpos) {//到达预定位置，则解禁鼠标滚轮，并退出
			window.onmousewheel = function(){
				return true;
			}
			return true; 
		} 
		if (currentpos < finalpos) {//未到达，则计算下一步所要移动的距离
			dist = Math.ceil((finalpos - currentpos)/10); 
			currentpos += dist; 
		} 
		if (currentpos > finalpos) { 
			dist = Math.ceil((currentpos - finalpos)/10); 
			currentpos -= dist; 
		}
		
		var scrTop = BlogDirectory.getScrollBarPosition();//获取滚动条当前位置
		window.scrollTo(0, currentpos);//移动窗口
		if(BlogDirectory.getScrollBarPosition() == scrTop)//若已到底部，则解禁鼠标滚轮，并退出
		{
			window.onmousewheel = function(){
				return true;
			}
			return true;
		}
		
		//进行下一步移动
		var repeat = "BlogDirectory.moveScrollBar(" + finalpos + "," + interval + ")"; 
		document.body.movement = setTimeout(repeat, interval); 
	},
	
	htmlDecode:function (text){
		var temp = document.createElement("div");
		temp.innerHTML = text;
		var output = temp.innerText || temp.textContent;
		temp = null;
		return output;
	},

	/*
	创建博客目录，
	id表示包含博文正文的 div 容器的 id，
	mt 和 st 分别表示主标题和次级标题的标签名称（如 H2、H3，大写或小写都可以！），
	interval 表示移动的速度
	*/
	createBlogDirectory:function (id, mt, st, de, interval){
		 //获取博文正文div容器
		var elem = document.getElementById(id);
		if(!elem) return false;
		//获取div中所有元素结点,下面循环引用 。
		var nodes = elem.getElementsByTagName("*");
             
		var dlist = document.getElementById("cataloglist");
		//将列表dt添加在内容div内。
		//divSideBarContents.appendChild(dlist);
		var num = 0;//统计找到的mt和st
		mt = mt.toUpperCase();//转化成大写
		st = st.toUpperCase();//转化成大写
		de = de.toUpperCase();//转化成大写
		//遍历所有元素结点
        var hh11=0;/* 一个是中间变量一个是递增变量*/
        var hh22=0;
        var hh33=0;
		
		document.getElementById("sideBar").style.display = 'none';
		for(var i=0 ; i < nodes.length; i++){
			if(nodes[i].nodeName == "H1" || nodes[i].nodeName == "H2" || nodes[i].nodeName == "H3" ){
				//console.log(i,nodes[i].nodeName,nodes[i].innerHTML,nodes[i].getAttribute("id"));
				var nodetext = nodes[i].innerHTML;
				var listnodetext = nodes[i].innerHTML;
				//nodetext = nodetext.replace(/&nbsp;/ig, "");//替换掉所有的&nbsp;
				nodetext = BlogDirectory.htmlDecode(nodetext);
                                ahrefnodetext = nodetext.replace(/(^\s+)|(\s+$)/g, '').replace(/^\-{1,}/g, ""); //去除字符号前后的空格,及开头结尾的字符。
				ahrefnodetext = ahrefnodetext.toLowerCase().replace(/ by/g,"_By"); //处理By介词在dokuwiki中为大写首字母的情况。
				ahrefnodetext = ahrefnodetext.replace(/\./g,"").replace(/\%/g,"").replace(/( |&)/g,"_").replace(/_{1,}/g,"_");//&、空格、连续下划线转单下划线。.去除
				//nodes[i].innerHTML = listnodetext;
				//ahrefnodetext = nodetext.toLowerCase().replace(/\.{1}[A-Za-z]{1,}$/g,"");//去除扩展名
				// var nodetext = nodes[i].innerHTML.replace(/<\/?[^>]+>/g,"");//innerHTML里面的内容可能有HTML标签，所以用正则表达式去除HTML的标签
				// nodetext = nodetext.replace(/&nbsp;/ig, "");//替换掉所有的&nbsp;
				// nodetext = BlogDirectory.htmlDecode(nodetext);
				//插入锚        
				//nodes[i].setAttribute("id", "blogTitle" + num);
                                
 				var nodeIdGet = nodes[i].getAttribute("id"); //将标题h的id获取作为下方传递。
                                // console.count("代码被执行了多少次了?")     
                                //遍历和创建dl、dt、de、dd到导航。
				var item;
				switch(nodes[i].nodeName){
				  case mt:
				              item = document.createElement("dt");
							  
                              hh11 = hh11 +1 ;
							  nodes[i].setAttribute('id',listnodetext);  //先将innerHTML赋给h标签的id
                              nodes[i].innerHTML = "§ " + hh11 + ". " + listnodetext;   // 再将innerHTML添加序号，两不误。
                              hh22 = 0 ; 
							  //console.log("case H1",i,nodes[i].nodeName,nodes[i].innerHTML,nodes[i].getAttribute("id"),"NNNN",ahrefnodetext);
							  break;
				  case st:
							  item = document.createElement("dd");
                              hh22 = hh22 +1 ;
							  nodes[i].setAttribute('id',listnodetext);
                              nodes[i].innerHTML = hh11 + "." + hh22 + ". " + listnodetext;
                              hh33 = 0; 
							  //console.log("case H2",i,nodes[i].nodeName,nodes[i].innerHTML,nodes[i].getAttribute("id"),"NNNN",ahrefnodetext);
							  break;
				  case de:
				              item = document.createElement("de");
                                                hh33 = hh33 + 1 ;
                                                if (hh33 >= 1 ) {
                                                   if ( hh11 == 0 ){
                                                           hh11 = hh11 + 1;
                                                   }
                                                   if ( hh22 == 0 ){
                                                           hh22 = hh22 + 1;
                                                   }
							                        nodes[i].setAttribute('id',listnodetext);
                                                    nodes[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
                                                }
							  //console.log("case H3",i,nodes[i].nodeName,nodes[i].innerHTML,nodes[i].getAttribute("id"),"NNNN",ahrefnodetext);
							  break;



			 } //end swith
			 
             var ahreftextnode = document.createElement('a');
             //var ahreftextnode= item.appendChild(a)
             ahreftextnode.setAttribute('href', "#"+nodes[i].getAttribute("id"));
             ahreftextnode.innerHTML=nodes[i].innerHTML;
             //ahreftextnode.innerHTML=nodetext;
             item.appendChild(ahreftextnode);

             dlist.appendChild(item);
             num++;//for 中 if end
			} 
		}
        // console.timeEnd("开始时间点");
		//添加目录列表结束标记而矣(位于for循环之外 )
		var enddd = document.createElement("de");
		var enddiv = document.createElement("div");
		var dddivcontents = document.createTextNode('-END-');
		enddiv.appendChild(dddivcontents);
		enddd.appendChild(enddiv);
		dlist.appendChild(enddd);
		if(hh11 >= 1 || hh22 >=1 || hh33 >= 1){document.getElementById("sideBar").style.display = 'block';}
		if(num == 0) return false; 
		
		document.getElementById("sideBarTab").onmouseenter = function(){
			document.getElementById("sideBarContents").style.display = 'block';
			
			document.getElementById("sideBar").style.opacity= 1;
		}
		document.getElementById("sideBarContents").onmouseenter = function(){
			document.getElementById("sideBarContents").style.display = 'block';
			
			document.getElementById("sideBar").style.opacity= 1;
		}
		
		///鼠标离开时的事件
		document.getElementById("sideBar").onmouseleave = function() {
			//sideBarContents.style.display = 'none';
			document.getElementById("sideBar").style.opacity= 0.6;
		}
		var mmaincontent = document.getElementById('maincontent');
		//单击标题，隐藏目录列表。
		mmaincontent.onclick=function() {
			document.getElementById("sideBarContents").style.display = 'none';
			//divSideBarContents.style.display= none;
		}
			
	}
	
};


function getScrollTop() {
    var scroll_top = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scroll_top = document.documentElement.scrollTop;
    }
    else if (document.body) {
        scroll_top = document.body.scrollTop;
    }
    return scroll_top;
}
window.onload=function(){
	/*页面加载完成之后生成博客目录，此处设置调用的标签*/
 
	BlogDirectory.createBlogDirectory("maincontent","h1","h2","h3",20);

}

