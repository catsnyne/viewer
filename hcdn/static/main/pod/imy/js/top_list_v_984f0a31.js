////--/-
dayjs().format();
/* 生成页面顶部的随机与最近文章列表 */
fetch('/main/pod/imy/data/json_api_links_all.json').then(function(response) {
  return response.json().then(function(json) {

  /*追加名言部分，可以删除 begin */
  function sleep (time) {
      return new Promise((resolve) => setTimeout(resolve, time));
  }
  (async function() {
      /*因为json变量后面有随机化处理，此异步逻辑再次引用json变量时其值会化,不再是原始值。简单的赋值没用。但slice方法可实现固化变量的效果。*/
      var json_mingyan = json.slice();
      //Object.freeze(json_mingyan); /*freeze方法会固化，并且后面的sort也无法执行，故不能使用。*/
      const  item = document.querySelectorAll('div.notice-item');
      const  length = item.length;
      for (var i = 0; i < length; i++) {
      var temp = item[i];
          //console.log(temp);
          //console.log(document.querySelector("#scroll_mingyan").childNodes[0]);
          list_latest_publish =  '<div title="效果实现于2022/08/19" class="notice-item"><i class="icon-edit"></i><a class="notice-content" target="_blank" href="' + json_mingyan[i].link_url + '">' + item[i].querySelector('a').innerText + '</a><a class="notice-link text-primary" target="_blank" href=""> </a>     </div>';
          //document.querySelector("#scroll_mingyan").replaceChild(temp,document.querySelector("#scroll_mingyan").childNodes[0]);
          document.querySelector("#scroll_mingyan2").innerHTML=list_latest_publish;
          await sleep(5000);
          if(i===( length -1)){
             i = 0;
          }
      }
  })();
  /*追加名言部分，可以删除 end*/
  //------------------------------------------------------------------------------- 
    /*正常的主文件列表 */
      var list_latest_publish = '';
      for(j=0;j<8;j++){
         var date = new Date(json[j].link_time);
         list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
         //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + (date.getMonth()+1) + '/' +date.getDate()  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
      }
      document.getElementById("latest_publish").innerHTML=list_latest_publish;

      var list_random = '';
      /*随机化处理*/
      json.sort(function () { return 0.5 - Math.random() ;});
      for(j=0;j<8;j++){
         list_random = list_random + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random").innerHTML=list_random;

      var list_random2 = '';
      for(j=8;j<16;j++){
         list_random2 = list_random2 + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random2").innerHTML=list_random2;


  });
});

/* ip2region 显示客户端ip归属地  */
var realip = '';
fetch('/geoip2').then(function(response) {
  return response.json().then(function(json) {
      var realip = json.realip;
      document.getElementById("dropdown-menu-6").innerText=realip;

  }).then(function(){
  var realip = document.getElementById("dropdown-menu-6").innerText;
  fetch('/main/pod/pages/ip2region/ip2region?ip='+ realip ).then(function(response){
      return response.json().then(function(json){
         //document.getElementById("dropdown-menu-6").innerText=json.ip + json.region;
         document.getElementById("dropdown-menu-6").innerHTML='<a href="//haudi.top/geoip2" target="_blank">' + json.ip + json.region + '</a>';
      });
  });
 });
});
