 /*
	功能：生成博客目录的JS工具
	测试：IE8，火狐，google测试通过
	孤傲苍狼
	2014-5-11
	catsnyne@gmail.com 修改 2019-03-20 
			--->应用于word导出html后，添加此js与css
	
*/
var BlogDirectory = {
	/*
		获取元素位置，距浏览器左边界的距离（left）和距浏览器上边界的距离（top）
	*/
	getElementPosition:function (ele) {        
		var topPosition = 0;
		var leftPosition = 0;
		while (ele){              
			topPosition += ele.offsetTop;
			leftPosition += ele.offsetLeft;        
			ele = ele.offsetParent;     
		}  
		return {top:topPosition, left:leftPosition}; 
	},

	/*
	获取滚动条当前位置
	*/
	getScrollBarPosition:function () {
		var scrollBarPosition = document.body.scrollTop || document.documentElement.scrollTop;
		return  scrollBarPosition;
	},
	
	/*
	移动滚动条，finalPos 为目的位置，internal 为移动速度
	*/
	moveScrollBar:function(finalpos, interval) {

		//若不支持此方法，则退出
		if(!window.scrollTo) {
			return false;
		}

		//窗体滚动时，禁用鼠标滚轮
		window.onmousewheel = function(){
			return false;
		};
		  
		//清除计时
		if (document.body.movement) { 
			clearTimeout(document.body.movement); 
		} 

		var currentpos =BlogDirectory.getScrollBarPosition();//获取滚动条当前位置

		var dist = 0; 
		if (currentpos == finalpos) {//到达预定位置，则解禁鼠标滚轮，并退出
			window.onmousewheel = function(){
				return true;
			}
			return true; 
		} 
		if (currentpos < finalpos) {//未到达，则计算下一步所要移动的距离
			dist = Math.ceil((finalpos - currentpos)/10); 
			currentpos += dist; 
		} 
		if (currentpos > finalpos) { 
			dist = Math.ceil((currentpos - finalpos)/10); 
			currentpos -= dist; 
		}
		
		var scrTop = BlogDirectory.getScrollBarPosition();//获取滚动条当前位置
		window.scrollTo(0, currentpos);//移动窗口
		if(BlogDirectory.getScrollBarPosition() == scrTop)//若已到底部，则解禁鼠标滚轮，并退出
		{
			window.onmousewheel = function(){
				return true;
			}
			return true;
		}
		
		//进行下一步移动
		var repeat = "BlogDirectory.moveScrollBar(" + finalpos + "," + interval + ")"; 
		document.body.movement = setTimeout(repeat, interval); 
	},
	
	htmlDecode:function (text){
		var temp = document.createElement("div");
		temp.innerHTML = text;
		var output = temp.innerText || temp.textContent;
		temp = null;
		return output;
	},

	/*
	创建博客目录，
	id表示包含博文正文的 div 容器的 id，
	mt 和 st 分别表示主标题和次级标题的标签名称（如 H2、H3，大写或小写都可以！），
	interval 表示移动的速度
	*/
	createBlogDirectory:function (id, mt, st, de, interval){
		 //获取博文正文div容器
		var elem = document.getElementById(id);
		if(!elem) return false;
		//获取div中所有元素结点,下面循环引用 。
		var nodes = elem.getElementsByTagName("*");

		var dlist = document.getElementById("sideBar");
		var dlist_left = document.getElementById("tab-summary");
		//将列表dt添加在内容div内。
		//divSideBarContents.appendChild(dlist);
		var num = 0;//统计找到的mt和st
		mt = mt.toUpperCase();//转化成大写
		st = st.toUpperCase();//转化成大写
		de = de.toUpperCase();//转化成大写
		//遍历所有元素结点
                var hh11=0;/* 一个是中间变量一个是递增变量*/
                var hh22=0;
                var hh33=0;
                // console.time("开始时间点");
                var reg1 = new RegExp("^[\(-\){1,}|\(-\){1,}$]");
                // var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
		for(var i=0; i<nodes.length; i++)
		{
			if( (nodes[i].nodeName == mt|| nodes[i].nodeName == st|| nodes[i].nodeName == de)){
                  // ||（逻辑或） && 与
			// if(!(isDOMContains(document.getElementById("page-wrapper"),nodes[i],document.getElementById("wrapper"))) && (nodes[i].nodeName == mt|| nodes[i].nodeName == st|| nodes[i].nodeName == de))    
                        //跳过page-wrapper节点下的h标题进入检测。函数引用，说明见函数。
			
				//获取标题文本,提取用作href锚点跳转。
				var nodetext = nodes[i].innerHTML;
				////console.log(nodes[i]);
				////console.log(nodetext);
				var node_id = nodetext ;
				// var node_id = nodes[i].attributes["id"].value
				var listnodetext = nodes[i].innerHTML;

				nodes[i].id="hx" + num;  
				//console.log("before:" + nodes[i].previousElementSibling);

				//var newItem=document.createElement("a");
				//var textnode=document.createTextNode("Water");
				//newItem.appendChild(textnode);
				//var list=nodes[i].parentNode;
				//console.log(nodes[i].parentNode);
				//console.log(nodes[i].id);
				//list.insertBefore(newItem,document.getElementById("hx" + num));
				//// list.insertBefore(newItem,list.childNodes[0]);
				//var hx_a = nodes[i].previousElementSibling;
				// hx_a.id= "hx_a" + i; 
				//console.log(nodes[i].getAttribute("id"));
 				var nodeIdGet = nodes[i].getAttribute("id"); //将标题h的id获取作为下方传递。
                                // console.count("代码被执行了多少次了?")     
                                //遍历和创建dl、dt、de、dd到导航。
				var item;

                //console.log(nodes[i].nodeName);
				switch(nodes[i].nodeName)
				{
					case mt:    //若为主标题 
						item = document.createElement("dt");
                                                hh11 = hh11 +1 ;
                                                nodes[i].innerHTML = "§ " + hh11 + ". " + listnodetext;
                                                hh22 = 0 ; 
						break;
					case st:    //若为子标题
						item = document.createElement("dd");
                                                hh22 = hh22 +1 ;
                                                nodes[i].innerHTML = hh11 + "." + hh22 + ". " + listnodetext;
                                                hh33 = 0; 
						break;
					case de:    //若为子标题
						item = document.createElement("de");
                                                hh33 = hh33 + 1 ;
                                                if (hh33 >= 1 ) {
                                                   if ( hh11 == 0 ){
                                                           hh11 = hh11 + 1;
                                                   }
                                                   if ( hh22 == 0 ){
                                                           hh22 = hh22 + 1;
                                                   }
                                                    nodes[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
                                                }
                                                //hh33 = hh33 + 1 ;
						break;
				}




				
				//创建锚链接
                                //var ahreftextnode="<a >目录<div><a href='/1p'>返回首页</a></div>移入显示-移出淡化<br/>点击正文区域隐藏目录<br/></h2>";
                                var ahreftextnode = document.createElement('a');
                                //var ahreftextnode= item.appendChild(a)
                                ahreftextnode.setAttribute('href', "#"+nodeIdGet);
                                ahreftextnode.innerHTML=nodes[i].innerHTML;
								//ahreftextnode.innerHTML=nodetext;
								////console.log(item);
                                item.appendChild(ahreftextnode);

			//	};
				//将自定义表项加入自定义列表中
				//dlist.appendChild(item);
				dlist_left.appendChild(item);
				 num++;
			}
		}
                // console.timeEnd("开始时间点");
		//添加目录列表结束标记而矣
		enddd = document.createElement("de");
		enddiv = document.createElement("div");
		dddivcontents = document.createTextNode('-END-');
		enddiv.appendChild(dddivcontents);
		enddd.appendChild(enddiv);
		dlist.appendChild(enddd);
		
		if(num == 0) return false; 
		/*鼠标进入时的事件处理*/
//                var page-wrapper_box=document.getElementById('page-wrapper');
                // 元素上边距离页面上边的距离
  //              page-wrapper_box.getBoundingClientRect().bottom           




		document.getElementById("sideBarTab").onmouseenter = function(){
			document.getElementById("sideBarContents").style.display = 'block';
			
			document.getElementById("sideBar").style.opacity= 1;
		}
		document.getElementById("sideBarContents").onmouseenter = function(){
			document.getElementById("sideBarContents").style.display = 'block';
			
			document.getElementById("sideBar").style.opacity= 1;
		}
		
		///鼠标离开时的事件
		document.getElementById("sideBar").onmouseleave = function() {
			//sideBarContents.style.display = 'none';
			document.getElementById("sideBar").style.opacity= 0.6;
		}
		var mmaincontent = document.getElementById('page-wrapper');
		//单击标题，隐藏目录列表。
		mmaincontent.onclick=function() {
			document.getElementById("sideBarContents").style.display = 'none';
			//divSideBarContents.style.display= none;
		}
	        //将html div 添加为正文的同级下一个div	
                //document.getElementById("dokuwiki__content").appendChild(divSideBar);
		//document.body.appendChild(sideBar);
	}
	
};



function page_wrapper_number() {
  var get_dw_toc = document.getElementById('page-wrapper');
  var page_wrapper_atag = get_dw_toc.getElementsByTagName("a");   
  var dw1=0;dw2=0;dw3=0;
  for(var i=0 ; i < page_wrapper_atag.length; i++) {     
      //console.log(page-wrapper_atag[i]);     
      var a_level = page_wrapper_atag[i].parentElement.parentElement.className;
      var a_level1 = "level1";
      var a_level2 = "level2";
      var a_level3 = "level3";
      //console.log(a_level);     
      switch(a_level){
          case a_level1:
                      dw1=dw1+1;
                      page_wrapper_atag[i].innerHTML="" + dw1 + ". " + page-wrapper_atag[i].innerHTML;
                      dw2=0;
                      break;
          case a_level2:
                      dw2=dw2+1;
                      page_wrapper_atag[i].innerHTML=dw1 + "." + dw2 + ". " + page-wrapper_atag[i].innerHTML;
                      dw3=0;
                      break;
          case a_level3:
                      dw3=dw3+1;
                      if(dw1 == 0){
                         dw1=dw1+1;
                      }
                      if(dw2 == 0){
                         dw2=dw2+1;
                      }
                      page_wrapper_atag[i].innerHTML=dw1 + "." + dw2 + "." + dw3 + ". " + page-wrapper_atag[i].innerHTML;
                      break;



     } 
      
  } 

}
/* 理解：
scrollTop为滚动条在Y轴上的滚动距离。
clientHeight为内容可视区域的高度。
scrollHeight为内容可视区域的高度加上溢出（滚动）的距离。
从这个三个属性的介绍就可以看出来，滚动条到底部的条件即为scrollTop + clientHeight == scrollHeight。 */
window.onscroll = function(){
  //var divSideBarContentsFn = document.getElementById(sideBarContents);
  //var page-wrapper_box=document.getElementById('page-wrapper');
  
   if(document.getElementById("page-wrapper")){
	   if ( document.getElementById('page-wrapper').getBoundingClientRect().bottom <= 60 ){
			// console.log(document.getElementById('page-wrapper').getBoundingClientRect().bottom + "bottom"); 
			//divSideBarContentsFn.style.display = 'block';
			document.getElementById('sideBar').style.display = 'block';
			//document.getElementById('page-wrapper').classList.add("__web-inspector-hide-shortcut__");
			//document.getElementById('page-wrapper').style.display = 'none';
			//document.getElementsByTagName("h1").setAttribute('style', 'color;green');
			// sideBar.style.opacity= 1;
	   }
	   else if ( getScrollTop() <= document.getElementById('wrapper').getBoundingClientRect().bottom ){ //100大概是位于滚动条最上端。
	   //else if ( document.getElementById('page-wrapper').getBoundingClientRect().bottom >= 60 ){ //100大概是位于滚动条最上端。
			document.getElementById('sideBar').style.display = 'none';
			//console.log("快没有page-wrapper了:bottom:" + document.getElementById('page-wrapper').getBoundingClientRect().bottom + ",," + "ScrollTop:" + getScrollTop());
			//document.getElementById('page-wrapper').style.display = 'block';
			document.getElementById('page-wrapper').classList.remove("__web-inspector-hide-shortcut__");
			//document.getElementById('page-wrapper').style.background = 'yellow';
	   }
   }
}

function isDOMContains(parentEle,ele,container){
    //parentEle: 要判断节点的父级节点
    //ele:要判断的子节点
    //container : 二者的父级节点
    
    //如果parentEle h和ele传的值一样，那么两个节点相同
    if(parentEle == ele){
        return true
    }
    if(!ele || !ele.nodeType || ele.nodeType != 1){
        return false;
    }
    //如果浏览器支持contains
    if(parentEle.contains){
        return parentEle.contains(ele)
    }
    //火狐支持
    if(parentEle.compareDocumentPosition){
        return !!(parentEle.compareDocumentPosition(ele)&16);
    }

    //获取ele的父节点
    var parEle = ele.parentNode;
    while(parEle && parEle != container){
       if(parEle == parentEle){
        return true;
       }
       parEle = parEle.parentNode;
    }
    return false;
}

function getScrollTop() {
    var scroll_top = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scroll_top = document.documentElement.scrollTop;
    }
    else if (document.body) {
        scroll_top = document.body.scrollTop;
    }
    return scroll_top;
}
function seturlexternTarget_Blank() {
    //将dw_toc中餐外部引用url添加 新窗口打开属性。
    var geturlextern = document.getElementsByClassName("urlextern");
    for(var i=0; i<geturlextern.length; i++){
       geturlextern[i].setAttribute('target','_blank'); 
    }


}
function imgOpenAtNewPage() {
    //将dw_toc中餐外部引用url添加 新窗口打开属性。

    var imgOpenAtNewPage = document.getElementById("dokuwiki__content").getElementsByTagName("img");
    for(var i=0; i<imgOpenAtNewPage.length; i++){
       imgOpenAtNewPage[i].parentNode.setAttribute('target','_blank'); 
    }
}
function innerlink_wikilink1() {
    //将dw_toc中内部引用url添加 图标。
    var wikilink1 = document.getElementsByClassName("wikilink1") 
    var linksvg = "<svg class='octicon octicon-link' viewBox='0 0 20 16' version='1.1' width='16' aria-hidden='true'><path fill-rule='evenodd' d='M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z'></path></svg>"
    for(var i=0; i<wikilink1.length; i++){
       wikilink1[i].innerHTML = linksvg + wikilink1[i].innerHTML; 
       //wikilink1[i].innerHTML = "❂" + wikilink1[i].innerHTML; 
    }
}
window.onload=function(){
	/*页面加载完成之后生成博客目录，此处设置调用的标签*/
      if(document.getElementById("page-wrapper")){
          BlogDirectory.createBlogDirectory("page-wrapper","h1","h2","h3",20);
          page_wrapper_number();
         // seturlexternTarget_Blank(); 
      }  //divscrollFn(event);
          //imgOpenAtNewPage();
          //innerlink_wikilink1();
	  //document.body.className += ' loaded';
}

