

[TOC]



# svn copy 

```bash
# 涉及svn操作的checkout / update / commit / copy / diff /merge

[root@centos76 tomcat_7000]# tree -L 1
.
├── assets/
├── changelog.txt
├── forms
├── index.html
├── portfolio-details.html
└── Readme.txt

2 directories, 6 files
[root@centos76 tomcat_7000]#

[root@centos76 tomcat_7000]# echo "new">> index_new.html

[root@centos76 tomcat_7000]# svn add index_new.html
A         index_new.html
[root@centos76 tomcat_7000]# svn commit -m "only add index_new.html"
Adding         index_new.html
Transmitting file data .
Committed revision 3.
[root@centos76 tomcat_7000]# svn log -r 3 -v
------------------------------------------------------------------------
r3 | svnadmin | 2020-07-13 15:15:08 +0800 (Mon, 13 Jul 2020) | 1 line
Changed paths:
   A /tomcat_7000/index_new.html

only add index_new.html
------------------------------------------------------------------------
[root@centos76 tomcat_7000]# echo "get">> index_get.html
[root@centos76 tomcat_7000]# svn add index_get.html
A         index_get.html
[root@centos76 tomcat_7000]# svn commit -m "only add index_get.html"
Adding         index_get.html
Transmitting file data .
Committed revision 4.
[root@centos76 tomcat_7000]# echo "make">> index_make.html
[root@centos76 tomcat_7000]# svn add index_make.html
A         index_make.html
[root@centos76 tomcat_7000]# svn commit -m "only add index_make.html"
Adding         index_make.html
Transmitting file data .
Committed revision 5.
[root@centos76 tomcat_7000]# tree -L 1
.
├── assets/
├── changelog.txt
├── forms
├── index.html
├── index_new.html
├── index_make.html
├── index_get.html
├── portfolio-details.html
└── Readme.txt

2 directories, 6 files
[root@centos76 tomcat_7000]#

[root@centos76 tomcat_7000]# svn log -r 1:5
------------------------------------------------------------------------
r5 | svnadmin | 2020-07-13 15:16:35 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_make.html
------------------------------------------------------------------------
r4 | svnadmin | 2020-07-13 15:16:09 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_get.html
------------------------------------------------------------------------
r3 | svnadmin | 2020-07-13 15:15:08 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_new.html
------------------------------------------------------------------------
r2 | svnadmin | 2020-03-27 08:52:09 +0800 (Fri, 27 Mar 2020) | 1 line

initial
------------------------------------------------------------------------
r1 | svnadmin | 2020-03-27 08:45:17 +0800 (Fri, 27 Mar 2020) | 1 line

新增流水线

[root@centos76 tomcat_7000]# 
[root@centos76 tomcat_7000]# svn merge -r 5:3  ./
svn: E195020: Cannot merge into mixed-revision working copy [2:5]; try updating first   # 需要先更新一下，本地svn库
[root@centos76 tomcat_7000]# svn update
Updating '.':
At revision 5.
[root@centos76 tomcat_7000]# svn merge -r 5:3  ./
--- Reverse-merging r5 through r4 into '.':
D    index_make.html               # 删除r5、r4中添加的文件
D    index_get.html
--- Recording mergeinfo for reverse merge of r5 through r4 into '.':
 U   .
--- Eliding mergeinfo from '.':
 U   .
[root@centos76 tomcat_7000]# tree -L 1
.
├── assets/
├── changelog.txt
├── forms
├── index.html
├── index_new.html    # 表明为r3状态
├── portfolio-details.html
└── Readme.txt

[root@centos76 tomcat_7000]# svn log
------------------------------------------------------------------------
r5 | svnadmin | 2020-07-13 15:16:35 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_make.html
------------------------------------------------------------------------
r4 | svnadmin | 2020-07-13 15:16:09 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_get.html
------------------------------------------------------------------------
r3 | svnadmin | 2020-07-13 15:15:08 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_new.html
------------------------------------------------------------------------
r2 | svnadmin | 2020-03-27 08:52:09 +0800 (Fri, 27 Mar 2020) | 1 line

initial
------------------------------------------------------------------------
r1 | svnadmin | 2020-03-27 08:45:17 +0800 (Fri, 27 Mar 2020) | 1 line

新增流水线
------------------------------------------------------------------------
[root@centos76 tomcat_7000]# svn update
Updating '.':
At revision 5.
# 再次新增文件 
[root@centos76 tomcat_7000]# echo "xixi" >> index_xixi.html
[root@centos76 tomcat_7000]# svn add index_xixi.html
A         index_xixi.html
[root@centos76 tomcat_7000]# svn commit -m "only add index_xixi.html"
Deleting       index_get.html                     
# 在svn本地的文件状态开始更新，此前服务端已经在svn merge操作时回滚了。
Deleting       index_make.html
Adding         index_xixi.html
Transmitting file data .
Committed revision 6.

[root@centos76 tomcat_7000]# svn log
------------------------------------------------------------------------
r5 | svnadmin | 2020-07-13 15:16:35 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_make.html
------------------------------------------------------------------------
r4 | svnadmin | 2020-07-13 15:16:09 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_get.html
------------------------------------------------------------------------
r3 | svnadmin | 2020-07-13 15:15:08 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_new.html
------------------------------------------------------------------------
r2 | svnadmin | 2020-03-27 08:52:09 +0800 (Fri, 27 Mar 2020) | 1 line

initial
------------------------------------------------------------------------
r1 | svnadmin | 2020-03-27 08:45:17 +0800 (Fri, 27 Mar 2020) | 1 line

新增流水线
------------------------------------------------------------------------
[root@centos76 tomcat_7000]# svn log -r 6
------------------------------------------------------------------------
r6 | svnadmin | 2020-07-13 15:27:33 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_xixi.html
------------------------------------------------------------------------
[root@centos76 tomcat_7000]# svn log -r 5
------------------------------------------------------------------------
r5 | svnadmin | 2020-07-13 15:16:35 +0800 (Mon, 13 Jul 2020) | 1 line

only add index_make.html
------------------------------------------------------------------------

# 将r6回滚到r5
[root@centos76 tomcat_7000]# svn merge -r 6:5  ./
--- Reverse-merging r6 into '.':
A    index_get.html
A    index_make.html
D    index_xixi.html
--- Recording mergeinfo for reverse merge of r6 into '.':
 U   .
--- Eliding mergeinfo from '.':
 U   .

# 如何将r6中新增的文件index_xixi.html，合并到r7中。
利用svn diff，但这个命令只能输出文件列表，不可导出。可以使用ToitorsiSVN图形客户端的高级功能实现导出。

手动覆盖合并差异文件到r6中，再作提交。

```





# svn copy 

```
svn copy -r HEAD  svn://10.24.68.81/root/test  svn://10.24.68.81/root/ww -m 'svn copy dir only'
# 最新版本复制
svn copy -r 6  svn://10.24.68.81/root/test  svn://10.24.68.81/root/ww -m 'svn copy dir only'
# 指定版本复制
# 此时如果/root/zwfw目录不存在，则会创建复本。如果存在则会在目录 内创建test子目录/root/zwfw/test。需要根据实际需要结合svn delete使用。
```



# svn delete

```
svn delete  svn://10.24.68.81/root/ -m 'svn copy dir only'
```





# svn import 

静默提交 -q

```

cd 到代码根目录,需要远程目录svn://192.168.0.127/ecloud_default/kywt不存在，如果存在则会添加为svn://192.168.0.127/root/ 的子目录。
svn import ./ svn://192.168.0.127/root/ -m "initial root"
svn import -q ./ svn://192.168.0.127/root/ -m "initial root"
svn import /some/dir svn://192.168.0.127/root/ -m "initial root"
```



# 更新图标不显示注册表修改



```ini


# 导出相应项（）
REG EXPORT HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ShellIconOverlayIdentifiers C:\svn_raw.reg
# 删除所有项
REG delete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ShellIconOverlayIdentifiers"
# 
备份修改导出reg文件，
将Tortoise 1-N  Modified修改为其它的名称，其其实就是让他不识别，比如加个前缀old_,用记事本批量替换即可。
[HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ShellIconOverlayIdentifiers\Tortoise2Modified]
@="{C5994561-53D9-4125-87C9-F193FC689CB2}"

#然后保存替换后的reg文件，双击导入。

#重启资源处理器生效。

进入任务管理器，选择资源管理器，重新启动，或关闭explorer.exe


# 这个是键值的删除
REG delete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\ShellIconOverlayIdentifiers" /v PendingFileRenameOperations


```

