/**
 * author imy 
 *
 * */
/*include dayjs*/
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):(t="undefined"!=typeof globalThis?globalThis:t||self).dayjs=e()}(this,(function(){"use strict";var t=1e3,e=6e4,n=36e5,r="millisecond",i="second",s="minute",u="hour",a="day",o="week",f="month",h="quarter",c="year",d="date",$="Invalid Date",l=/^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,y=/\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,M={name:"en",weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_")},m=function(t,e,n){var r=String(t);return!r||r.length>=e?t:""+Array(e+1-r.length).join(n)+t},g={s:m,z:function(t){var e=-t.utcOffset(),n=Math.abs(e),r=Math.floor(n/60),i=n%60;return(e<=0?"+":"-")+m(r,2,"0")+":"+m(i,2,"0")},m:function t(e,n){if(e.date()<n.date())return-t(n,e);var r=12*(n.year()-e.year())+(n.month()-e.month()),i=e.clone().add(r,f),s=n-i<0,u=e.clone().add(r+(s?-1:1),f);return+(-(r+(n-i)/(s?i-u:u-i))||0)},a:function(t){return t<0?Math.ceil(t)||0:Math.floor(t)},p:function(t){return{M:f,y:c,w:o,d:a,D:d,h:u,m:s,s:i,ms:r,Q:h}[t]||String(t||"").toLowerCase().replace(/s$/,"")},u:function(t){return void 0===t}},v="en",D={};D[v]=M;var p=function(t){return t instanceof _},S=function t(e,n,r){var i;if(!e)return v;if("string"==typeof e){var s=e.toLowerCase();D[s]&&(i=s),n&&(D[s]=n,i=s);var u=e.split("-");if(!i&&u.length>1)return t(u[0])}else{var a=e.name;D[a]=e,i=a}return!r&&i&&(v=i),i||!r&&v},w=function(t,e){if(p(t))return t.clone();var n="object"==typeof e?e:{};return n.date=t,n.args=arguments,new _(n)},O=g;O.l=S,O.i=p,O.w=function(t,e){return w(t,{locale:e.$L,utc:e.$u,x:e.$x,$offset:e.$offset})};var _=function(){function M(t){this.$L=S(t.locale,null,!0),this.parse(t)}var m=M.prototype;return m.parse=function(t){this.$d=function(t){var e=t.date,n=t.utc;if(null===e)return new Date(NaN);if(O.u(e))return new Date;if(e instanceof Date)return new Date(e);if("string"==typeof e&&!/Z$/i.test(e)){var r=e.match(l);if(r){var i=r[2]-1||0,s=(r[7]||"0").substring(0,3);return n?new Date(Date.UTC(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)):new Date(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)}}return new Date(e)}(t),this.$x=t.x||{},this.init()},m.init=function(){var t=this.$d;this.$y=t.getFullYear(),this.$M=t.getMonth(),this.$D=t.getDate(),this.$W=t.getDay(),this.$H=t.getHours(),this.$m=t.getMinutes(),this.$s=t.getSeconds(),this.$ms=t.getMilliseconds()},m.$utils=function(){return O},m.isValid=function(){return!(this.$d.toString()===$)},m.isSame=function(t,e){var n=w(t);return this.startOf(e)<=n&&n<=this.endOf(e)},m.isAfter=function(t,e){return w(t)<this.startOf(e)},m.isBefore=function(t,e){return this.endOf(e)<w(t)},m.$g=function(t,e,n){return O.u(t)?this[e]:this.set(n,t)},m.unix=function(){return Math.floor(this.valueOf()/1e3)},m.valueOf=function(){return this.$d.getTime()},m.startOf=function(t,e){var n=this,r=!!O.u(e)||e,h=O.p(t),$=function(t,e){var i=O.w(n.$u?Date.UTC(n.$y,e,t):new Date(n.$y,e,t),n);return r?i:i.endOf(a)},l=function(t,e){return O.w(n.toDate()[t].apply(n.toDate("s"),(r?[0,0,0,0]:[23,59,59,999]).slice(e)),n)},y=this.$W,M=this.$M,m=this.$D,g="set"+(this.$u?"UTC":"");switch(h){case c:return r?$(1,0):$(31,11);case f:return r?$(1,M):$(0,M+1);case o:var v=this.$locale().weekStart||0,D=(y<v?y+7:y)-v;return $(r?m-D:m+(6-D),M);case a:case d:return l(g+"Hours",0);case u:return l(g+"Minutes",1);case s:return l(g+"Seconds",2);case i:return l(g+"Milliseconds",3);default:return this.clone()}},m.endOf=function(t){return this.startOf(t,!1)},m.$set=function(t,e){var n,o=O.p(t),h="set"+(this.$u?"UTC":""),$=(n={},n[a]=h+"Date",n[d]=h+"Date",n[f]=h+"Month",n[c]=h+"FullYear",n[u]=h+"Hours",n[s]=h+"Minutes",n[i]=h+"Seconds",n[r]=h+"Milliseconds",n)[o],l=o===a?this.$D+(e-this.$W):e;if(o===f||o===c){var y=this.clone().set(d,1);y.$d[$](l),y.init(),this.$d=y.set(d,Math.min(this.$D,y.daysInMonth())).$d}else $&&this.$d[$](l);return this.init(),this},m.set=function(t,e){return this.clone().$set(t,e)},m.get=function(t){return this[O.p(t)]()},m.add=function(r,h){var d,$=this;r=Number(r);var l=O.p(h),y=function(t){var e=w($);return O.w(e.date(e.date()+Math.round(t*r)),$)};if(l===f)return this.set(f,this.$M+r);if(l===c)return this.set(c,this.$y+r);if(l===a)return y(1);if(l===o)return y(7);var M=(d={},d[s]=e,d[u]=n,d[i]=t,d)[l]||1,m=this.$d.getTime()+r*M;return O.w(m,this)},m.subtract=function(t,e){return this.add(-1*t,e)},m.format=function(t){var e=this,n=this.$locale();if(!this.isValid())return n.invalidDate||$;var r=t||"YYYY-MM-DDTHH:mm:ssZ",i=O.z(this),s=this.$H,u=this.$m,a=this.$M,o=n.weekdays,f=n.months,h=function(t,n,i,s){return t&&(t[n]||t(e,r))||i[n].slice(0,s)},c=function(t){return O.s(s%12||12,t,"0")},d=n.meridiem||function(t,e,n){var r=t<12?"AM":"PM";return n?r.toLowerCase():r},l={YY:String(this.$y).slice(-2),YYYY:this.$y,M:a+1,MM:O.s(a+1,2,"0"),MMM:h(n.monthsShort,a,f,3),MMMM:h(f,a),D:this.$D,DD:O.s(this.$D,2,"0"),d:String(this.$W),dd:h(n.weekdaysMin,this.$W,o,2),ddd:h(n.weekdaysShort,this.$W,o,3),dddd:o[this.$W],H:String(s),HH:O.s(s,2,"0"),h:c(1),hh:c(2),a:d(s,u,!0),A:d(s,u,!1),m:String(u),mm:O.s(u,2,"0"),s:String(this.$s),ss:O.s(this.$s,2,"0"),SSS:O.s(this.$ms,3,"0"),Z:i};return r.replace(y,(function(t,e){return e||l[t]||i.replace(":","")}))},m.utcOffset=function(){return 15*-Math.round(this.$d.getTimezoneOffset()/15)},m.diff=function(r,d,$){var l,y=O.p(d),M=w(r),m=(M.utcOffset()-this.utcOffset())*e,g=this-M,v=O.m(this,M);return v=(l={},l[c]=v/12,l[f]=v,l[h]=v/3,l[o]=(g-m)/6048e5,l[a]=(g-m)/864e5,l[u]=g/n,l[s]=g/e,l[i]=g/t,l)[y]||g,$?v:O.a(v)},m.daysInMonth=function(){return this.endOf(f).$D},m.$locale=function(){return D[this.$L]},m.locale=function(t,e){if(!t)return this.$L;var n=this.clone(),r=S(t,e,!0);return r&&(n.$L=r),n},m.clone=function(){return O.w(this.$d,this)},m.toDate=function(){return new Date(this.valueOf())},m.toJSON=function(){return this.isValid()?this.toISOString():null},m.toISOString=function(){return this.$d.toISOString()},m.toString=function(){return this.$d.toUTCString()},M}(),T=_.prototype;return w.prototype=T,[["$ms",r],["$s",i],["$m",s],["$H",u],["$W",a],["$M",f],["$y",c],["$D",d]].forEach((function(t){T[t[1]]=function(e){return this.$g(e,t[0],t[1])}})),w.extend=function(t,e){return t.$i||(t(e,_,w),t.$i=!0),w},w.locale=S,w.isDayjs=p,w.unix=function(t){return w(1e3*t)},w.en=D[v],w.Ls=D,w.p={},w}));

dayjs().format();
if(window.location.href.search('file:///') != -1 ){
    document.title = '离线导出版本: ' + document.title;
}
if(window.location.href.search('xinzhiba.top') != -1  ){
    document.title = 'X-site Dev: ' + document.title;
}
var oMeta = document.createElement('meta');
oMeta.content='catsnyne_gmail_com,amos';
oMeta.name='author';
var pMeta = document.createElement('meta');
pMeta.content='IE=edge;text/html; charset=UTF-8';
pMeta.httpEquiv='X-UA-Compatible';
var qMeta = document.createElement('meta');
qMeta.charset='utf-8';
document.getElementsByTagName('head')[0].appendChild(oMeta);
document.getElementsByTagName('head')[0].appendChild(pMeta);
document.getElementsByTagName('head')[0].appendChild(qMeta);

/*清除typora导出时未删除的head style样式 */
if ( document.head.getElementsByTagName("style")[0] ){
    document.head.getElementsByTagName("style")[0].remove();
}
////content_div = document.getElementById('ctl00_cphContent_UpdatePanel1');
////document.body.appendChild(content_div);

//// document.getElementsByTagName("form")[0].remove();
////brday = document.getElementById('ctl00_cphContent_uiHoster_lblBirthday');
////brday.innerText=' NULL';
////content_write = document.getElementById('write');
////document.body.appendChild(content_write);
////content_div.remove();
////document.getElementsByTagName("link")[0].remove();

// 顶部div
var typora_image ="<div><div class='top_header'>Navigations</div><div class='top_line'>The first step is as good as half over. </div><!-- --> <div id='MainMenu'> <div id='MainMenuTop'></div> <ul> <li><a href='https://xinzhiba.top/main/pod/pages/passwordgenerate' title='随机密码' target='_blank'><span>随机密码</span></a></li> <li><a href='https://xinzhiba.top/geoip2' title='IP归属地查询' target='_blank'><span>IP归属地</span></a></li> <li><a class='a_clr_1' href='https://xinzhiba.top/main/pod/pages/mb_unit_convert' title='二进制单位转换' target='_blank'><span>进制转换</span></a></li> <li><a href='https://xinzhiba.top/navigation/' ltitle='工具导航'><span>工具导航</span></a></li> </ul></div></div>";
var typora_topdiv = document.createElement("div");
typora_topdiv.setAttribute('id','topdiv')
typora_topdiv.innerHTML=typora_image;
//document.getElementById('write').appendChild(typora_topdiv);

// document.getElementById('write').insertBefore(typora_topdiv,document.getElementById('write').getElementsByTagName("p")[0]);
//document.getElementsByClassName('typora-export-content')[0].insertBefore(typora_topdiv,document.getElementById('write'));
//document.getElementsByClassName('typora-export-content')[0].insertBefore(typora_topdiv,document.getElementById('write'));
document.body.insertBefore(typora_topdiv,document.getElementsByClassName('typora-export-content')[0]);


var typora_image ="<div id='js_bt'><span><a href='https://xinzhiba.top'>Sow nothing, reap nothing.</a></span></div>";
var typora_btdiv = document.createElement("div");
typora_btdiv.innerHTML=typora_image;
document.getElementById('write').appendChild(typora_btdiv);

document.body.appendChild(typora_btdiv);


if(window.location.href.search('xinzhiba.top') != -1 ){
    var site_url ='<div id=><span><a target="_blank" href="http://beian.miit.gov.cn/" title=""> 豫ICP备2022027398号-1</a></span><span><a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=41148102000279" title=""><img src="/res/img/beian.png" height="15" alt="豫公网安备 41148102000279号">豫公网安备 41148102000279号</a></span></div>';
}
var toc ="<div id='list_1'><div><ul><li class='list_1_a_active'><a >📜Readme</a></li><li class='list_1_a_gray'><a>🔗Star</a></li></ul></div><iv><div id='list_2' class='select_list'><div id='tab-summary' class='topic_list'> <div><h2 style='color:black;text-align:center;'>Table of Contents</h2></div><a href='#'></a></div></div><div id='list_2_1' class='select_list tab_hidden'><div id='tab-summary1' class='topic_list'> <div><h2 style='color:black;text-align:center;'>往期清单</h2></div></div></div><div id='list_3'> <div style='text-align:center;'> Do have faith in what you're doing </div> <div style='text-align:center;'> World is powered by solitude. </div>	  <div style='text-align:center;'> " + site_url+ "</div></div>";
var md_toc_div = document.createElement("div");
md_toc_div.setAttribute('id','md_toc')
md_toc_div.innerHTML=toc;
document.body.appendChild(md_toc_div);
//document.getElementById('write').appendChild(typora_topdiv);

////document.body.insertBefore(md_toc_div,document.getElementById('write'));
document.getElementsByClassName('typora-export-content')[0].insertBefore(md_toc_div,document.getElementById('write'));



// ############################################################

var md_toc = document.getElementById("write")


var dlist = document.getElementById("md_toc");
var dlist_left = document.getElementById("tab-summary");



var num = 0;//统计找到的mt和st

//遍历所有元素结点
		var hh11=0;/* 一个是中间变量一个是递增变量*/
		var hh22=0;
		var hh33=0;
		var hh44=0;

for(var i=0; i<md_toc.childElementCount; i++)
{ 
	//document.getElementsByClassName("md-toc")[0].children[0].children.length

	//console.log(md_toc[i].className);
	//console.log(md_toc[i].children[0].hash);


var t_name = md_toc.children[i].tagName;
// console.log(t_name);
// var str = md_toc[i].className;
var reg = RegExp(/md-toc-h/);
//console.log(reg.test(str)); // true
  if(RegExp(/H1/).test(t_name)){
	//console.log(t_name,RegExp(/H1/).test(t_name));
	var md_toc_hx = "H1";
  }else if(RegExp(/LINK/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "LINK";
  }else if(RegExp(/H2/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H2";
  }else if(RegExp(/H3/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H3";
  }else if(RegExp(/H4/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H4";
  }else{
    //console.log("else off");
	var md_toc_hx = "false";
  }

 var listnodetext = md_toc.children[i].getAttribute('id') ;
// console.log(listnodetext);
 //var listnodetext = md_toc.children[i].innerText ;
 //var listnodetext = md_toc[i].innerText ;

// var str="jfkldsjalk,.23@#!$$k~!  @#$%^&*()(_+-=|\{}[]';:,./<>??gg  g~```gf"; 
// listnodetext=listnodetext.replace("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]",""); 

var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]","g");
// 将以上匹配的字符全部清理掉。
//// listnodetext=listnodetext.replace(reg,"").toLowerCase();
// g
// listnodetext=listnodetext.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\、|\（|\、|\）|\?]/g,""); 
//// console.log(listnodetext);


//var nodeIdGet = listnodetext; 
var md_toc_side = "";
var default_none = 0;
switch(md_toc_hx)
{
	case "H1":    //若为主标题 
		item = document.createElement("dt");
								hh11 = hh11 +1 ;
								////md_toc[i].innerHTML = "§ " + hh11 + ". " + listnodetext;
                                                                var h1_str = toChinesNum(hh11) ;
                                                                if (h1_str.length >=2 && h1_str.length <= 3 && h1_str.substr(0,1) =='一') {
                                                                   md_toc_side="" + h1_str.substr(1,2);
                                                                }else {
                                                                 md_toc_side="" + toChinesNum(hh11) ;
                                                                }
								hh22 = 0 ; 
		break;
	case "H2":    //若为子标题
		item = document.createElement("dd");
								hh22 = hh22 +1 ;
								////md_toc[i].innerHTML = hh11 + "." + hh22 + ". " + listnodetext;
								md_toc_side= hh11 + "." + hh22;
								hh33 = 0; 
		break;
	case "H3":    //若为子标题
		item = document.createElement("de");
								hh33 = hh33 + 1 ;
								if (hh33 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33;
								}
								//hh33 = hh33 + 1 ;
		break;
	
	case "H4":    //若为子标题
		item = document.createElement("df");
								hh44 = hh44 + 1 ;
								if (hh44 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								   if ( hh33 == 0 ){
										   hh33 = hh33 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33 + "." + hh44;
								}
								//hh33 = hh33 + 1 ;
		break;
	default:    //若为子标题
                 default_none = 1;
                 hh11 = hh11;
                 hh22 = hh22;
                 hh33 = hh33; 
                 hh44 = hh44;
                // console.log("switch_default",default_none,"hh11:",hh11,"hh22:",hh22,"hh33:",hh33,"hh44:",hh44);
}


if (default_none == 0 ) {
// console.log("if_default_none:",default_none);
var ahreftextnode = document.createElement('a');
//var ahreftextnode= item.appendChild(a)
// ahreftextnode.setAttribute('href', "#" + nodeIdGet);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].hash);
ahreftextnode.setAttribute('href', '#' + listnodetext);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].getAttribute('href'));
//// ahreftextnode.classList.add("list-group-item");

ahreftextnode.innerHTML= "<em>" + md_toc_side + ". </em>" + listnodetext;
//ahreftextnode.innerHTML=nodetext;
// console.log(item);
item.appendChild(ahreftextnode);
item.className='dXdefault';

//	};
//将自定义表项加入自定义列表中
//dlist.appendChild(item);
//console.log(dlist_left)
dlist_left.appendChild(item);
}
num++;





// console.timeEnd("开始时间点");
//添加目录列表结束标记而矣

}

toppp = document.createElement("toppp");
enddd = document.createElement("enddd");

top_a_contents = document.createElement('a');
top_a_contents.setAttribute('href','#top');
end_a_contents = document.createElement('a');
end_a_contents.setAttribute('href','#bottom');


top_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='height:2em;background-color:#4b4033;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 0 0 4px 4px;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

end_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='margin-top:1em;padding:0.3em 0;background-color:#4baa33;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 4px 4px 0 0;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

enddd.appendChild(end_a_contents);
toppp.appendChild(top_a_contents);

 var plink = document.getElementById("post_1");
 var link = document.getElementById("post_2");
//plink.insertBefore(toppp,link);
// plink.appendChild(enddd);




//// ######################################################################

/* 保证左侧目录占用整个高度,在滚动时 */

//document.getElementById("md_toc").style.position = "fixed";  
//document.getElementById("md_toc").style.top = ( 118 - document.documentElement.scrollTop);    

function md_toc_view() {
  if (document.documentElement.scrollTop > 118) {
    document.getElementById("md_toc").style.position = "fixed";    
    document.getElementById("md_toc").style.top = "0";    
  } else {
    document.getElementById("md_toc").setAttribute('style', '');;  
    //document.getElementById("md_toc").style.position = "initial";  
  }
}



/*baidu tongji,此段将自动在head中添加一个script*/
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?384e012e99d0eef2e010ff5cc5355ce9";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();

  /* var typora_json = document.createElement("script");
  typora_json.src = "https://xinzhiba.top/cdn_static/typora.json";
  typora_json.type = "text/javascript";
  document.getElementsByTagName('head')[0].appendChild(typora_json);*/
  var check_ie = document.createElement("script");
  check_ie.src="https://xinzhiba.top/cdn_static/check_ie_latest.js";
  document.getElementsByTagName('head')[0].appendChild(check_ie);




for(var i=0;i < document.getElementsByClassName('CodeMirror-code').length; i++){
   for( var j=0; j < document.getElementsByClassName('CodeMirror-code')[i].children.length ;j++ ){
      if( !(j % 2) ){
         document.getElementsByClassName('CodeMirror-code')[i].children[j].setAttribute('class','alt1')
      }else{
         document.getElementsByClassName('CodeMirror-code')[i].children[j].setAttribute('class','alt2')
      }
   }


}



    var list = document.getElementById("tab-summary").getElementsByTagName("a");
    for (var i = 1; i < list.length; i++) {
        //为li注册鼠标进入事件
        list[i].parentNode.onmouseover = function () {
            //设置其背景颜色为黄色
            //this.style.backgroundColor = "#f4ebe5";
            this.className='dXhover'; //this.parentNode.style.backgroundColor = "#f4ebe5";
        };
        //为li注册鼠标离开事件
        list[i].parentNode.onmouseout = function () {
            //恢复到这个标签默认的颜色
            //this.style.backgroundColor = "";
            this.className='dXdefault';
            //this.parentNode.style.backgroundColor = "";
        };
    }



/*标签切换*/

    var tabs =document.getElementById("list_1").getElementsByTagName("li");
    var contents =document.getElementById("md_toc").getElementsByClassName("select_list");
                //var contents = document.getElementsByClassName('tab-content')[0].getElementsByTagName('div');

            (function changeTab(tab) {
                for(var i = 0, len = tabs.length; i < len; i++) {
                    tabs[i].onclick = showTab;
                }
            })();

            function showTab() {
                for(var i = 0, len = 2 ; i < len; i++) {
                    if(tabs[i] === this) {
                        console.log("=",tabs[i]);
                        tabs[i].className = 'list_1_a_active';
                        contents[i].className = 'select_list tab_show';
                        console.log("contents[i]",contents[i]);
                        console.log("i = ",i);
                    } else {
                        console.log("else",tabs[i]);
                        tabs[i].className = 'list_1_a_gray ';
                        contents[i].className = 'select_list tab_hidden';
                        console.log("else i = ",i);
                    }
                }
            }



// 加载json列表
/*var tab_url_json = JSON.parse(tab_url);
var a_list = "";
for ( var i = 0; i < tab_url_json.length ; i++ ) {
    a_list = a_list + '<a href="' + tab_url_json[i].link_url + '">' + tab_url_json[i].link_title + '</a>';
}
console.log(a_list);
var temp = document.getElementById('tab-summary1').getElementsByTagName('h2')[0].parentNode;
temp.innerHTML=a_list;
*/



fetch('/main/pod/imy/data/json_api_links_all.json').then(function(response) {
  return response.json().then(function(json) {
      var list_latest_publish_j_b = '';
      var list_latest_publish = '';
      var link_title_parse= '';
      var j_b= 0; // j的前一个
      var j_a= 0; // j的后一个 
      var length = json.length; 
      var mk=0;
      var tnum=0;
      var json_mk={};
      var tags='';
      var json_tags=[];
      var json_tags_same=[]; /* 相同标签 */
      //console.log(json_tags_same);
      var arr_tags=[];
      for(j=0;j<length;j++){
         if(json[j].link_root == 'markdown'){
            json_mk[mk]=json[j]; 
            mk++;
         }
         //var date = new Date(json[j].link_time);
         if(json[j].link_url.search(window.location.pathname) != -1){
             arr_tags=json[j].link_tags;
             //console.log(json[j].link_tags);
             link_title_parse=j;
             j_b=j-1;
             j_a=j+1;
             //console.log(json[j].link_url);
             //console.log(window.location.pathname);
             if(j != 0){
                 list_latest_publish_j_b = list_latest_publish + '<li class="new list list-edit" style="list-style:none;float:right;margin-right:2em;"><span class="date" style="display: ;">' + dayjs(json[j_b].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a  href="' + json[j_b].link_url + '">' + json[j_b].link_title + '</a></span><span>后一篇</span></li>';
             }else{
                 list_latest_publish_j_b = list_latest_publish + '<li class="new list list-edit" style="list-style:none;float:right;margin-right:2em;"><span class="date" style="display: ;">' + '</span><span class="title"><i class="icon-flag"></i> <a  href="/">最后一篇了，回首页看看吧</a></span></li>';
             }
             list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';
             list_latest_publish_j_a =  '<li class="new list list-edit" style="float:left;list-style:none;margin-left:2em;"><span class="date" style="display: ;">' + dayjs(json[j_a].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a href="' + json[j_a].link_url + '">' + json[j_a].link_title + '</a></span><span>前一篇</span></li>';
             //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';
         }
         //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
      }
      var tnum=0;
      //console.log(json_tags_same);
      for(j=0;j<length;j++){  /*遍历和当前链接相同标签的链接*/
          for(t=0;t<arr_tags.length;t++){ /* 将当前链接的标签对所有json进程逐个遍历 */ 
              //console.log(arr_tags[t]);
              //console.log(json.filter(function( _json){ return _json.link_tags == arr_tags[t]})); 
               // 此处需要使用=,而不是==，前者可以从指定数据中取，后者则是字符串完全一致
              //console.log(json[j].link_tags);
              //console.log(json[j].link_tags.includes(arr_tags[t]));
              //console.log(json[j].link_tags);
              //console.log(['网络分析', 'basic'].includes('basic'));
              if(json[j].link_tags.includes(arr_tags[t])){
                  //console.log("tnum:" + tnum);
                  if(eval(tnum) < eval(1)){  /* js是弱类型数值不能直接比较，会被当成字符串，而字符串有长短*/
                      //console.log(json_tags_same);
                      //console.log( json_tags_same.length);
                      //console.log(json[j]);
                      json_tags_same[tnum]=json[j]; /*  为防止一个链接的多个标签被重复遍历,作if-else判断,从第2个开始判断 */
                      //console.log("0000000000000");
                      //console.log(json_tags_same);
                      tnum++;
                  }else{
                   //console.log( json_tags_same.length);
                    if(!(json_tags_same[json_tags_same.length -1].link_title == json[j].link_title)){  /*这里只需要做一次大循环遍历与上一个小循环的最后一个对象的对比判断*/
                      json_tags_same[tnum]=json[j]; /*  为防止一个链接的多个标签被重复遍历,作if-else判断,从第2个开始判断 */
                      //console.log("1000000000000");
                      //console.log(json_tags_same.length);
                      tnum++;
                      //console.log('===='+ tnum);
                    } 
                 }
              }
          }
      }
      //console.log( json_tags_same );
      //console.log( typeof(json_tags) );
     var aside_list='';
     for(a=0;a<tnum;a++){ // 将 markdown类型显示在siderbar的副标签下。
        aside_list = aside_list + '<li data-priority=""><a href="' + json_tags_same[a].link_url + '"><div></div><div><strong>' + json_tags_same[a].link_title + '</strong></div></a></li>';
        //aside_list = aside_list + '<a href="' + json_tags[a].link_url + '">' + json_tags[a].link_title + '</a>';
     }
      console.log( aside_list);
      var tags_aside = '<aside aria-label="更多结果">   <ol id="b_context">     <li class="b_ans" h="SERP,5442.1" data-bm="20">    <div display="show" class="dis_show" style="float:right;width:1em;margin-top:2em;background-color:#fff;margin-left:-1em;margin-right:-1em"><a href="javascript:void(0)" style="text-decoration: none;">↹点击折叠侧边栏</a></div>   <div class="b_rrsr dis_show" style="float:right;padding-top:1.5em;min-height:200px">         <span style="margin-left:1.2em;float:left;font-size:1em;font-weight:bold">相关链接</span>         <ul class="b_vList b_divsec"> 		 </ul>       </div>     </li>   </ol> </aside>';
      var tags_aside_div = document.createElement('div');
      tags_aside_div.innerHTML=tags_aside;
      document.querySelector("#write").appendChild(tags_aside_div);
     // document.body.appendChild(tags_aside_div);
      document.querySelector("aside>ol>li>div>ul").innerHTML=aside_list;
     var a_list = "";
     for(a=0;a<mk;a++){ // 将 markdown类型显示在siderbar的副标签下。
        a_list = a_list + '<a href="' + json_mk[a].link_url + '">' + json_mk[a].link_title + '</a>';
     }
     if(document.querySelector('#js_bt')){
         var typora_image ="<div id='js_bt'><span><a href='https://xinzhiba.top'>Sow nothing, reap nothing.</a></span></div>";
         var typora_btdiv = document.createElement("div");
         typora_btdiv.innerHTML=typora_image;
         document.getElementById('write').appendChild(typora_btdiv);

         document.body.appendChild(typora_btdiv);
      }
      document.getElementById("js_bt").innerHTML=list_latest_publish_j_a + list_latest_publish_j_b;
      document.querySelector("div.b_rrsr>span").innerHTML='当前页面标签:<button>'+ arr_tags+ "</button>，以下为相关";
      document.getElementById('tab-summary1').getElementsByTagName('h2')[0].parentNode.innerHTML=a_list;
      //console.log(tags);
      //document.getElementById("latest_publish").innerHTML=list_latest_publish;

  });
});
window.onload=function(){
var aside = document.querySelector("div[display='show']");
aside.onclick = function(){
  if(document.querySelector('div.b_rrsr').className == 'b_rrsr dis_show'){
      document.querySelector('div.b_rrsr').className='b_rrsr dis_none';
      document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">↹点击展开侧边栏</a>';
  }else{
      document.querySelector('div.b_rrsr').className='b_rrsr dis_show';
      document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">↹点击折叠侧边栏</a>';
  }
} 
}
function tags_list(){
        // 用于检测滚动条到底时显示相关链接的标签
        //变量scrollTop是滚动条滚动时，距离顶部的距离
        var scrollTop = document.documentElement.scrollTop||document.body.scrollTop;
        //变量windowHeight是可视区的高度
        var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
        //变量scrollHeight是滚动条的总高度
        var scrollHeight = document.documentElement.scrollHeight||document.body.scrollHeight;
               //滚动条到底部的条件
            if(scrollTop+windowHeight==scrollHeight){
                document.querySelector('div.b_rrsr').className='b_rrsr dis_show';
                document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">↹点击折叠侧边栏</a>';
                console.log("距顶部"+scrollTop+"可视区高度"+windowHeight+"滚动条总高度"+scrollHeight);
             }   
}
window.onscroll = function() {
/* 保证左侧目录占用整个高度,在滚动时 */
   md_toc_view();tags_list();
};
/*  数字转换*/

     /**
     * 数字转成汉字
     * @params num === 要转换的数字
     * @return 汉字
     * */
     /**
     * 数字转成汉字
     * @params num === 要转换的数字
     * @return 汉字
     * */
    function toChinesNum(num) {
      let changeNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
      let unit = ['', '十', '百', '千', '万']
      num = parseInt(num)
      let getWan = (temp) => {
        let strArr = temp.toString().split('').reverse()
        let newNum = ''
        let newArr = []
        strArr.forEach((item, index) => {
          newArr.unshift(item === '0' ? changeNum[item] : changeNum[item] + unit[index])
        })
        let numArr = []
        newArr.forEach((m, n) => {
          if (m !== '零') numArr.push(n)
        })
        if (newArr.length > 1) {
          newArr.forEach((m, n) => {
            if (newArr[newArr.length - 1] === '零') {
              if (n <= numArr[numArr.length - 1]) {
                newNum += m
              }
            } else {
              newNum += m
            }
          })
        } else {
          newNum = newArr[0]
        }
 
        return newNum
      }
      let overWan = Math.floor(num / 10000)
      let noWan = num % 10000
      if (noWan.toString().length < 4) {
        noWan = '0' + noWan
      }
      return overWan ? getWan(overWan) + '万' + getWan(noWan) : getWan(num)
    }

