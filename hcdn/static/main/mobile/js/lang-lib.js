var lang = navigator.language;
var langBtn = $("#lang-btn");
var locale = lang.toLowerCase().indexOf('zh') > -1 ? "zh" : "en";

if (typeof Storage !== "undefined") {
  var currentLang = localStorage.currentLang;
  if (currentLang) {
    langBtn.text(currentLang == "zh" ? "En" : "中");
    locale = currentLang;
  }
}

langBtn.click(function () {
  locale = langBtn.text() == "中" ? "zh" : "en";
  if (typeof Storage !== "undefined") {
    localStorage.currentLang = locale;
  }
  location.reload();
});

// 特殊类型走中文，机场扫码属于特殊类型，走国际化
if (isSpecialType && lx != AIRPORT) {
  locale = "zh";
}

var di18n = new DI18n({
  locale: locale,
  isReplace: true,
  messages: {
    en: {
      姓名: "Name",
      证件号: "ID Number",
      欢迎进入: "Welcome to",
      解除隔离日期: "date of release",
      您已登记成功: "register success!",
      请在: "please observe home quarantine in ",
      小区居家隔离: "",
      请将此信息和有效证件原件出示给相关工作人员:
        "please show this information and you certificate.",
      登记信息: "register info",
      绑定信息: "binding info",
      编码: "code",
      名称: "org name",
      区域: "district",
      地址: "address",
      // 联系人: "contacter",
      // 联系方式: "contact info",
      本人信息: "personnal info",
      证件类型: "ID type",
      登记位置: "register place",
      登记信息确认: "Confirm the registered information",
      "检测到您之前登记过以下信息，您可以点击快捷登记进行登记，也可以点击继续登记自主登记。":
        "It is detected that you have previously registered the following information, you can click on shortcut to complete registration or click Reset to continue register.",
      操作: "Operation",
      重新填写: "Reset",
      全市居民小区健康登记管理系统:
        "Zhengzhou's Health Registration Management System of Citywide Residential Quarter",
      "应疫情防控要求，请郑州返程人员进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。":
        "For epidemic prevention, personnel who return to Zhengzhou are required to truthfully fill out the form below. The personal information submitted shall not be divulged for other purposes.Zhengzhou City Health Code System is developed based on Zhengzhou City’s integrated government service platform.For the prevention and control of the epidemic, I agree to become a Zheng Haoban user of Zhengzhou's integrated government service platform.",
      "针对疫情防控战役，要求所有人进入郑州时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。": "For the epidemic prevention and control campaign, all people are required to fill in and register when they enter Zhengzhou. Personal information will not be disclosed, please fill in truthfully.Zhengzhou City Health Code System is developed based on Zhengzhou City’s integrated government service platform.For the prevention and control of the epidemic, I agree to become a Zheng Haoban user of Zhengzhou's integrated government service platform.",
      "新郑机场健康扫码": "Health scanning code of Xinzheng Airport",
      "入郑健康扫码": "Health scanning code of Xinzheng Airport",
      // 请选择籍贯: "",
      请填写您的姓名: "Please fill in your name",
      身份证: "Chinese Identity card",
      其他: "others",
      请填写证件号码: "Please fill in your ID number",
      手机号: "Mobile Number",
      请填写11位数字手机号: "Please fill in your mobile number",
      现住地址: "Current Address",
      请输入详细地址:
        "please fill in your detail address: e.g. Room xx, Unit xx, building xx",
      车牌号: "lisence plate number",
      请输入车牌号: "Please fill in your license plate number",
      是否外地返回: "return from outside Zhengzhou",
      是: "yes",
      否: "no",
      来郑日期: "Date of arrival in Zhengzhou",
      请选择来郑日期: "Choose the date of your arrival in Zhengzhou",
      交通方式: "Transportation Method",
      请选择交通方式: "Choose your method of transportation",
      出发地: "Departure point",
      请选择出发地: "Choose the point of your departure",
      隔离方式: "Quarantine Manner",
      请选择隔离方式: "Choose your quarantine manner",
      我已阅读并承诺遵守: "I have read and would comply with &nbsp;",
      "《承诺告知书》": "the commitment notification",
      提交: "Submit",
      "是否国外返回": "Whether to return abroad",
      "请选择是否国外返回": "Select whether to return abroad",
      "出发国家": "Departure country",
      "请选择出发国家": "Please select Departure country"
    },
    zh: {
      欢迎进入: "欢迎进入",
      姓名: "姓名",
      证件号: "证件号",
      解除隔离日期: "解除隔离日期",
      您已登记成功: "您已登记成功！",
      请在: "请在",
      小区居家隔离: "小区居家隔离",
      请将此信息和有效证件原件出示给相关工作人员:
        "请将此信息和有效证件原件出示给相关工作人员",
      登记信息: "登记信息",
      绑定信息: "绑定信息",
      编码: "编码",
      名称: "名称",
      区域: "区域",
      地址: "地址",
      联系人: "联系人",
      联系方式: "联系方式",
      本人信息: "本人信息",
      证件类型: "证件类型",
      来郑日期: "来郑日期",
      登记位置: "登记位置",
      登记信息确认: "登记信息确认",
      "检测到您之前登记过以下信息，您可以点击快捷登记进行登记，也可以点击继续登记自主登记。":
        "检测到您之前登记过以下信息，您可以点击快捷登记进行登记，也可以点击继续登记自主登记。",
      操作: "操作",
      重新填写: "重新填写",
      全市居民小区健康登记管理系统: "全市居民小区健康登记管理系统",
      "应疫情防控要求，请郑州返程人员进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。":
        "应疫情防控要求，请郑州返程人员进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
      "针对疫情防控战役，要求所有人进入郑州时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。": "针对疫情防控战役，要求所有人进入郑州时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
      新郑机场健康扫码: "新郑机场健康扫码",
      // 请选择籍贯: "请选择籍贯",
      请填写您的姓名: "请填写您的姓名",
      身份证: "身份证",
      其他: "其他",
      请填写证件号码: "请填写证件号码",
      手机号: "手机号",
      请填写11位数字手机号: "请填写11位数字手机号",
      现住地址: "现住地址",
      请输入详细地址: "请输入详细地址：如xx小区xx号楼xx单元xx室",
      车牌号: "车牌号",
      请输入车牌号: "请输入车牌号",
      是否外地返回: "是否外地返回",
      是: "是",
      否: "否",
      请选择来郑日期: "请选择来郑日期",
      交通方式: "交通方式",
      请选择交通方式: "请选择交通方式",
      出发地: "出发地",
      请选择出发地: "请选择出发地",
      隔离方式: "隔离方式",
      请选择隔离方式: "请选择隔离方式",
      我已阅读并承诺遵守: "我已阅读并承诺遵守",
      "《承诺告知书》": "《承诺告知书》",
      提交: "提交",
      "是否国外返回": "是否国外返回",
      "请选择是否国外返回": "请选择是否国外返回",
      "出发国家": "出发国家",
      "请选择出发国家": "请选择出发国家",
    },
  },
});
