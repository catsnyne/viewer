/**
 * author imy 
 *
 * */
/*include dayjs*/
!function(t,e){"object"==typeof exports&&"undefined"!=typeof module?module.exports=e():"function"==typeof define&&define.amd?define(e):(t="undefined"!=typeof globalThis?globalThis:t||self).dayjs=e()}(this,(function(){"use strict";var t=1e3,e=6e4,n=36e5,r="millisecond",i="second",s="minute",u="hour",a="day",o="week",f="month",h="quarter",c="year",d="date",$="Invalid Date",l=/^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/,y=/\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g,M={name:"en",weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_")},m=function(t,e,n){var r=String(t);return!r||r.length>=e?t:""+Array(e+1-r.length).join(n)+t},g={s:m,z:function(t){var e=-t.utcOffset(),n=Math.abs(e),r=Math.floor(n/60),i=n%60;return(e<=0?"+":"-")+m(r,2,"0")+":"+m(i,2,"0")},m:function t(e,n){if(e.date()<n.date())return-t(n,e);var r=12*(n.year()-e.year())+(n.month()-e.month()),i=e.clone().add(r,f),s=n-i<0,u=e.clone().add(r+(s?-1:1),f);return+(-(r+(n-i)/(s?i-u:u-i))||0)},a:function(t){return t<0?Math.ceil(t)||0:Math.floor(t)},p:function(t){return{M:f,y:c,w:o,d:a,D:d,h:u,m:s,s:i,ms:r,Q:h}[t]||String(t||"").toLowerCase().replace(/s$/,"")},u:function(t){return void 0===t}},v="en",D={};D[v]=M;var p=function(t){return t instanceof _},S=function t(e,n,r){var i;if(!e)return v;if("string"==typeof e){var s=e.toLowerCase();D[s]&&(i=s),n&&(D[s]=n,i=s);var u=e.split("-");if(!i&&u.length>1)return t(u[0])}else{var a=e.name;D[a]=e,i=a}return!r&&i&&(v=i),i||!r&&v},w=function(t,e){if(p(t))return t.clone();var n="object"==typeof e?e:{};return n.date=t,n.args=arguments,new _(n)},O=g;O.l=S,O.i=p,O.w=function(t,e){return w(t,{locale:e.$L,utc:e.$u,x:e.$x,$offset:e.$offset})};var _=function(){function M(t){this.$L=S(t.locale,null,!0),this.parse(t)}var m=M.prototype;return m.parse=function(t){this.$d=function(t){var e=t.date,n=t.utc;if(null===e)return new Date(NaN);if(O.u(e))return new Date;if(e instanceof Date)return new Date(e);if("string"==typeof e&&!/Z$/i.test(e)){var r=e.match(l);if(r){var i=r[2]-1||0,s=(r[7]||"0").substring(0,3);return n?new Date(Date.UTC(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)):new Date(r[1],i,r[3]||1,r[4]||0,r[5]||0,r[6]||0,s)}}return new Date(e)}(t),this.$x=t.x||{},this.init()},m.init=function(){var t=this.$d;this.$y=t.getFullYear(),this.$M=t.getMonth(),this.$D=t.getDate(),this.$W=t.getDay(),this.$H=t.getHours(),this.$m=t.getMinutes(),this.$s=t.getSeconds(),this.$ms=t.getMilliseconds()},m.$utils=function(){return O},m.isValid=function(){return!(this.$d.toString()===$)},m.isSame=function(t,e){var n=w(t);return this.startOf(e)<=n&&n<=this.endOf(e)},m.isAfter=function(t,e){return w(t)<this.startOf(e)},m.isBefore=function(t,e){return this.endOf(e)<w(t)},m.$g=function(t,e,n){return O.u(t)?this[e]:this.set(n,t)},m.unix=function(){return Math.floor(this.valueOf()/1e3)},m.valueOf=function(){return this.$d.getTime()},m.startOf=function(t,e){var n=this,r=!!O.u(e)||e,h=O.p(t),$=function(t,e){var i=O.w(n.$u?Date.UTC(n.$y,e,t):new Date(n.$y,e,t),n);return r?i:i.endOf(a)},l=function(t,e){return O.w(n.toDate()[t].apply(n.toDate("s"),(r?[0,0,0,0]:[23,59,59,999]).slice(e)),n)},y=this.$W,M=this.$M,m=this.$D,g="set"+(this.$u?"UTC":"");switch(h){case c:return r?$(1,0):$(31,11);case f:return r?$(1,M):$(0,M+1);case o:var v=this.$locale().weekStart||0,D=(y<v?y+7:y)-v;return $(r?m-D:m+(6-D),M);case a:case d:return l(g+"Hours",0);case u:return l(g+"Minutes",1);case s:return l(g+"Seconds",2);case i:return l(g+"Milliseconds",3);default:return this.clone()}},m.endOf=function(t){return this.startOf(t,!1)},m.$set=function(t,e){var n,o=O.p(t),h="set"+(this.$u?"UTC":""),$=(n={},n[a]=h+"Date",n[d]=h+"Date",n[f]=h+"Month",n[c]=h+"FullYear",n[u]=h+"Hours",n[s]=h+"Minutes",n[i]=h+"Seconds",n[r]=h+"Milliseconds",n)[o],l=o===a?this.$D+(e-this.$W):e;if(o===f||o===c){var y=this.clone().set(d,1);y.$d[$](l),y.init(),this.$d=y.set(d,Math.min(this.$D,y.daysInMonth())).$d}else $&&this.$d[$](l);return this.init(),this},m.set=function(t,e){return this.clone().$set(t,e)},m.get=function(t){return this[O.p(t)]()},m.add=function(r,h){var d,$=this;r=Number(r);var l=O.p(h),y=function(t){var e=w($);return O.w(e.date(e.date()+Math.round(t*r)),$)};if(l===f)return this.set(f,this.$M+r);if(l===c)return this.set(c,this.$y+r);if(l===a)return y(1);if(l===o)return y(7);var M=(d={},d[s]=e,d[u]=n,d[i]=t,d)[l]||1,m=this.$d.getTime()+r*M;return O.w(m,this)},m.subtract=function(t,e){return this.add(-1*t,e)},m.format=function(t){var e=this,n=this.$locale();if(!this.isValid())return n.invalidDate||$;var r=t||"YYYY-MM-DDTHH:mm:ssZ",i=O.z(this),s=this.$H,u=this.$m,a=this.$M,o=n.weekdays,f=n.months,h=function(t,n,i,s){return t&&(t[n]||t(e,r))||i[n].slice(0,s)},c=function(t){return O.s(s%12||12,t,"0")},d=n.meridiem||function(t,e,n){var r=t<12?"AM":"PM";return n?r.toLowerCase():r},l={YY:String(this.$y).slice(-2),YYYY:this.$y,M:a+1,MM:O.s(a+1,2,"0"),MMM:h(n.monthsShort,a,f,3),MMMM:h(f,a),D:this.$D,DD:O.s(this.$D,2,"0"),d:String(this.$W),dd:h(n.weekdaysMin,this.$W,o,2),ddd:h(n.weekdaysShort,this.$W,o,3),dddd:o[this.$W],H:String(s),HH:O.s(s,2,"0"),h:c(1),hh:c(2),a:d(s,u,!0),A:d(s,u,!1),m:String(u),mm:O.s(u,2,"0"),s:String(this.$s),ss:O.s(this.$s,2,"0"),SSS:O.s(this.$ms,3,"0"),Z:i};return r.replace(y,(function(t,e){return e||l[t]||i.replace(":","")}))},m.utcOffset=function(){return 15*-Math.round(this.$d.getTimezoneOffset()/15)},m.diff=function(r,d,$){var l,y=O.p(d),M=w(r),m=(M.utcOffset()-this.utcOffset())*e,g=this-M,v=O.m(this,M);return v=(l={},l[c]=v/12,l[f]=v,l[h]=v/3,l[o]=(g-m)/6048e5,l[a]=(g-m)/864e5,l[u]=g/n,l[s]=g/e,l[i]=g/t,l)[y]||g,$?v:O.a(v)},m.daysInMonth=function(){return this.endOf(f).$D},m.$locale=function(){return D[this.$L]},m.locale=function(t,e){if(!t)return this.$L;var n=this.clone(),r=S(t,e,!0);return r&&(n.$L=r),n},m.clone=function(){return O.w(this.$d,this)},m.toDate=function(){return new Date(this.valueOf())},m.toJSON=function(){return this.isValid()?this.toISOString():null},m.toISOString=function(){return this.$d.toISOString()},m.toString=function(){return this.$d.toUTCString()},M}(),T=_.prototype;return w.prototype=T,[["$ms",r],["$s",i],["$m",s],["$H",u],["$W",a],["$M",f],["$y",c],["$D",d]].forEach((function(t){T[t[1]]=function(e){return this.$g(e,t[0],t[1])}})),w.extend=function(t,e){return t.$i||(t(e,_,w),t.$i=!0),w},w.locale=S,w.isDayjs=p,w.unix=function(t){return w(1e3*t)},w.en=D[v],w.Ls=D,w.p={},w}));




/*include clipboard.js*/
/*!
 * clipboard.js v2.0.11
 * https://clipboardjs.com/
 *
 * Licensed MIT © Zeno Rocha
 */
!function(t,e){"object"==typeof exports&&"object"==typeof module?module.exports=e():"function"==typeof define&&define.amd?define([],e):"object"==typeof exports?exports.ClipboardJS=e():t.ClipboardJS=e()}(this,function(){return n={686:function(t,e,n){"use strict";n.d(e,{default:function(){return b}});var e=n(279),i=n.n(e),e=n(370),u=n.n(e),e=n(817),r=n.n(e);function c(t){try{return document.execCommand(t)}catch(t){return}}var a=function(t){t=r()(t);return c("cut"),t};function o(t,e){var n,o,t=(n=t,o="rtl"===document.documentElement.getAttribute("dir"),(t=document.createElement("textarea")).style.fontSize="12pt",t.style.border="0",t.style.padding="0",t.style.margin="0",t.style.position="absolute",t.style[o?"right":"left"]="-9999px",o=window.pageYOffset||document.documentElement.scrollTop,t.style.top="".concat(o,"px"),t.setAttribute("readonly",""),t.value=n,t);return e.container.appendChild(t),e=r()(t),c("copy"),t.remove(),e}var f=function(t){var e=1<arguments.length&&void 0!==arguments[1]?arguments[1]:{container:document.body},n="";return"string"==typeof t?n=o(t,e):t instanceof HTMLInputElement&&!["text","search","url","tel","password"].includes(null==t?void 0:t.type)?n=o(t.value,e):(n=r()(t),c("copy")),n};function l(t){return(l="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}var s=function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{},e=t.action,n=void 0===e?"copy":e,o=t.container,e=t.target,t=t.text;if("copy"!==n&&"cut"!==n)throw new Error('Invalid "action" value, use either "copy" or "cut"');if(void 0!==e){if(!e||"object"!==l(e)||1!==e.nodeType)throw new Error('Invalid "target" value, use a valid Element');if("copy"===n&&e.hasAttribute("disabled"))throw new Error('Invalid "target" attribute. Please use "readonly" instead of "disabled" attribute');if("cut"===n&&(e.hasAttribute("readonly")||e.hasAttribute("disabled")))throw new Error('Invalid "target" attribute. You can\'t cut text from elements with "readonly" or "disabled" attributes')}return t?f(t,{container:o}):e?"cut"===n?a(e):f(e,{container:o}):void 0};function p(t){return(p="function"==typeof Symbol&&"symbol"==typeof Symbol.iterator?function(t){return typeof t}:function(t){return t&&"function"==typeof Symbol&&t.constructor===Symbol&&t!==Symbol.prototype?"symbol":typeof t})(t)}function d(t,e){for(var n=0;n<e.length;n++){var o=e[n];o.enumerable=o.enumerable||!1,o.configurable=!0,"value"in o&&(o.writable=!0),Object.defineProperty(t,o.key,o)}}function y(t,e){return(y=Object.setPrototypeOf||function(t,e){return t.__proto__=e,t})(t,e)}function h(n){var o=function(){if("undefined"==typeof Reflect||!Reflect.construct)return!1;if(Reflect.construct.sham)return!1;if("function"==typeof Proxy)return!0;try{return Date.prototype.toString.call(Reflect.construct(Date,[],function(){})),!0}catch(t){return!1}}();return function(){var t,e=v(n);return t=o?(t=v(this).constructor,Reflect.construct(e,arguments,t)):e.apply(this,arguments),e=this,!(t=t)||"object"!==p(t)&&"function"!=typeof t?function(t){if(void 0!==t)return t;throw new ReferenceError("this hasn't been initialised - super() hasn't been called")}(e):t}}function v(t){return(v=Object.setPrototypeOf?Object.getPrototypeOf:function(t){return t.__proto__||Object.getPrototypeOf(t)})(t)}function m(t,e){t="data-clipboard-".concat(t);if(e.hasAttribute(t))return e.getAttribute(t)}var b=function(){!function(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function");t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,writable:!0,configurable:!0}}),e&&y(t,e)}(r,i());var t,e,n,o=h(r);function r(t,e){var n;return function(t){if(!(t instanceof r))throw new TypeError("Cannot call a class as a function")}(this),(n=o.call(this)).resolveOptions(e),n.listenClick(t),n}return t=r,n=[{key:"copy",value:function(t){var e=1<arguments.length&&void 0!==arguments[1]?arguments[1]:{container:document.body};return f(t,e)}},{key:"cut",value:function(t){return a(t)}},{key:"isSupported",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:["copy","cut"],t="string"==typeof t?[t]:t,e=!!document.queryCommandSupported;return t.forEach(function(t){e=e&&!!document.queryCommandSupported(t)}),e}}],(e=[{key:"resolveOptions",value:function(){var t=0<arguments.length&&void 0!==arguments[0]?arguments[0]:{};this.action="function"==typeof t.action?t.action:this.defaultAction,this.target="function"==typeof t.target?t.target:this.defaultTarget,this.text="function"==typeof t.text?t.text:this.defaultText,this.container="object"===p(t.container)?t.container:document.body}},{key:"listenClick",value:function(t){var e=this;this.listener=u()(t,"click",function(t){return e.onClick(t)})}},{key:"onClick",value:function(t){var e=t.delegateTarget||t.currentTarget,n=this.action(e)||"copy",t=s({action:n,container:this.container,target:this.target(e),text:this.text(e)});this.emit(t?"success":"error",{action:n,text:t,trigger:e,clearSelection:function(){e&&e.focus(),window.getSelection().removeAllRanges()}})}},{key:"defaultAction",value:function(t){return m("action",t)}},{key:"defaultTarget",value:function(t){t=m("target",t);if(t)return document.querySelector(t)}},{key:"defaultText",value:function(t){return m("text",t)}},{key:"destroy",value:function(){this.listener.destroy()}}])&&d(t.prototype,e),n&&d(t,n),r}()},828:function(t){var e;"undefined"==typeof Element||Element.prototype.matches||((e=Element.prototype).matches=e.matchesSelector||e.mozMatchesSelector||e.msMatchesSelector||e.oMatchesSelector||e.webkitMatchesSelector),t.exports=function(t,e){for(;t&&9!==t.nodeType;){if("function"==typeof t.matches&&t.matches(e))return t;t=t.parentNode}}},438:function(t,e,n){var u=n(828);function i(t,e,n,o,r){var i=function(e,n,t,o){return function(t){t.delegateTarget=u(t.target,n),t.delegateTarget&&o.call(e,t)}}.apply(this,arguments);return t.addEventListener(n,i,r),{destroy:function(){t.removeEventListener(n,i,r)}}}t.exports=function(t,e,n,o,r){return"function"==typeof t.addEventListener?i.apply(null,arguments):"function"==typeof n?i.bind(null,document).apply(null,arguments):("string"==typeof t&&(t=document.querySelectorAll(t)),Array.prototype.map.call(t,function(t){return i(t,e,n,o,r)}))}},879:function(t,n){n.node=function(t){return void 0!==t&&t instanceof HTMLElement&&1===t.nodeType},n.nodeList=function(t){var e=Object.prototype.toString.call(t);return void 0!==t&&("[object NodeList]"===e||"[object HTMLCollection]"===e)&&"length"in t&&(0===t.length||n.node(t[0]))},n.string=function(t){return"string"==typeof t||t instanceof String},n.fn=function(t){return"[object Function]"===Object.prototype.toString.call(t)}},370:function(t,e,n){var f=n(879),l=n(438);t.exports=function(t,e,n){if(!t&&!e&&!n)throw new Error("Missing required arguments");if(!f.string(e))throw new TypeError("Second argument must be a String");if(!f.fn(n))throw new TypeError("Third argument must be a Function");if(f.node(t))return c=e,a=n,(u=t).addEventListener(c,a),{destroy:function(){u.removeEventListener(c,a)}};if(f.nodeList(t))return o=t,r=e,i=n,Array.prototype.forEach.call(o,function(t){t.addEventListener(r,i)}),{destroy:function(){Array.prototype.forEach.call(o,function(t){t.removeEventListener(r,i)})}};if(f.string(t))return t=t,e=e,n=n,l(document.body,t,e,n);throw new TypeError("First argument must be a String, HTMLElement, HTMLCollection, or NodeList");var o,r,i,u,c,a}},817:function(t){t.exports=function(t){var e,n="SELECT"===t.nodeName?(t.focus(),t.value):"INPUT"===t.nodeName||"TEXTAREA"===t.nodeName?((e=t.hasAttribute("readonly"))||t.setAttribute("readonly",""),t.select(),t.setSelectionRange(0,t.value.length),e||t.removeAttribute("readonly"),t.value):(t.hasAttribute("contenteditable")&&t.focus(),n=window.getSelection(),(e=document.createRange()).selectNodeContents(t),n.removeAllRanges(),n.addRange(e),n.toString());return n}},279:function(t){function e(){}e.prototype={on:function(t,e,n){var o=this.e||(this.e={});return(o[t]||(o[t]=[])).push({fn:e,ctx:n}),this},once:function(t,e,n){var o=this;function r(){o.off(t,r),e.apply(n,arguments)}return r._=e,this.on(t,r,n)},emit:function(t){for(var e=[].slice.call(arguments,1),n=((this.e||(this.e={}))[t]||[]).slice(),o=0,r=n.length;o<r;o++)n[o].fn.apply(n[o].ctx,e);return this},off:function(t,e){var n=this.e||(this.e={}),o=n[t],r=[];if(o&&e)for(var i=0,u=o.length;i<u;i++)o[i].fn!==e&&o[i].fn._!==e&&r.push(o[i]);return r.length?n[t]=r:delete n[t],this}},t.exports=e,t.exports.TinyEmitter=e}},r={},o.n=function(t){var e=t&&t.__esModule?function(){return t.default}:function(){return t};return o.d(e,{a:e}),e},o.d=function(t,e){for(var n in e)o.o(e,n)&&!o.o(t,n)&&Object.defineProperty(t,n,{enumerable:!0,get:e[n]})},o.o=function(t,e){return Object.prototype.hasOwnProperty.call(t,e)},o(686).default;function o(t){if(r[t])return r[t].exports;var e=r[t]={exports:{}};return n[t](e,e.exports,o),e.exports}var n,r});

/*import https://github.com/KeeeX/qrcodejs 页面二维码*/
var QRCode;!function(){function t(t){this.mode=r.MODE_8BIT_BYTE,this.data=t,this.parsedData=[];for(var e=0,o=this.data.length;e<o;e++){var i=[],n=this.data.charCodeAt(e);n>65536?(i[0]=240|(1835008&n)>>>18,i[1]=128|(258048&n)>>>12,i[2]=128|(4032&n)>>>6,i[3]=128|63&n):n>2048?(i[0]=224|(61440&n)>>>12,i[1]=128|(4032&n)>>>6,i[2]=128|63&n):n>128?(i[0]=192|(1984&n)>>>6,i[1]=128|63&n):i[0]=n,this.parsedData.push(i)}this.parsedData=Array.prototype.concat.apply([],this.parsedData),this.parsedData.length!=this.data.length&&(this.parsedData.unshift(191),this.parsedData.unshift(187),this.parsedData.unshift(239))}function e(t,e){this.typeNumber=t,this.errorCorrectLevel=e,this.modules=null,this.moduleCount=0,this.dataCache=null,this.dataList=[]}t.prototype={getLength:function(t){return this.parsedData.length},write:function(t){for(var e=0,r=this.parsedData.length;e<r;e++)t.put(this.parsedData[e],8)}},e.prototype={addData:function(e){var r=new t(e);this.dataList.push(r),this.dataCache=null},isDark:function(t,e){if(t<0||this.moduleCount<=t||e<0||this.moduleCount<=e)throw new Error(t+","+e);return this.modules[t][e]},getModuleCount:function(){return this.moduleCount},make:function(){this.makeImpl(!1,this.getBestMaskPattern())},makeImpl:function(t,r){this.moduleCount=4*this.typeNumber+17,this.modules=new Array(this.moduleCount);for(var o=0;o<this.moduleCount;o++){this.modules[o]=new Array(this.moduleCount);for(var i=0;i<this.moduleCount;i++)this.modules[o][i]=null}this.setupPositionProbePattern(0,0),this.setupPositionProbePattern(this.moduleCount-7,0),this.setupPositionProbePattern(0,this.moduleCount-7),this.setupPositionAdjustPattern(),this.setupTimingPattern(),this.setupTypeInfo(t,r),this.typeNumber>=7&&this.setupTypeNumber(t),null==this.dataCache&&(this.dataCache=e.createData(this.typeNumber,this.errorCorrectLevel,this.dataList)),this.mapData(this.dataCache,r)},setupPositionProbePattern:function(t,e){for(var r=-1;r<=7;r++)if(!(t+r<=-1||this.moduleCount<=t+r))for(var o=-1;o<=7;o++)e+o<=-1||this.moduleCount<=e+o||(this.modules[t+r][e+o]=0<=r&&r<=6&&(0==o||6==o)||0<=o&&o<=6&&(0==r||6==r)||2<=r&&r<=4&&2<=o&&o<=4)},getBestMaskPattern:function(){for(var t=0,e=0,r=0;r<8;r++){this.makeImpl(!0,r);var o=g.getLostPoint(this);(0==r||t>o)&&(t=o,e=r)}return e},createMovieClip:function(t,e,r){var o=t.createEmptyMovieClip(e,r);this.make();for(var i=0;i<this.modules.length;i++)for(var n=1*i,a=0;a<this.modules[i].length;a++){var s=1*a;this.modules[i][a]&&(o.beginFill(0,100),o.moveTo(s,n),o.lineTo(s+1,n),o.lineTo(s+1,n+1),o.lineTo(s,n+1),o.endFill())}return o},setupTimingPattern:function(){for(var t=8;t<this.moduleCount-8;t++)null==this.modules[t][6]&&(this.modules[t][6]=t%2==0);for(var e=8;e<this.moduleCount-8;e++)null==this.modules[6][e]&&(this.modules[6][e]=e%2==0)},setupPositionAdjustPattern:function(){for(var t=g.getPatternPosition(this.typeNumber),e=0;e<t.length;e++)for(var r=0;r<t.length;r++){var o=t[e],i=t[r];if(null==this.modules[o][i])for(var n=-2;n<=2;n++)for(var a=-2;a<=2;a++)this.modules[o+n][i+a]=-2==n||2==n||-2==a||2==a||0==n&&0==a}},setupTypeNumber:function(t){for(var e=g.getBCHTypeNumber(this.typeNumber),r=0;r<18;r++){var o=!t&&1==(e>>r&1);this.modules[Math.floor(r/3)][r%3+this.moduleCount-8-3]=o}for(r=0;r<18;r++){o=!t&&1==(e>>r&1);this.modules[r%3+this.moduleCount-8-3][Math.floor(r/3)]=o}},setupTypeInfo:function(t,e){for(var r=this.errorCorrectLevel<<3|e,o=g.getBCHTypeInfo(r),i=0;i<15;i++){var n=!t&&1==(o>>i&1);i<6?this.modules[i][8]=n:i<8?this.modules[i+1][8]=n:this.modules[this.moduleCount-15+i][8]=n}for(i=0;i<15;i++){n=!t&&1==(o>>i&1);i<8?this.modules[8][this.moduleCount-i-1]=n:i<9?this.modules[8][15-i-1+1]=n:this.modules[8][15-i-1]=n}this.modules[this.moduleCount-8][8]=!t},mapData:function(t,e){for(var r=-1,o=this.moduleCount-1,i=7,n=0,a=this.moduleCount-1;a>0;a-=2)for(6==a&&a--;;){for(var s=0;s<2;s++)if(null==this.modules[o][a-s]){var h=!1;n<t.length&&(h=1==(t[n]>>>i&1)),g.getMask(e,o,a-s)&&(h=!h),this.modules[o][a-s]=h,-1==--i&&(n++,i=7)}if((o+=r)<0||this.moduleCount<=o){o-=r,r=-r;break}}}},e.PAD0=236,e.PAD1=17,e.createData=function(t,r,o){for(var i=m.getRSBlocks(t,r),n=new _,a=0;a<o.length;a++){var s=o[a];n.put(s.mode,4),n.put(s.getLength(),g.getLengthInBits(s.mode,t)),s.write(n)}var h=0;for(a=0;a<i.length;a++)h+=i[a].dataCount;if(n.getLengthInBits()>8*h)throw new Error("code length overflow. ("+n.getLengthInBits()+">"+8*h+")");for(n.getLengthInBits()+4<=8*h&&n.put(0,4);n.getLengthInBits()%8!=0;)n.putBit(!1);for(;!(n.getLengthInBits()>=8*h||(n.put(e.PAD0,8),n.getLengthInBits()>=8*h));)n.put(e.PAD1,8);return e.createBytes(n,i)},e.createBytes=function(t,e){for(var r=0,o=0,i=0,n=new Array(e.length),a=new Array(e.length),s=0;s<e.length;s++){var h=e[s].dataCount,l=e[s].totalCount-h;o=Math.max(o,h),i=Math.max(i,l),n[s]=new Array(h);for(var u=0;u<n[s].length;u++)n[s][u]=255&t.buffer[u+r];r+=h;var f=g.getErrorCorrectPolynomial(l),d=new p(n[s],f.getLength()-1).mod(f);a[s]=new Array(f.getLength()-1);for(u=0;u<a[s].length;u++){var c=u+d.getLength()-a[s].length;a[s][u]=c>=0?d.get(c):0}}var m=0;for(u=0;u<e.length;u++)m+=e[u].totalCount;var _=new Array(m),v=0;for(u=0;u<o;u++)for(s=0;s<e.length;s++)u<n[s].length&&(_[v++]=n[s][u]);for(u=0;u<i;u++)for(s=0;s<e.length;s++)u<a[s].length&&(_[v++]=a[s][u]);return _};for(var r={MODE_NUMBER:1,MODE_ALPHA_NUM:2,MODE_8BIT_BYTE:4,MODE_KANJI:8},o={L:1,M:0,Q:3,H:2},i=0,n=1,a=2,s=3,h=4,l=5,u=6,f=7,g={PATTERN_POSITION_TABLE:[[],[6,18],[6,22],[6,26],[6,30],[6,34],[6,22,38],[6,24,42],[6,26,46],[6,28,50],[6,30,54],[6,32,58],[6,34,62],[6,26,46,66],[6,26,48,70],[6,26,50,74],[6,30,54,78],[6,30,56,82],[6,30,58,86],[6,34,62,90],[6,28,50,72,94],[6,26,50,74,98],[6,30,54,78,102],[6,28,54,80,106],[6,32,58,84,110],[6,30,58,86,114],[6,34,62,90,118],[6,26,50,74,98,122],[6,30,54,78,102,126],[6,26,52,78,104,130],[6,30,56,82,108,134],[6,34,60,86,112,138],[6,30,58,86,114,142],[6,34,62,90,118,146],[6,30,54,78,102,126,150],[6,24,50,76,102,128,154],[6,28,54,80,106,132,158],[6,32,58,84,110,136,162],[6,26,54,82,110,138,166],[6,30,58,86,114,142,170]],G15:1335,G18:7973,G15_MASK:21522,getBCHTypeInfo:function(t){for(var e=t<<10;g.getBCHDigit(e)-g.getBCHDigit(g.G15)>=0;)e^=g.G15<<g.getBCHDigit(e)-g.getBCHDigit(g.G15);return(t<<10|e)^g.G15_MASK},getBCHTypeNumber:function(t){for(var e=t<<12;g.getBCHDigit(e)-g.getBCHDigit(g.G18)>=0;)e^=g.G18<<g.getBCHDigit(e)-g.getBCHDigit(g.G18);return t<<12|e},getBCHDigit:function(t){for(var e=0;0!=t;)e++,t>>>=1;return e},getPatternPosition:function(t){return g.PATTERN_POSITION_TABLE[t-1]},getMask:function(t,e,r){switch(t){case i:return(e+r)%2==0;case n:return e%2==0;case a:return r%3==0;case s:return(e+r)%3==0;case h:return(Math.floor(e/2)+Math.floor(r/3))%2==0;case l:return e*r%2+e*r%3==0;case u:return(e*r%2+e*r%3)%2==0;case f:return(e*r%3+(e+r)%2)%2==0;default:throw new Error("bad maskPattern:"+t)}},getErrorCorrectPolynomial:function(t){for(var e=new p([1],0),r=0;r<t;r++)e=e.multiply(new p([1,d.gexp(r)],0));return e},getLengthInBits:function(t,e){if(1<=e&&e<10)switch(t){case r.MODE_NUMBER:return 10;case r.MODE_ALPHA_NUM:return 9;case r.MODE_8BIT_BYTE:case r.MODE_KANJI:return 8;default:throw new Error("mode:"+t)}else if(e<27)switch(t){case r.MODE_NUMBER:return 12;case r.MODE_ALPHA_NUM:return 11;case r.MODE_8BIT_BYTE:return 16;case r.MODE_KANJI:return 10;default:throw new Error("mode:"+t)}else{if(!(e<41))throw new Error("type:"+e);switch(t){case r.MODE_NUMBER:return 14;case r.MODE_ALPHA_NUM:return 13;case r.MODE_8BIT_BYTE:return 16;case r.MODE_KANJI:return 12;default:throw new Error("mode:"+t)}}},getLostPoint:function(t){for(var e=t.getModuleCount(),r=0,o=0;o<e;o++)for(var i=0;i<e;i++){for(var n=0,a=t.isDark(o,i),s=-1;s<=1;s++)if(!(o+s<0||e<=o+s))for(var h=-1;h<=1;h++)i+h<0||e<=i+h||0==s&&0==h||a==t.isDark(o+s,i+h)&&n++;n>5&&(r+=3+n-5)}for(o=0;o<e-1;o++)for(i=0;i<e-1;i++){var l=0;t.isDark(o,i)&&l++,t.isDark(o+1,i)&&l++,t.isDark(o,i+1)&&l++,t.isDark(o+1,i+1)&&l++,0!=l&&4!=l||(r+=3)}for(o=0;o<e;o++)for(i=0;i<e-6;i++)t.isDark(o,i)&&!t.isDark(o,i+1)&&t.isDark(o,i+2)&&t.isDark(o,i+3)&&t.isDark(o,i+4)&&!t.isDark(o,i+5)&&t.isDark(o,i+6)&&(r+=40);for(i=0;i<e;i++)for(o=0;o<e-6;o++)t.isDark(o,i)&&!t.isDark(o+1,i)&&t.isDark(o+2,i)&&t.isDark(o+3,i)&&t.isDark(o+4,i)&&!t.isDark(o+5,i)&&t.isDark(o+6,i)&&(r+=40);var u=0;for(i=0;i<e;i++)for(o=0;o<e;o++)t.isDark(o,i)&&u++;return r+=10*(Math.abs(100*u/e/e-50)/5)}},d={glog:function(t){if(t<1)throw new Error("glog("+t+")");return d.LOG_TABLE[t]},gexp:function(t){for(;t<0;)t+=255;for(;t>=256;)t-=255;return d.EXP_TABLE[t]},EXP_TABLE:new Array(256),LOG_TABLE:new Array(256)},c=0;c<8;c++)d.EXP_TABLE[c]=1<<c;for(c=8;c<256;c++)d.EXP_TABLE[c]=d.EXP_TABLE[c-4]^d.EXP_TABLE[c-5]^d.EXP_TABLE[c-6]^d.EXP_TABLE[c-8];for(c=0;c<255;c++)d.LOG_TABLE[d.EXP_TABLE[c]]=c;function p(t,e){if(void 0==t.length)throw new Error(t.length+"/"+e);for(var r=0;r<t.length&&0==t[r];)r++;this.num=new Array(t.length-r+e);for(var o=0;o<t.length-r;o++)this.num[o]=t[o+r]}function m(t,e){this.totalCount=t,this.dataCount=e}function _(){this.buffer=[],this.length=0}p.prototype={get:function(t){return this.num[t]},getLength:function(){return this.num.length},multiply:function(t){for(var e=new Array(this.getLength()+t.getLength()-1),r=0;r<this.getLength();r++)for(var o=0;o<t.getLength();o++)e[r+o]^=d.gexp(d.glog(this.get(r))+d.glog(t.get(o)));return new p(e,0)},mod:function(t){if(this.getLength()-t.getLength()<0)return this;for(var e=d.glog(this.get(0))-d.glog(t.get(0)),r=new Array(this.getLength()),o=0;o<this.getLength();o++)r[o]=this.get(o);for(o=0;o<t.getLength();o++)r[o]^=d.gexp(d.glog(t.get(o))+e);return new p(r,0).mod(t)}},m.RS_BLOCK_TABLE=[[1,26,19],[1,26,16],[1,26,13],[1,26,9],[1,44,34],[1,44,28],[1,44,22],[1,44,16],[1,70,55],[1,70,44],[2,35,17],[2,35,13],[1,100,80],[2,50,32],[2,50,24],[4,25,9],[1,134,108],[2,67,43],[2,33,15,2,34,16],[2,33,11,2,34,12],[2,86,68],[4,43,27],[4,43,19],[4,43,15],[2,98,78],[4,49,31],[2,32,14,4,33,15],[4,39,13,1,40,14],[2,121,97],[2,60,38,2,61,39],[4,40,18,2,41,19],[4,40,14,2,41,15],[2,146,116],[3,58,36,2,59,37],[4,36,16,4,37,17],[4,36,12,4,37,13],[2,86,68,2,87,69],[4,69,43,1,70,44],[6,43,19,2,44,20],[6,43,15,2,44,16],[4,101,81],[1,80,50,4,81,51],[4,50,22,4,51,23],[3,36,12,8,37,13],[2,116,92,2,117,93],[6,58,36,2,59,37],[4,46,20,6,47,21],[7,42,14,4,43,15],[4,133,107],[8,59,37,1,60,38],[8,44,20,4,45,21],[12,33,11,4,34,12],[3,145,115,1,146,116],[4,64,40,5,65,41],[11,36,16,5,37,17],[11,36,12,5,37,13],[5,109,87,1,110,88],[5,65,41,5,66,42],[5,54,24,7,55,25],[11,36,12,7,37,13],[5,122,98,1,123,99],[7,73,45,3,74,46],[15,43,19,2,44,20],[3,45,15,13,46,16],[1,135,107,5,136,108],[10,74,46,1,75,47],[1,50,22,15,51,23],[2,42,14,17,43,15],[5,150,120,1,151,121],[9,69,43,4,70,44],[17,50,22,1,51,23],[2,42,14,19,43,15],[3,141,113,4,142,114],[3,70,44,11,71,45],[17,47,21,4,48,22],[9,39,13,16,40,14],[3,135,107,5,136,108],[3,67,41,13,68,42],[15,54,24,5,55,25],[15,43,15,10,44,16],[4,144,116,4,145,117],[17,68,42],[17,50,22,6,51,23],[19,46,16,6,47,17],[2,139,111,7,140,112],[17,74,46],[7,54,24,16,55,25],[34,37,13],[4,151,121,5,152,122],[4,75,47,14,76,48],[11,54,24,14,55,25],[16,45,15,14,46,16],[6,147,117,4,148,118],[6,73,45,14,74,46],[11,54,24,16,55,25],[30,46,16,2,47,17],[8,132,106,4,133,107],[8,75,47,13,76,48],[7,54,24,22,55,25],[22,45,15,13,46,16],[10,142,114,2,143,115],[19,74,46,4,75,47],[28,50,22,6,51,23],[33,46,16,4,47,17],[8,152,122,4,153,123],[22,73,45,3,74,46],[8,53,23,26,54,24],[12,45,15,28,46,16],[3,147,117,10,148,118],[3,73,45,23,74,46],[4,54,24,31,55,25],[11,45,15,31,46,16],[7,146,116,7,147,117],[21,73,45,7,74,46],[1,53,23,37,54,24],[19,45,15,26,46,16],[5,145,115,10,146,116],[19,75,47,10,76,48],[15,54,24,25,55,25],[23,45,15,25,46,16],[13,145,115,3,146,116],[2,74,46,29,75,47],[42,54,24,1,55,25],[23,45,15,28,46,16],[17,145,115],[10,74,46,23,75,47],[10,54,24,35,55,25],[19,45,15,35,46,16],[17,145,115,1,146,116],[14,74,46,21,75,47],[29,54,24,19,55,25],[11,45,15,46,46,16],[13,145,115,6,146,116],[14,74,46,23,75,47],[44,54,24,7,55,25],[59,46,16,1,47,17],[12,151,121,7,152,122],[12,75,47,26,76,48],[39,54,24,14,55,25],[22,45,15,41,46,16],[6,151,121,14,152,122],[6,75,47,34,76,48],[46,54,24,10,55,25],[2,45,15,64,46,16],[17,152,122,4,153,123],[29,74,46,14,75,47],[49,54,24,10,55,25],[24,45,15,46,46,16],[4,152,122,18,153,123],[13,74,46,32,75,47],[48,54,24,14,55,25],[42,45,15,32,46,16],[20,147,117,4,148,118],[40,75,47,7,76,48],[43,54,24,22,55,25],[10,45,15,67,46,16],[19,148,118,6,149,119],[18,75,47,31,76,48],[34,54,24,34,55,25],[20,45,15,61,46,16]],m.getRSBlocks=function(t,e){var r=m.getRsBlockTable(t,e);if(void 0==r)throw new Error("bad rs block @ typeNumber:"+t+"/errorCorrectLevel:"+e);for(var o=r.length/3,i=[],n=0;n<o;n++)for(var a=r[3*n+0],s=r[3*n+1],h=r[3*n+2],l=0;l<a;l++)i.push(new m(s,h));return i},m.getRsBlockTable=function(t,e){switch(e){case o.L:return m.RS_BLOCK_TABLE[4*(t-1)+0];case o.M:return m.RS_BLOCK_TABLE[4*(t-1)+1];case o.Q:return m.RS_BLOCK_TABLE[4*(t-1)+2];case o.H:return m.RS_BLOCK_TABLE[4*(t-1)+3];default:return}},_.prototype={get:function(t){var e=Math.floor(t/8);return 1==(this.buffer[e]>>>7-t%8&1)},put:function(t,e){for(var r=0;r<e;r++)this.putBit(1==(t>>>e-r-1&1))},getLengthInBits:function(){return this.length},putBit:function(t){var e=Math.floor(this.length/8);this.buffer.length<=e&&this.buffer.push(0),t&&(this.buffer[e]|=128>>>this.length%8),this.length++}};var v=[[17,14,11,7],[32,26,20,14],[53,42,32,24],[78,62,46,34],[106,84,60,44],[134,106,74,58],[154,122,86,64],[192,152,108,84],[230,180,130,98],[271,213,151,119],[321,251,177,137],[367,287,203,155],[425,331,241,177],[458,362,258,194],[520,412,292,220],[586,450,322,250],[644,504,364,280],[718,560,394,310],[792,624,442,338],[858,666,482,382],[929,711,509,403],[1003,779,565,439],[1091,857,611,461],[1171,911,661,511],[1273,997,715,535],[1367,1059,751,593],[1465,1125,805,625],[1528,1190,868,658],[1628,1264,908,698],[1732,1370,982,742],[1840,1452,1030,790],[1952,1538,1112,842],[2068,1628,1168,898],[2188,1722,1228,958],[2303,1809,1283,983],[2431,1911,1351,1051],[2563,1989,1423,1093],[2699,2099,1499,1139],[2809,2213,1579,1219],[2953,2331,1663,1273]];function C(){var t=!1,e=navigator.userAgent;if(/android/i.test(e)){t=!0;var r=e.toString().match(/android ([0-9]\.[0-9])/i);r&&r[1]&&(t=parseFloat(r[1]))}return t}var w=function(){var t=function(t,e){this._el=t,this._htOption=e};return t.prototype.draw=function(t){var e=this._htOption,r=this._el,o=t.getModuleCount();Math.floor(e.width/o),Math.floor(e.height/o);function i(t,e){var r=document.createElementNS("http://www.w3.org/2000/svg",t);for(var o in e)e.hasOwnProperty(o)&&r.setAttribute(o,e[o]);return r}this.clear();var n=i("svg",{viewBox:"0 0 "+String(o)+" "+String(o),width:"100%",height:"100%",fill:e.colorLight});n.setAttributeNS("http://www.w3.org/2000/xmlns/","xmlns:xlink","http://www.w3.org/1999/xlink"),r.appendChild(n),n.appendChild(i("rect",{fill:e.colorLight,width:"100%",height:"100%"})),n.appendChild(i("rect",{fill:e.colorDark,width:"1",height:"1",id:"template"}));for(var a=0;a<o;a++)for(var s=0;s<o;s++)if(t.isDark(a,s)){var h=i("use",{x:String(s),y:String(a)});h.setAttributeNS("http://www.w3.org/1999/xlink","href","#template"),n.appendChild(h)}},t.prototype.clear=function(){for(;this._el.hasChildNodes();)this._el.removeChild(this._el.lastChild)},t}(),D="svg"===document.documentElement.tagName.toLowerCase()?w:"undefined"==typeof CanvasRenderingContext2D?function(){var t=function(t,e){this._el=t,this._htOption=e};return t.prototype.draw=function(t){for(var e=this._htOption,r=this._el,o=t.getModuleCount(),i=Math.floor(e.width/o),n=Math.floor(e.height/o),a=['<table style="border:0;border-collapse:collapse;">'],s=0;s<o;s++){a.push("<tr>");for(var h=0;h<o;h++)a.push('<td style="border:0;border-collapse:collapse;padding:0;margin:0;width:'+i+"px;height:"+n+"px;background-color:"+(t.isDark(s,h)?e.colorDark:e.colorLight)+';"></td>');a.push("</tr>")}a.push("</table>"),r.innerHTML=a.join("");var l=r.childNodes[0],u=(e.width-l.offsetWidth)/2,f=(e.height-l.offsetHeight)/2;u>0&&f>0&&(l.style.margin=f+"px "+u+"px")},t.prototype.clear=function(){this._el.innerHTML=""},t}():function(){function t(){this._elImage.src=this._elCanvas.toDataURL("image/png"),this._elImage.style.display="block",this._elCanvas.style.display="none"}if(this._android&&this._android<=2.1){var e=1/window.devicePixelRatio,r=CanvasRenderingContext2D.prototype.drawImage;CanvasRenderingContext2D.prototype.drawImage=function(t,o,i,n,a,s,h,l,u){if("nodeName"in t&&/img/i.test(t.nodeName))for(var f=arguments.length-1;f>=1;f--)arguments[f]=arguments[f]*e;else void 0===l&&(arguments[1]*=e,arguments[2]*=e,arguments[3]*=e,arguments[4]*=e);r.apply(this,arguments)}}var o=function(t,e){this._bIsPainted=!1,this._android=C(),this._htOption=e,this._elCanvas=document.createElement("canvas"),this._elCanvas.width=e.width,this._elCanvas.height=e.height,t.appendChild(this._elCanvas),this._el=t,this._oContext=this._elCanvas.getContext("2d"),this._bIsPainted=!1,this._elImage=document.createElement("img"),this._elImage.alt="Scan me!",this._elImage.style.display="none",this._el.appendChild(this._elImage),this._bSupportDataURI=null};return o.prototype.draw=function(t){var e=this._elImage,r=this._oContext,o=this._htOption,i=t.getModuleCount(),n=o.width/i,a=o.height/i,s=Math.round(n),h=Math.round(a);e.style.display="none",this.clear();for(var l=0;l<i;l++)for(var u=0;u<i;u++){var f=t.isDark(l,u),g=u*n,d=l*a;r.strokeStyle=f?o.colorDark:o.colorLight,r.lineWidth=1,r.fillStyle=f?o.colorDark:o.colorLight,r.fillRect(g,d,n,a),r.strokeRect(Math.floor(g)+.5,Math.floor(d)+.5,s,h),r.strokeRect(Math.ceil(g)-.5,Math.ceil(d)-.5,s,h)}this._bIsPainted=!0},o.prototype.makeImage=function(){this._bIsPainted&&function(t,e){var r=this;if(r._fFail=e,r._fSuccess=t,null===r._bSupportDataURI){var o=document.createElement("img"),i=function(){r._bSupportDataURI=!1,r._fFail&&r._fFail.call(r)};return o.onabort=i,o.onerror=i,o.onload=function(){r._bSupportDataURI=!0,r._fSuccess&&r._fSuccess.call(r)},void(o.src="data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==")}!0===r._bSupportDataURI&&r._fSuccess?r._fSuccess.call(r):!1===r._bSupportDataURI&&r._fFail&&r._fFail.call(r)}.call(this,t)},o.prototype.isPainted=function(){return this._bIsPainted},o.prototype.clear=function(){this._oContext.clearRect(0,0,this._elCanvas.width,this._elCanvas.height),this._bIsPainted=!1},o.prototype.round=function(t){return t?Math.floor(1e3*t)/1e3:t},o}();function A(t,e){for(var r=1,i=function(t){var e=encodeURI(t).toString().replace(/\%[0-9a-fA-F]{2}/g,"a");return e.length+(e.length!=t?3:0)}(t),n=0,a=v.length;n<=a;n++){var s=0;switch(e){case o.L:s=v[n][0];break;case o.M:s=v[n][1];break;case o.Q:s=v[n][2];break;case o.H:s=v[n][3]}if(i<=s)break;r++}if(r>v.length)throw new Error("Too long data");return r}(QRCode=function(t,e){if(this._htOption={width:256,height:256,typeNumber:4,colorDark:"#000000",colorLight:"#ffffff",correctLevel:o.H},"string"==typeof e&&(e={text:e}),e)for(var r in e)this._htOption[r]=e[r];"string"==typeof t&&(t=document.getElementById(t)),this._htOption.useSVG&&(D=w),this._android=C(),this._el=t,this._oQRCode=null,this._oDrawing=new D(this._el,this._htOption),this._htOption.text&&this.makeCode(this._htOption.text)}).prototype.makeCode=function(t){this._oQRCode=new e(A(t,this._htOption.correctLevel),this._htOption.correctLevel),this._oQRCode.addData(t),this._oQRCode.make(),this._el.title=t,this._oDrawing.draw(this._oQRCode),this.makeImage()},QRCode.prototype.makeImage=function(){"function"==typeof this._oDrawing.makeImage&&(!this._android||this._android>=3)&&this._oDrawing.makeImage()},QRCode.prototype.clear=function(){this._oDrawing.clear()},QRCode.CorrectLevel=o}(),"undefined"!=typeof module&&(module.exports=QRCode);


dayjs().format();
if(window.location.href.search('file:///') != -1 ){
    document.title = '离线导出版本: ' + document.title;
}
if(window.location.href.search('haudi.top') != -1  ){
    document.title = 'X-site Dev: ' + document.title;
}
var oMeta = document.createElement('meta');
oMeta.content='catsnyne_gmail_com,amos';
oMeta.name='author';
var pMeta = document.createElement('meta');
pMeta.content='IE=edge;text/html; charset=UTF-8';
pMeta.httpEquiv='X-UA-Compatible';
var qMeta = document.createElement('meta');
qMeta.charset='utf-8';
document.getElementsByTagName('head')[0].appendChild(oMeta);
document.getElementsByTagName('head')[0].appendChild(pMeta);
document.getElementsByTagName('head')[0].appendChild(qMeta);

/*清除typora导出时未删除的head style样式 */
if ( document.head.getElementsByTagName("style")[0] ){
    document.head.getElementsByTagName("style")[0].remove();
}
////content_div = document.getElementById('ctl00_cphContent_UpdatePanel1');
////document.body.appendChild(content_div);

//// document.getElementsByTagName("form")[0].remove();
////brday = document.getElementById('ctl00_cphContent_uiHoster_lblBirthday');
////brday.innerText=' NULL';
////content_write = document.getElementById('write');
////document.body.appendChild(content_write);
////content_div.remove();
////document.getElementsByTagName("link")[0].remove();

// 顶部div
var typora_image ="<div><div class='top_header'>Navigations</div><div class='top_line'>The first step is as good as half over. </div><!-- --> <div id='MainMenu'> <div id='MainMenuTop'></div> <ul> <li><a href='https://haudi.top/main/pod/pages/passwordgenerate' title='随机密码' target='_blank'><span>随机密码</span></a></li> <li><a href='https://haudi.top/geoip2' title='IP归属地查询' target='_blank'><span>IP归属地</span></a></li> <li><a class='a_clr_1' href='https://haudi.top/main/pod/pages/mb_unit_convert' title='二进制单位转换' target='_blank'><span>进制转换</span></a></li> <li><a href='https://haudi.top/navigation/' ltitle='工具导航'><span>工具导航</span></a></li>  <li><a href='/pipelines/static/sheetlist.html' ltitle='历史博客'><span>发表清单</span></a></li></ul></div></div>";
var typora_topdiv = document.createElement("div");
typora_topdiv.setAttribute('id','topdiv')
typora_topdiv.innerHTML=typora_image;
//document.getElementById('write').appendChild(typora_topdiv);

// document.getElementById('write').insertBefore(typora_topdiv,document.getElementById('write').getElementsByTagName("p")[0]);
//document.getElementsByClassName('typora-export-content')[0].insertBefore(typora_topdiv,document.getElementById('write'));
//document.getElementsByClassName('typora-export-content')[0].insertBefore(typora_topdiv,document.getElementById('write'));
document.body.insertBefore(typora_topdiv,document.getElementsByClassName('typora-export-content')[0]);


var typora_image ="<div id='js_bt'><span><a href='https://haudi.top'>Sow nothing, reap nothing.</a></span></div>";
var typora_btdiv = document.createElement("div");
typora_btdiv.innerHTML=typora_image;

if(document.querySelector('#write')){
  document.getElementById('write').appendChild(typora_btdiv);
}

document.body.appendChild(typora_btdiv);


if(window.location.href.search('haudi.top') != -1 ){
    /*二维码div占位与备案*/
    var qrc_div = '<div id="qrcode"  style="width:200px;height:200px;margin-top:15px;margin-left:auto;margin-right:auto;margin-bottom: 15px;"> </div>';
    var site_url ='<div id=><span><a target="_blank" href="http://beian.miit.gov.cn/" title=""> 豫ICP备2022027398号-1</a></span><span><a target="_blank" href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=41148102000279" title=""><img src="/res/img/beian.png" height="15" alt="豫公网安备 41148102000279号">豫公网安备 41148102000279号</a></span>' + '</div>';
}

var toc ="<div id='list_1'><div><ul><li class='list_1_a_active'><a >&#x1F4DC;readme</a></li><li class='list_1_a_gray'><a>&#x1F517;Star</a></li></ul></div><div><div id='list_2' class='select_list'><div id='tab-summary' class='topic_list'> <div><h2 style='color:black;text-align:center;'>Table of Contents</h2></div><a href='#'></a></div></div><div id='list_2_1' class='select_list tab_hidden'><div id='tab-summary1' class='topic_list'> <div><h2 style='color:black;text-align:center;'>往期清单</h2></div></div></div><div id='list_3'> " + qrc_div + " <div style='text-align:center;'>Do have faith in what you're doing </div> <div style='text-align:center;'> World is powered by solitude. </div>	  <div style='text-align:center;'> " + site_url + "</div></div>";
var md_toc_div = document.createElement("div");
md_toc_div.setAttribute('id','md_toc')
md_toc_div.innerHTML=toc;
document.body.appendChild(md_toc_div);
//document.getElementById('write').appendChild(typora_topdiv);

////document.body.insertBefore(md_toc_div,document.getElementById('write'));
document.getElementsByClassName('typora-export-content')[0].insertBefore(md_toc_div,document.getElementById('write'));

/*生成二维码,对应div在前方定义 start */

		/*官方方法 var qrcode = new QRCode(document.getElementById("qrcode"), {
			width : 200,
			height : 200
		});

		var inputField = window.location.href;

		function makeCode () {    
			if (!inputField) {
				inputField.focus();
				return;
			}

			qrcode.makeCode(inputField);
		}

		makeCode();*/
// https://www.cnblogs.com/fanlinqiang/p/11228080.html
//let tmp = document.getElementById("qrcode");
let tmp = document.createElement('div');
let qrcode = new QRCode(tmp, {
   text: window.location.href,
   width: 200,
   height: 200,
   colorDark : "#000000",
   colorLight : "#ffffff",
   useCanvas: true,
   correctLevel : QRCode.CorrectLevel.H
});
let logo = new Image();
//logo.crossOrigin = 'Anonymous';
//logl.src = 'https://cn.vuejs.org/images/logo.png';
logo.src = 'data:image/png;base64,AAABAAEAICAAAAEAIACoEAAAFgAAACgAAAAgAAAAQAAAAAEAIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoNMw4iEURsGQwyGgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaDTQEOx13zGMxxv8wGGB6AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADccbzxgMMH/ZjPM/0Yji8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWi20XGYzzP9mM8z/Tyef9hoNMwgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB3StFSZjPM/2YzzP9gMMH/HA45QgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIdf1yRwQc//ZjPM/2YzzP83G26GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkW3aAIBW1NRmM8z/ZjPM/0cjjtQZDDMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABoNMwAaDTMoGg0zPhkMMhgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAnHvebmk3zf9mM8z/Wi20/xsNNTQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAaDTMoOh10wlQqqP9cLrj/Tied/Dcbb7oaDTMsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACmieEKhFzW5mYzzP9lMsv/ORxzqAAAAAAAAAAAAAAAAAAAAAAAAAAAHw8+QksllvBlMsv/ZjPM/2YzzP9mM8z/ZTLL/0wmmfQhEEJSAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACoi+Jebz/P/2YzzP9WK6z8HA43NgAAAAAAAAAAAAAAAB8PPkBPJ5/0ZjPM/2YzzP9mM8z/ZjPM/2YzzP9mM8z/ZjPM/1IppPohEEJQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK+V5ACUcdu8ZjPM/2YzzP8+H33MGg0yBAAAAAAbDTYuTSaZ8GYzzP9mM8z/ZjPM/2c0zP9yRNDqgVfVzIRb1vRpN83/ZjPM/04nnPQbDTYyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALKY5SR/VdT0ZjPM/2Ewwv8rFVeGGQwzFkUiiuBmM8z/ZjPM/2YzzP9lM8nwajnNanlM0gYAAAAAt57mEqeK4ZxyRND/ZjPM/0EggtoaDTMMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAKiL4m5vP8//ZjPM/1crrv8+H33aZTLL/2YzzP9mM8v/YDG/2FwuuCYAAAAAAAAAAAAAAAAAAAAAuKLnAKmN4oZwQM//YzHH/zAYX5IAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAs5rlApt63bRnNMz/ZjPM/2YzzP9mM8z/ZjPL/1wut9JTKaYWAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuKDnAKGB36RpN83/VSqp/BsNNjYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtZzmFHdMzOpmM8z/ZjPM/2YzzP9ZLbLkSSSSHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtZzmCpFs2tJmM8z/Ph98yhoNMwIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAeDzs4Vyut/GYzzP9mM8z/ZjPM/z0eerYaDTMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAtJvmLn1S0/hfL77/JhNLZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGg0zAkEggspmM8z/ZjPM/2YzzP9mM8z/XC64/yQSR1oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApIbgfGs6zf9IJJDoGg0yDgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlEktYXy+//2YzzP9kMsj/e1PM6mYzzP9mM8z/SSSR6hoNMxYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACwleUGjWfZ0mQyyP8vF12KAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGg0zAkYjjNhmM8z/ZjPM/1Eposiki9cciGDX6GYzzP9lMsr/MxlnogAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACuk+Q8dkjR/FAooPYaDTMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkEkhSYDDA/2YzzP9cLrj8MxllMAAAAACvlORMdUfR/2YzzP9XK67/HQ45PAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACYddyiZjPL/zYbbJwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEQiibpmM8z/ZjPM/0UiiawAAAAAAAAAAAAAAACce96eZzTM/2YzzP8/H37MGg0zAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAK2S4xx/VNTyUCih+BoNMxoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAkEkgcVyuu/GYzzP9jMcb/JBJITAAAAAAAAAAAAAAAAK+V5BKEXNboZjPM/2AwwP8oFE9wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJ9/335oNsv/LxdffgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADkcc25lMsv/ZjPM/1MppvgaDTIKAAAAAAAAAAAAAAAAAAAAAKGC32hqOM3/ZjPM/00mm/QaDTMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAq4/jCoZe1uJIJI/eGg0zAAAAAAAAAAAAAAAAAAAAAAAAAAAAUCigsmYzzP9mM8z/TCaXzAAAAAAAAAAAAAAAAAAAAAAAAAAAo4TgCH9U1OZmM8z/ZTLL/zgccKwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqIviWmQ6uP8aDTMoAAAAAAAAAAAAAAAAAAAAAAAAAABaLbXgZjPM/2YzzP9HI42kAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAjWfZimYzzP9mM8z/VSqr/BoNMyYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACvleQAjmvTmCERQxwAAAAAAAAAAAAAAAAAAAAAAAAAAGMyxfZmM8z/ZjPM/0Ihg4oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACXdNwybj7O/2YzzP9lMsv/MBhgbAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdEbQ4mYzzP9mM8z/OBxxcgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJBr2gB8UdPgZjPM/2Uyyv8vF11gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAChgt+AcEHP/2Awwf8jEUU8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAJp43YBrOs3/Wy612CkVUwwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALmi5wKuk+RafFXKZjIZYwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAuKHnCKCA31ZuPs0SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA///////////n////4////+P////j////4f///+H////x/g//8PwH//j4A//4cAH//CDw//4B+H/+A/x//wf+P/8H/z/+B/+f/gP/j/wh/8/8cf/H+HD/5/j4//f4/H/z8Pw/+/D8P/vw/j//8f4///H+P/////////////////8=';

logo.onload = function () {
   let container = document.getElementById('qrcode');
   let qrImg = qrcode._el.getElementsByTagName('img')[0];
   let canvas = qrcode._el.getElementsByTagName('canvas')[0];
   var ctx = canvas.getContext("2d");
   ctx.drawImage(logo, 200 * 0.5 - 20, 200 * 0.5 - 20, 40, 40);
   qrImg.src = canvas.toDataURL();
   container.appendChild(qrcode._el);
   //document.getElementById('list_3').insertBefore(qrcode._el,container);
   // 销毁临时dom
   tmp = null;
   qrcode = null;
   logo = null;
   document.querySelector("#qrcode>div").setAttribute("title","扫码到手机上查看或分享.[功能实现于2023.01.26]");
}


/*生成二维码,对应div在前方定义 end */


// ############################################################

var md_toc = document.getElementById("write")


var dlist = document.getElementById("md_toc");
var dlist_left = document.getElementById("tab-summary");



var num = 0;//统计找到的mt和st

//遍历所有元素结点
		var hh11=0;/* 一个是中间变量一个是递增变量*/
		var hh22=0;
		var hh33=0;
		var hh44=0;

for(var i=0; i<md_toc.childElementCount; i++)
{ 
	//document.getElementsByClassName("md-toc")[0].children[0].children.length

	//console.log(md_toc[i].className);
	//console.log(md_toc[i].children[0].hash);


var t_name = md_toc.children[i].tagName;
// console.log(t_name);
// var str = md_toc[i].className;
var reg = RegExp(/md-toc-h/);
//console.log(reg.test(str)); // true
  if(RegExp(/H1/).test(t_name)){
	//console.log(t_name,RegExp(/H1/).test(t_name));
	var md_toc_hx = "H1";
  }else if(RegExp(/LINK/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "LINK";
  }else if(RegExp(/H2/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H2";
  }else if(RegExp(/H3/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H3";
  }else if(RegExp(/H4/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H4";
  }else{
    //console.log("else off");
	var md_toc_hx = "false";
  }

 var listnodetext = md_toc.children[i].getAttribute('id') ;
// console.log(listnodetext);
 //var listnodetext = md_toc.children[i].innerText ;
 //var listnodetext = md_toc[i].innerText ;

// var str="jfkldsjalk,.23@#!$$k~!  @#$%^&*()(_+-=|\{}[]';:,./<>??gg  g~```gf"; 
// listnodetext=listnodetext.replace("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]",""); 

var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]","g");
// 将以上匹配的字符全部清理掉。
//// listnodetext=listnodetext.replace(reg,"").toLowerCase();
// g
// listnodetext=listnodetext.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\、|\（|\、|\）|\?]/g,""); 
//// console.log(listnodetext);


//var nodeIdGet = listnodetext; 
var md_toc_side = "";
var default_none = 0;
switch(md_toc_hx)
{
	case "H1":    //若为主标题 
		item = document.createElement("dt");
								hh11 = hh11 +1 ;
								////md_toc[i].innerHTML = "§ " + hh11 + ". " + listnodetext;
                                                                var h1_str = toChinesNum(hh11) ;
                                                                if (h1_str.length >=2 && h1_str.length <= 3 && h1_str.substr(0,1) =='一') {
                                                                   md_toc_side="" + h1_str.substr(1,2);
                                                                }else {
                                                                 md_toc_side="" + toChinesNum(hh11) ;
                                                                }
								hh22 = 0 ; 
		break;
	case "H2":    //若为子标题
		item = document.createElement("dd");
								hh22 = hh22 +1 ;
								////md_toc[i].innerHTML = hh11 + "." + hh22 + ". " + listnodetext;
								md_toc_side= hh11 + "." + hh22;
								hh33 = 0; 
		break;
	case "H3":    //若为子标题
		item = document.createElement("de");
								hh33 = hh33 + 1 ;
								if (hh33 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33;
								}
								//hh33 = hh33 + 1 ;
		break;
	
	case "H4":    //若为子标题
		item = document.createElement("df");
								hh44 = hh44 + 1 ;
								if (hh44 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								   if ( hh33 == 0 ){
										   hh33 = hh33 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33 + "." + hh44;
								}
								//hh33 = hh33 + 1 ;
		break;
	default:    //若为子标题
                 default_none = 1;
                 hh11 = hh11;
                 hh22 = hh22;
                 hh33 = hh33; 
                 hh44 = hh44;
                // console.log("switch_default",default_none,"hh11:",hh11,"hh22:",hh22,"hh33:",hh33,"hh44:",hh44);
}


if (default_none == 0 ) {
// console.log("if_default_none:",default_none);
var ahreftextnode = document.createElement('a');
//var ahreftextnode= item.appendChild(a)
// ahreftextnode.setAttribute('href', "#" + nodeIdGet);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].hash);
ahreftextnode.setAttribute('href', '#' + listnodetext);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].getAttribute('href'));
//// ahreftextnode.classList.add("list-group-item");

ahreftextnode.innerHTML= "<em>" + md_toc_side + ". </em>" + listnodetext;
//ahreftextnode.innerHTML=nodetext;
// console.log(item);
item.appendChild(ahreftextnode);
item.className='dXdefault';

//	};
//将自定义表项加入自定义列表中
//dlist.appendChild(item);
//console.log(dlist_left)
dlist_left.appendChild(item);
}
num++;





// console.timeEnd("开始时间点");
//添加目录列表结束标记而矣

}

toppp = document.createElement("toppp");
enddd = document.createElement("enddd");

top_a_contents = document.createElement('a');
top_a_contents.setAttribute('href','#top');
end_a_contents = document.createElement('a');
end_a_contents.setAttribute('href','#bottom');


top_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='height:2em;background-color:#4b4033;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 0 0 4px 4px;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

end_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='margin-top:1em;padding:0.3em 0;background-color:#4baa33;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 4px 4px 0 0;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

enddd.appendChild(end_a_contents);
toppp.appendChild(top_a_contents);

 var plink = document.getElementById("post_1");
 var link = document.getElementById("post_2");
//plink.insertBefore(toppp,link);
// plink.appendChild(enddd);




//// ######################################################################

/* 保证左侧目录占用整个高度,在滚动时 */

//document.getElementById("md_toc").style.position = "fixed";  
//document.getElementById("md_toc").style.top = ( 118 - document.documentElement.scrollTop);    

function md_toc_view() {
  if (document.documentElement.scrollTop > 118) {
    document.getElementById("md_toc").style.position = "fixed";    
    document.getElementById("md_toc").style.top = "0";    
  } else {
    document.getElementById("md_toc").setAttribute('style', '');;  
    //document.getElementById("md_toc").style.position = "initial";  
  }
}



/*baidu tongji,此段将自动在head中添加一个script*/
var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?78ccf6821a7fadad00056d1bef58e558";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();
/*var _hmt = _hmt || [];
(function() {
  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?78ccf6821a7fadad00056d1bef58e558";
  var s = document.getElementsByTagName("script")[0]; 
  s.parentNode.insertBefore(hm, s);
})();*/

  /* var typora_json = document.createElement("script");
  typora_json.src = "https://haudi.top/cdn_static/typora.json";
  typora_json.type = "text/javascript";
  document.getElementsByTagName('head')[0].appendChild(typora_json);*/
  /* var check_ie = document.createElement("script");
  check_ie.src="https://haudi.top/cdn_static/check_ie_latest.js";
  document.getElementsByTagName('head')[0].appendChild(check_ie);*/




for(var i=0;i < document.getElementsByClassName('CodeMirror-code').length; i++){
   for( var j=0; j < document.getElementsByClassName('CodeMirror-code')[i].children.length ;j++ ){
      if( !(j % 2) ){
         document.getElementsByClassName('CodeMirror-code')[i].children[j].setAttribute('class','alt1')
      }else{
         document.getElementsByClassName('CodeMirror-code')[i].children[j].setAttribute('class','alt2')
      }
   }


}



    var list = document.getElementById("tab-summary").getElementsByTagName("a");
    for (var i = 1; i < list.length; i++) {
        //为li注册鼠标进入事件
        list[i].parentNode.onmouseover = function () {
            //设置其背景颜色为黄色
            //this.style.backgroundColor = "#f4ebe5";
            this.className='dXhover'; //this.parentNode.style.backgroundColor = "#f4ebe5";
        };
        //为li注册鼠标离开事件
        list[i].parentNode.onmouseout = function () {
            //恢复到这个标签默认的颜色
            //this.style.backgroundColor = "";
            this.className='dXdefault';
            //this.parentNode.style.backgroundColor = "";
        };
    }



/*标签切换*/

    var tabs =document.getElementById("list_1").getElementsByTagName("li");
    var contents =document.getElementById("md_toc").getElementsByClassName("select_list");
                //var contents = document.getElementsByClassName('tab-content')[0].getElementsByTagName('div');
    if(document.querySelector("#tab-summary").childNodes.length <= 3 ){
      tabs[1].className = 'list_1_a_active';
      contents[1].className = 'select_list tab_show';
      tabs[0].className = 'list_1_a_gray';
      contents[0].className = 'select_list tab_hidden';
    }
            (function changeTab(tab) {
                for(var i = 0, len = tabs.length; i < len; i++) {
                    tabs[i].onclick = showTab;
                }
            })();

            function showTab() {
                for(var i = 0, len = 2 ; i < len; i++) {
                    if(tabs[i] === this) {
                        //console.log("=",tabs[i]);
                        tabs[i].className = 'list_1_a_active';
                        contents[i].className = 'select_list tab_show';
                        //console.log("contents[i]",contents[i]);
                        //console.log("i = ",i);
                    } else {
                        //console.log("else",tabs[i]);
                        tabs[i].className = 'list_1_a_gray ';
                        contents[i].className = 'select_list tab_hidden';
                        //console.log("else i = ",i);
                    }
                }
            }



// 加载json列表
/*var tab_url_json = JSON.parse(tab_url);
var a_list = "";
for ( var i = 0; i < tab_url_json.length ; i++ ) {
    a_list = a_list + '<a href="' + tab_url_json[i].link_url + '">' + tab_url_json[i].link_title + '</a>';
}
//console.log(a_list);
var temp = document.getElementById('tab-summary1').getElementsByTagName('h2')[0].parentNode;
temp.innerHTML=a_list;
*/



fetch('/main/pod/imy/data/json_api_links_all.json').then(function(response) {
  return response.json().then(function(json) {
      var list_latest_publish_j_b = '';
      var list_latest_publish = '';
      var link_title_parse= '';
      var j_b= 0; // j的前一个
      var j_a= 0; // j的后一个 
      var length = json.length; 
      var mk=0;
      var tnum=0;
      var json_mk={};
      var tags='';
      var json_tags=[];
      var json_tags_same=[]; /* 相同标签 */
      //console.log(json_tags_same);
      var arr_tags=[];
      for(j=0;j<length;j++){
         if(json[j].link_root == 'markdown'){
            json_mk[mk]=json[j]; 
            mk++;
         }
         //var date = new Date(json[j].link_time);
         if(json[j].link_url.search(window.location.pathname) != -1){
             arr_tags=json[j].link_tags;
             //console.log(json[j].link_tags);
             link_title_parse=j;
             j_b=j-1;
             j_a=j+1;
             //console.log(json[j].link_url);
             //console.log(window.location.pathname);
             if(j != 0){
                 list_latest_publish_j_b = list_latest_publish + '<li class="new list list-edit" style="list-style:none;float:right;margin-right:2em;"><span class="date" style="display: ;">' + dayjs(json[j_b].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a  href="' + json[j_b].link_url + '">' + json[j_b].link_title + '</a></span><span>后一篇</span></li>';
             }else{
                 list_latest_publish_j_b = list_latest_publish + '<li class="new list list-edit" style="list-style:none;float:right;margin-right:2em;"><span class="date" style="display: ;">' + '</span><span class="title"><i class="icon-flag"></i> <a  href="/">最后一篇了，回首页看看吧</a></span></li>';
             }
             list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';
             list_latest_publish_j_a =  '<li class="new list list-edit" style="float:left;list-style:none;margin-left:2em;"><span class="date" style="display: ;">' + dayjs(json[j_a].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a href="' + json[j_a].link_url + '">' + json[j_a].link_title + '</a></span><span>前一篇</span></li>';
             //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';
         }
         //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
      }
      var tnum=0;
      //console.log(json_tags_same);
      for(j=0;j<length;j++){  /*遍历和当前链接相同标签的链接*/
          for(t=0;t<arr_tags.length;t++){ /* 将当前链接的标签对所有json进程逐个遍历 */ 
              //console.log(arr_tags[t]);
              //console.log(json.filter(function( _json){ return _json.link_tags == arr_tags[t]})); 
               // 此处需要使用=,而不是==，前者可以从指定数据中取，后者则是字符串完全一致
              //console.log(json[j].link_tags);
              //console.log(json[j].link_tags.includes(arr_tags[t]));
              //console.log(json[j].link_tags);
              //console.log(['网络分析', 'basic'].includes('basic'));
              if(json[j].link_tags.includes(arr_tags[t])){
                  //console.log("tnum:" + tnum);
                  if(eval(tnum) < eval(1)){  /* js是弱类型数值不能直接比较，会被当成字符串，而字符串有长短*/
                      //console.log(json_tags_same);
                      //console.log( json_tags_same.length);
                      //console.log(json[j]);
                      json_tags_same[tnum]=json[j]; /*  为防止一个链接的多个标签被重复遍历,作if-else判断,从第2个开始判断 */
                      //console.log("0000000000000");
                      //console.log(json_tags_same);
                      tnum++;
                  }else{
                   //console.log( json_tags_same.length);
                    if(!(json_tags_same[json_tags_same.length -1].link_title == json[j].link_title)){  /*这里只需要做一次大循环遍历与上一个小循环的最后一个对象的对比判断*/
                      json_tags_same[tnum]=json[j]; /*  为防止一个链接的多个标签被重复遍历,作if-else判断,从第2个开始判断 */
                      //console.log("1000000000000");
                      //console.log(json_tags_same.length);
                      tnum++;
                      //console.log('===='+ tnum);
                    } 
                 }
              }
          }
      }
      //console.log( json_tags_same );
      //console.log( typeof(json_tags) );
     var aside_list='';
     for(a=0;a<tnum;a++){ // 将 markdown类型显示在siderbar的副标签下。
        aside_list = aside_list + '<li data-priority=""><a href="' + json_tags_same[a].link_url + '"><div></div><div><strong>' + json_tags_same[a].link_title + '</strong></div></a></li>';
        //aside_list = aside_list + '<a href="' + json_tags[a].link_url + '">' + json_tags[a].link_title + '</a>';
     }
      //console.log( aside_list);
      var tags_aside = '<aside aria-label="更多结果">   <ol id="b_context">     <div class="b_ans" h="SERP,5442.1" data-bm="20">'   + 
                                  '<div class="b_rrsr dis_show" style="float:left;padding-top:1.5em;min-height:200px;">"' + 
                                  '         <span style="margin-left:0.2em;margin-right:0.2em;float:left;font-size:1em;font-weight:bold">相关链接</span>    ' +
                                           '<ul class="b_vList b_divsec"> 		 </ul>  ' + 
                                  ' </div> ' + 
                                  ' <div display="show" class="dis_show" style="float:right;writing-mode:tb-rl;font-size:1.4em;font-family:sans-serif;margin-top:2em;background-color:#fff0;margin-left:-1em;margin-right:-1em"><a href="javascript:void(0)" style="text-decoration: none;">&#x1F914;点击折叠侧边栏</a></div>' +
                                  ' <div id="dokuwiki__pagetools">     <h3 class="a11y">页面工具</h3>     <div class="tools">        ' + 
                                  '      <ul> <li class="top"><a href="#topdiv" title="回到顶部 [t]" rel="nofollow" accesskey="t"><span>回到顶部</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><polygon points="3.515 2 , 20.485 2, 20.485 0 ,3.515 0"/><polygon points="11 6.828 ,4.929 12.899 ,3.515 11.485 ,12 3 ,12.707 3.707 ,20.485 11.485 ,19.071 12.899 ,13 6.828 ,13 20 ,11 20 ,11 6.828"/></svg></a></li>        ' +
                                  '   <!--       <li class="revs"><a href="/kb/docs/git/git?do=revisions" title="修订记录 [o]" rel="nofollow" accesskey="o"><span>修订记录</span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M11 7v5.11l4.71 2.79.79-1.28-4-2.37V7m0-5C8.97 2 5.91 3.92 4.27 6.77L2 4.5V11h6.5L5.75 8.25C6.96 5.73 9.5 4 12.5 4a7.5 7.5 0 0 1 7.5 7.5 7.5 7.5 0 0 1-7.5 7.5c-3.27 0-6.03-2.09-7.06-5h-2.1c1.1 4.03 4.77 7 9.16 7 5.24 0 9.5-4.25 9.5-9.5A9.5 9.5 0 0 0 12.5 2z"></path></svg></a></li>  ' + 
                                  '          <li class="backlink"><a href="/kb/docs/git/git?do=backlink" title="反向链接" rel="nofollow"><span>反向链接</span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M10.59 13.41c.41.39.41 1.03 0 1.42-.39.39-1.03.39-1.42 0a5.003 5.003 0 0 1 0-7.07l3.54-3.54a5.003 5.003 0 0 1 7.07 0 5.003 5.003 0 0 1 0 7.07l-1.49 1.49c.01-.82-.12-1.64-.4-2.42l.47-.48a2.982 2.982 0 0 0 0-4.24 2.982 2.982 0 0 0-4.24 0l-3.53 3.53a2.982 2.982 0 0 0 0 4.24m2.82-4.24c.39-.39 1.03-.39 1.42 0a5.003 5.003 0 0 1 0 7.07l-3.54 3.54a5.003 5.003 0 0 1-7.07 0 5.003 5.003 0 0 1 0-7.07l1.49-1.49c-.01.82.12 1.64.4 2.43l-.47.47a2.982 2.982 0 0 0 0 4.24 2.982 2.982 0 0 0 4.24 0l3.53-3.53a2.982 2.982 0 0 0 0-4.24.973.973 0 0 1 0-1.42z"></path></svg></a></li>  ' +
                                  '     -->     <li class="top"><a href="/" title="跳转首页 [o]" rel="nofollow" target="_blank" accesskey="o"><span>跳转首页</span><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M8 20H3V10H0L10 0l10 10h-3v10h-5v-6H8v6z"/></svg></a></li> ' +
                                  '    <!-- 此事件首次无效，特添加另一个事件实现一次触发2次  -->   <li id="copyurl" onmousedown="copyurl()" onclick="copyurl()"  class="top"><a href="javascript:void(0)" title="复制页面url-基于clipboard.js [o]" rel="nofollow" accesskey="o"><span>复制网址</span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M6 6V2c0-1.1.9-2 2-2h10a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-4v4a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V8c0-1.1.9-2 2-2h4zm2 0h4a2 2 0 0 1 2 2v4h4V2H8v4zM2 8v10h10V8H2z"   transform="translate(2,2)"/></svg></a></li> ' +
                                  '      <li  class="top"><a href="/navigation/" title="导航页面 [o]" rel="nofollow" target="_blank" accesskey="o"><span>导航页面</span><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"  viewBox="0 0 24 24"><path d="M0 0h9v9H0V0zm2 2v5h5V2H2zm-2 9h9v9H0v-9zm2 2v5h5v-5H2zm9-13h9v9h-9V0zm2 2v5h5V2h-5zm-2 9h9v9h-9v-9zm2 2v5h5v-5h-5z"    transform="translate(2,2)"/></svg></a></li> ' +
                                  '          <li class="top"><a href="#js_bt_footer" title="直达页尾 [b]" rel="nofollow" accesskey="t"><span>直达页尾</span><svg xmlns="http://www.w3.org/2000/svg"   width="24" height="24" viewBox="0 0 24 24"><polygon points="11 14.172 , 4.929 8.101 ,3.515 9.515, 12 18 ,12.707 17.293 ,20.485 9.515 ,19.071 8.101 ,13 14.172 ,13 0 ,11 0"/><polygon points="3.515 21.5 , 20.485 21.5, 20.485 19.5 ,3.515 19.5"/></a></li>     </ul> ' +
                                  '</div> </div>  ' +
                        '</div>   </ol> </aside>';
      var tags_aside_div = document.createElement('div');
      tags_aside_div.innerHTML=tags_aside;
      if(document.querySelector('#write')){
         document.querySelector("#write").appendChild(tags_aside_div);
      }
     // document.body.appendChild(tags_aside_div);
      document.querySelector("aside>ol>div>div>ul").innerHTML=aside_list;
     var a_list = "";
     for(a=0;a<mk;a++){ // 将 markdown类型显示在siderbar的副标签下。
        a_list = a_list + '<a href="' + json_mk[a].link_url + '">' + json_mk[a].link_title + '</a>';
     }
     if(document.querySelector('#js_bt')){
         var typora_image ="<div id='js_bt_footer'><span><a title='转到首页' href='https://haudi.top'>Sow nothing, reap nothing.</a></span></div>";
         var typora_btdiv = document.createElement("div");
         typora_btdiv.innerHTML=typora_image;
         document.getElementById('write').appendChild(typora_btdiv);

         document.body.appendChild(typora_btdiv);
      }
      /*如果没有加入json清单，则不显示右侧悬浮列表*/
      if(list_latest_publish_j_b){document.getElementById("js_bt").innerHTML=list_latest_publish_j_a + list_latest_publish_j_b;}
      if(!list_latest_publish_j_b){document.getElementById("js_bt").style.cssText = 'display:none';}
      if(list_latest_publish_j_b){document.querySelector("div.b_rrsr>span").innerHTML='当前页面标签:<button>'+ arr_tags+ "</button>，以下为相关";}
      if(!list_latest_publish_j_b){document.querySelector("aside>ol#b_context").style.cssText = 'display:none';}
      if(list_latest_publish_j_b){document.getElementById('tab-summary1').getElementsByTagName('h2')[0].parentNode.innerHTML=a_list;}
      //console.log(tags);
      //document.getElementById("latest_publish").innerHTML=list_latest_publish;

  });
});
window.onload=function(){
var aside = document.querySelector("div[display='show']");
/*当页面为移动端或过窄时显首次打开默认隐藏链接*/
if(document.body.clientWidth < 768){
   document.querySelector('div.b_rrsr').className='b_rrsr dis_none';
   document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">&#x1F634;点击展开侧边栏</a>';

}
/*点击触发显隐*/
aside.onclick = function(){
  if(document.querySelector('div.b_rrsr').className == 'b_rrsr dis_show'){
      document.querySelector('div.b_rrsr').className='b_rrsr dis_none';
      document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">&#x1F634;点击展开侧边栏</a>';
  }else{
      document.querySelector('div.b_rrsr').className='b_rrsr dis_show';
      document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">&#x1F920;点击折叠侧边栏</a>';
  }
} 
}
/*复制当前页面的地址栏url*/
function sleep (time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}
  //document.querySelector('li#copyurl').onclick = function(){
function copyurl(){
      var btn = document.querySelector('#copyurl');

      btn.addEventListener('click', () => {
        const textCopied = ClipboardJS.copy(window.location.href);
        //控制台输出console.log('copied!', textCopied);
        document.querySelector('li#copyurl span').innerHTML='<fonts style="color:#0ABF5B;font-weight:900;">Copyed!</fonts>';
        (async function() {
            //console.log('Do some thing, ' + new Date());
            await sleep(1300);
            document.querySelector('li#copyurl span').innerHTML='<fonts>复制地址</fonts>';
            //console.log('Do other things, ' + new Date());
        })();
      this.click(); //解决clipboard二次点击生效问题
      clipboard.destroy();
      })
 }


function tags_list(){
        // 用于检测滚动条到底时显示相关链接的标签
        //变量scrollTop是滚动条滚动时，距离顶部的距离
        var scrollTop = document.documentElement.scrollTop||document.body.scrollTop;
        //变量windowHeight是可视区的高度
        var windowHeight = document.documentElement.clientHeight || document.body.clientHeight;
        //变量scrollHeight是滚动条的总高度
        var scrollHeight = document.documentElement.scrollHeight||document.body.scrollHeight;
               //滚动条到底部的条件
            if(scrollTop+windowHeight==scrollHeight){
                document.querySelector('div.b_rrsr').className='b_rrsr dis_show';
                document.querySelector("div[display='show']").innerHTML='<a href="javascript:void(0)" style="text-decoration: none;">↹点击折叠侧边栏</a>';
                console.log("宽度"+document.body.clientWidth+"距顶部"+scrollTop+"可视区高度"+windowHeight+"滚动条总高度"+scrollHeight);
             }   
}
window.onscroll = function() {
/* 保证左侧目录占用整个高度,在滚动时 */
   if(document.querySelector('#write')){
     md_toc_view();tags_list();
   }
};
/*  数字转换*/

     /**
     * 数字转成汉字
     * @params num === 要转换的数字
     * @return 汉字
     * */
     /**
     * 数字转成汉字
     * @params num === 要转换的数字
     * @return 汉字
     * */
    function toChinesNum(num) {
      let changeNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
      let unit = ['', '十', '百', '千', '万']
      num = parseInt(num)
      let getWan = (temp) => {
        let strArr = temp.toString().split('').reverse()
        let newNum = ''
        let newArr = []
        strArr.forEach((item, index) => {
          newArr.unshift(item === '0' ? changeNum[item] : changeNum[item] + unit[index])
        })
        let numArr = []
        newArr.forEach((m, n) => {
          if (m !== '零') numArr.push(n)
        })
        if (newArr.length > 1) {
          newArr.forEach((m, n) => {
            if (newArr[newArr.length - 1] === '零') {
              if (n <= numArr[numArr.length - 1]) {
                newNum += m
              }
            } else {
              newNum += m
            }
          })
        } else {
          newNum = newArr[0]
        }
 
        return newNum
      }
      let overWan = Math.floor(num / 10000)
      let noWan = num % 10000
      if (noWan.toString().length < 4) {
        noWan = '0' + noWan
      }
      return overWan ? getWan(overWan) + '万' + getWan(noWan) : getWan(num)
    }

/*展示当前页面url的二维码*/

/* console.log ad only */
console.log('本站实为个人小站，技术调试与在线文档使用，请勿压测本站.');
console.log("%c假如你打开并看到了这里，请加个友链推广一下本站吧","background: rgba(252,234,187,1);background: -moz-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%,rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -webkit-gradient(left top, right top, color-stop(0%, rgba(252,234,187,1)), color-stop(12%, rgba(175,250,77,1)), color-stop(28%, rgba(0,247,49,1)), color-stop(39%, rgba(0,210,247,1)), color-stop(51%, rgba(0,189,247,1)), color-stop(64%, rgba(133,108,217,1)), color-stop(78%, rgba(177,0,247,1)), color-stop(87%, rgba(247,0,189,1)), color-stop(100%, rgba(245,22,52,1)));background: -webkit-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -o-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: -ms-linear-gradient(left, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);background: linear-gradient(to right, rgba(252,234,187,1) 0%, rgba(175,250,77,1) 12%, rgba(0,247,49,1) 28%, rgba(0,210,247,1) 39%, rgba(0,189,247,1) 51%, rgba(133,108,217,1) 64%, rgba(177,0,247,1) 78%, rgba(247,0,189,1) 87%, rgba(245,22,52,1) 100%);filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#f51634', GradientType=1 );font-size:2em;font-family:sans-serif;padding:0.2em;border-left:2px solid green;border-right:3px solid gray;border-radius: 2em;");

console.log("%c This's my personal website for blogs,use only for technology and online doc,please don't ddos or benchmark test.","font-size:1.5em;font-family:sans-serif;padding:0.2em;border-left:2px solid green;border-right:3px solid gray;border-radius: 1.2em;");

console.log("%c 技术源自实践,实践检验真知。"," text-shadow: 0 1px 0 #ccc,0 2px 0 #c9c9c9,0 3px 0 #bbb,0 4px 0 #b9b9b9,0 5px 0 #aaa,0 6px 1px rgba(0,0,0,.1),0 0 5px rgba(0,0,0,.1),0 1px 3px rgba(0,0,0,.3),0 3px 5px rgba(0,0,0,.2),0 5px 10px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.2),0 20px 20px rgba(0,0,0,.15);font-size:5em;font-family:sans-serif;")


window.console && window.console.log && console.log("%c CV : https://haudi.top/main/pod/pages/essay", "color:red;font-size:2em;");

window.console && window.console.log && (console.log("%c本站首页:","font-size:2em;color:blue;font-family:sans-serif;"),
    console.log("%c https://haudi.top","color:red;font-size:2em;"));
