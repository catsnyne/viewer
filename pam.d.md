

[TOC]



# 基本使用

pam_tally2模块用于某些数对系统进行失败的ssh登录尝试后锁定用户帐户。 此模块保留已尝试访问的计数和过多的失败尝试。

pam_tally2模块有两个部分，一个是pam_tally2.so，另一个是pam_tally2。 它是基于PAM模块上，并且可以被用于检查和调节计数器文件。 它可以显示用户登录尝试次数，单独设置计数，解锁所有用户计数。



1. pam_tally2与pam_tally模块的区别是前者增加了自动解锁时间的功能，后者没有。所以在老的发行版中，如果使用了pam_tally模块时，可以使用pam_tally 、faillog配合crontab 进行自动解锁。

2. 如果超过三次的话，用户不能登录并且此后登录用户错误登录次数还是会增加。

3. 在登录错误次数不满三次时，登录成功后，则这个用户登录错误值将清零，退出后重新telnet登录将采用新的计数。

```bash
[root@localhost ~]# pam_tally2 --help
--help: Unrecognised option --help
pam_tally2: [-f rooted-filename] [--file rooted-filename]
   [-u username] [--user username]
   [-r] [--reset[=n]] [--quiet]
[root@localhost ~]#

```



```bash
[root@localhost ~]# pam_tally2 --reset -u root
Login           Failures Latest failure     From
root              185    11/20/20 14:30:35  172.16.24.5
[root@localhost ~]# pam_tally2
Login           Failures Latest failure     From
ftp               167    11/15/20 22:50:55  10.6.91.231
eproot            404    11/20/20 14:10:27  172.16.24.5
[root@localhost ~]# pam_tally2 --user root
Login           Failures Latest failure     From
root                0
[root@localhost ~]#


```



- /etc/pam.d/login          # 终端登录修改该文件
- /etc/pam.d/kde           #**只在kde图形化登陆界面上做限制**
- /etc/pam.d/remote     # telnet 估计现在用的少了
- /etc/pam.d/sshd        # 通过ssh连接时做限制



-  `/etc/pam.d/system-auth` 想在所有登陆方式上，限制所有用户，添加如下


```
auth  required     pam_tally.so   onerr=fail  no_magic_root
account  required  pam_tally.so   deny=3  no_magic_root  even_deny_root_account  per_user  reset
```



## 问题处理

通过上面的pam_tally2查看是哪个ip导致服务器锁定，进行处理。



## 配置参数

#### **PAM配置方法**

所有的PAM配置方法都在man手册中有说明，比如要查找某个程序支持PAM模块的配置，可以使用man 加模块名(去掉.so)查找说明，如#`man pam_unix`。(模块名可以在目录`/lib/security/`或`/lib64/security/`中找到。)

### 参数详解

应用场景：设置Linux用户连续N次输入错误密码进行登陆时，自动锁定X分钟或永久锁定（这里的永久锁定指除非进行手工解锁，否则会一直锁定）。

配置格式：

```bash
pam_tally2.so [file=/path/to/counter] [onerr=[fail|succeed]] [magic_root] [even_deny_root] [deny=n] [lock_time=n] [unlock_time=n] [root_unlock_time=n] [serialize] [audit] [silent] [no_log_info]
```

#### 1、全局参数

```bash
file        # 用于指定统计次数存放的位置，默认保存在/var/log/tallylog文件中；
onerr       # 当意外发生时，返加PAM_SUCCESS或pam错误代码，一般该项不进行配置；
audit       # 如果登录的用户不存在，则将访问信息写入系统日志；
silent      # 静默模式，不输出任何日志信息；
no_log_info # 不打印日志信息通过syslog
```

上面的五项全局参数，一般在使用中都不需要单独配置。

#### 2、认证选项

```bash
deny             # 指定最大几次认证错误，如果超出此错误，将执行后面的策略。如锁定N秒，如果后面没有其他策略指定时，默认永远锁定，除非手动解锁。
lock_time         # 锁定多长时间，按秒为单位；
unlock_time       # 指定认证被锁后，多长时间自动解锁用户；
magic_root        # 如果用户uid＝0（即root账户或相当于root的帐户）在帐户认证时调用该模块发现失败时，不计入统计；
no_magic_root     # 连root用户也在限制范围，不给root特殊权限。
no_lock_time      # 不使用.fail_locktime项在/var/log/faillog 中记录用户 －－－按英文直译不太明白，个人理解即不进行用户锁定；
even_deny_root    # root用户在认证出错时，一样被锁定（该功能慎用，搞不好就要单用户时解锁了）
root_unlock_time  # root用户在失败时，锁定多长时间。该选项一般是配合even_deny_root 一起使用的。
```

### **PAM身份验证安全配置实例**

一、**强制使用强密码**（用户密码安全配置）

PAM配置文件：`/etc/pam.d/system-auth-ac`

模块名称：`pam_cracklib`(仅适用于password模块接口)

模块参数：

```bash
minlen=12           # 密码字符长度不少于12位(默认为9)
lcredit=-1          # 至少包含1个小写字母
ucredit=-1          # 至少包含1个大写字母
dcredit=-1          # 至少包含1个数字
ocredit=-1          # 至少包含1个特殊字符
retry=3             # 配置密码时，提示3次用户密码错误输入
difok=6             # 配置密码时，新密码中至少6个字符与旧密码不同(默认为5)
```

其他常用参数：

```bash
reject_username     # 新密码中不能包含与用户名称相同的字段
maxrepeat=N         # 拒绝包含超过N个连续字符的密码，默认值为0表示此检查已禁用
maxsequence=N       # 拒绝包含大于N的单调字符序列的密码，例如’1234’或’fedcb’，默认情况下即使没有这个参数配置，一般大多数这样的密码都不会通过，除非序列只是密码的一小部分
maxcla***epeat=N    # 拒绝包含相同类别的N个以上连续字符的密码。默认值为0表示此检查已禁用。
use_authtok         # 强制使用先前的密码，不提示用户输入新密码(不允许用户修改密码)
```

模块名称：pam_unix (适用于account，auth， password和session模块接口)

模块参数：

```bash
remember=N      # 保存每个用户使用过的N个密码，强制密码不能跟历史密码重复
```

其他常见参数：

```bash
sha512         #  当用户下一次更改密码时，使用SHA256算法进行加密
md5            #  当用户更改密码时，使用MD5算法对其进行加密。
try_first_pass #  在提示用户输入密码之前，模块首先尝试先前的密码，以测试是否满足该模块的需求。
use_first_pass #  该模块强制使用先前的密码(不允许用户修改密码)，如果密码为空或者密码不对，用户将被拒绝访问
shadow         #  用户保护密码
nullok         #  默认不允许空密码访问服务
use_authtok    #  强制使用先前的密码，不提示用户输入新密码(不允许用户修改密码)
```

**例** 修改配置`/etc/pam.d/system-auth-ac`文件，在password模块接口行修改或添加配置参数如下：

```bash
password requisite pam_cracklib.so try_first_pass retry=3 type= reject_username minlen=12 lcredit=-1 ucredit=-1 dcredit=-1 ocredit=-1 difok=6
```



需要注意的是，我在这里展示的是在`RHEL/CentOS`下的配置，`passwd`程序的PAM配置文件涉及主配置文件`/etc/pam.d/passwd`和`/etc/pam.d/system-auth-ac`（也可以是`/etc/pam.d/password-auth-ac`），其中`/etc/pam.d/passwd`配置文件默认只包含了`/etc/pam.d/system-auth-ac`配置文件，因此对于以上PAM身份验证密码模块配置，只修改/配置该文件即可。或者在Ubuntu中，配置文件包括：`/etc/pam.d/common-password、/etc/pam.d/common-account、/etc/pam.d/common-auth、/etc/pam.d/common-session`。

### 其他例子：

### Pam_tally2锁定SSH登录

默认情况下，pam_tally2模块已经安装在大多数Linux发行版，它是由PAM包本身的控制。 本文演示如何锁定和深远的登录尝试的失败一定次数后解锁SSH帐户。

#### 如何锁定和解锁用户帐户

使用`“/etc/pam.d/password-auth”`配置文件来配置的登录尝试的访问。 打开此文件并以下AUTH配置行举行的“ 身份验证 ”部分的开头添加到它。

```bash
auth required pam_tally2.so file=/var/log/tallylog deny=3  even_deny_root unlock_time=1200
```

接下来，添加以下行“ 账户 ”部分。

```bash
account required pam_tally2.so
参数 文件= /无功/日志/ tallylog             # 默认的日志文件是用来保持登录计数。 
deny = 3                                 # 拒绝后，3次尝试访问和锁定用户。
even_deny_root                            # 政策也适用于root用户。 
unlock_time = 1200                        # 帐户将被锁定，直到20分钟 （如果要永久锁定，直到手动解锁，请删除此参数） 一旦你使用上面的配置完成，现在尽量尝试使用任何“ 用户名 ”3失败的登录尝试到服务器。当你取得了超过3次，你会收到以下消息。 
[root@test01 ~]# ssh test01@172.16.25.126

test01@172.16.25.126's password:
Permission denied, please try again.
test01@172.16.25.126's password:
Permission denied, please try again.
test01@172.16.25.126's password:
Account locked due to 4 failed logins
Account locked due to 5 failed logins
Last login: Mon Apr 22 21:21:06 2017 from 172.16.16.52
```

现在，使用以下命令验证或检查用户尝试的计数器。

```bash
[root@test01 ~]# pam_tally2 --user test01

Login Failures Latest failure From
test01 15 04/22/17 21:22:37 172.16.16.52
```

如何重置或解锁用户帐户以再次启用访问。

```bash
[root@test01 pam.d]# pam_tally2 --user=test01 --reset

Login Failures Latest failure From
test01 15 04/22/13 17:10:42 172.16.16.52
```

验证登录尝试已重置或解锁

```bash
[root@test01 pam.d]# pam_tally2 --user=test01

Login Failures Latest failure From
test01 0
```

### 手动锁定

用户**禁止使用**

 可以用`usermod`命令来**锁定用户**密码，使密码无效，该**用户**名将不能使用。

如： `usermod -L 用户名`

解锁命令：`usermod -U 用户名`



## 自动注销

/etc/profile

source /etc/profile 立即生效

```
    TMOUT=300
    export TOMOUT
```



