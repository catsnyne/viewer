var cityList = [{ key: 'A', data: ['Angola-安哥拉-990244', 'Afghanistan-阿富汗-990093', 'Albania-阿尔巴尼亚-990335', 'Algeria-阿尔及利亚-990213', 'Andorra-安道尔共和国-990376', 'Anguilla-安圭拉岛-991254', 'Antigua and Barbuda-安提瓜和巴布达-991268', 'Argentina-阿根廷-990054', 'Armenia-亚美尼亚-990374', 'Ascension-阿森松-990247', 'Australia-澳大利亚-990061', 'Austria-奥地利-990043', 'Azerbaijan-阿塞拜疆-990994'] },
{ key: 'B', data: ['Bahamas-巴哈马-991242', 'Bahrain-巴林-990973', 'Bangladesh-孟加拉国-990880', 'Barbados-巴巴多斯-991246', 'Belarus-白俄罗斯-990375', 'Belgium-比利时-990032', 'Belize-伯利兹-990501', 'Benin-贝宁-990229', 'Bermuda Is-百慕大群岛-991441', 'Bolivia-玻利维亚-990591', 'Botswana-博茨瓦纳-990267', 'Brazil-巴西-990055', 'Brunei-文莱-990673', 'Bulgaria-保加利亚-990359', 'Burkina Faso-布基纳法索-990226', 'Burundi-布隆迪-990257'] },
{ key: 'C', data: ['Cameroon-喀麦隆-990237', 'Canada-加拿大-990002', 'Cayman Is-开曼群岛-991345', 'Central African Republic-中非共和国-990236', 'Chad-乍得-990235', 'Chile-智利-990056', 'Colombia-哥伦比亚-990057', 'Congo-刚果-990242', 'Cook Is-库克群岛-990682', 'Costa Rica-哥斯达黎加-990506', 'Cuba-古巴-990053', 'Cyprus-塞浦路斯-990357', 'Czech Republic-捷克-990420'] },
{ key: 'D', data: ['Denmark-丹麦-990045', 'Djibouti-吉布提-990253', 'Dominica Rep-多米尼加共和国-991890'] },
{ key: 'E', data: ['Ecuador-厄瓜多尔-990593', 'Egypt-埃及-990020', 'EI Salvador-萨尔瓦多-990503', 'Estonia-爱沙尼亚-990372', 'Ethiopia-埃塞俄比亚-990251'] },
{ key: 'F', data: ['Fiji-斐济-990679', 'Finland-芬兰-990358', 'France-法国-990033', 'French Guiana-法属圭亚那-990594', 'French Polynesia-法属玻利尼西亚-990689'] },
{ key: 'G', data: ['Gabon-加蓬-990241', 'Gambia-冈比亚-990220', 'Georgia-格鲁吉亚-990995', 'Germany-德国-990049', 'Ghana-加纳-990277', 'Gibraltar-直布罗陀-990350', 'Greece-希腊-990030', 'Grenada-格林纳达-991810', 'Guam-关岛-991671', 'Guatemala-危地马拉-990502', 'Guinea-几内亚-990224', 'Guyana-圭亚那-990592'] },
{ key: 'H', data: ['Haiti-海地-990509', 'Honduras-洪都拉斯-990504', 'Hungary-匈牙利-990036'] },
{ key: 'I', data: ['Iceland-冰岛-990354', 'India-印度-990091', 'Indonesia-印度尼西亚-990062', 'Iran-伊朗-990098', 'Iraq-伊拉克-990964', 'Ireland-爱尔兰-990353', 'Israel-以色列-990972', 'Italy-意大利-990039', 'Ivory Coast-科特迪瓦-990225'] },
{ key: 'J', data: ['Jordan-约旦-990962', 'Japan-日本-990081', 'Jamaica-牙买加-991876'] },
{ key: 'K', data: ['Kampuchea (Cambodia )-柬埔寨-990855', 'Kazakstan-哈萨克斯坦-990327', 'Kenya-肯尼亚-990254', 'Korea-韩国-990082', 'Kuwait-科威特-990965', 'Kyrgyzstan-吉尔吉斯坦-990331'] },
{ key: 'L', data: ['Laos-老挝-990856', 'Latvia-拉脱维亚-990371', 'Lebanon-黎巴嫩-990961', 'Lesotho-莱索托-990266', 'Liberia-利比里亚-990231', 'Libya-利比亚-990218', 'Liechtenstein-列支敦士登-990423', 'Lithuania-立陶宛-990370', 'Luxembourg-卢森堡-990352'] },
{ key: 'M', data: ['Madagascar-马达加斯加-990261', 'Malawi-马拉维-990265', 'Malaysia-马来西亚-990060', 'Maldives-马尔代夫-990960', 'Mali-马里-990223', 'Malta-马耳他-990356', 'Mariana Is-马里亚那群岛-991670', 'Martinique-马提尼克-990596', 'Mauritius-毛里求斯-990230', 'Mexico-墨西哥-990052', 'Moldova-摩尔多瓦-990373', 'Monaco-摩纳哥-990377', 'Mongolia-蒙古-990976', 'Montserrat Is-蒙特塞拉特岛-991664', 'Morocco-摩洛哥-990212', 'Mozambique-莫桑比克-990258', 'Myanmar-缅甸-990095',] },
{ key: 'N', data: ['Namibia-纳米比亚-990264', 'Nauru-瑙鲁-990674', 'Nepal-尼泊尔-990977', 'Netheriands Antilles-荷属安的列斯-990599', 'Netherlands-荷兰-990031', 'New Zealand-新西兰-990064', 'Nicaragua-尼加拉瓜-990505', 'Niger-尼日尔-990227', 'Nigeria-尼日利亚-990234', 'North Korea-朝鲜-990850', 'Norway-挪威-990047'] },
{ key: 'O', data: ['Oman-阿曼-990968'] },
{ key: 'P', data: ['Pakistan-巴基斯坦-990092', 'Panama-巴拿马-990507', 'Papua New Cuinea-巴布亚新几内亚-990675', 'Paraguay-巴拉圭-990595', 'Peru-秘鲁-990051', 'Philippines-菲律宾-990063', 'Poland-波兰-990048', 'Portugal-葡萄牙-990351', 'Puerto Rico-波多黎各-991787'] },
{ key: 'Q', data: ['Qatar-卡塔尔-990974'] },
{ key: 'R', data: ['Reunion-留尼旺-990262', 'Romania-罗马尼亚-990040', 'Russia-俄罗斯-990007'] },
{ key: 'S', data: ['Saint Lueia-圣卢西亚-991758', 'Saint Vincent-圣文森特岛-991784', 'Samoa Eastern-东萨摩亚(美)-990684', 'Samoa Western-西萨摩亚-990685', 'San Marino-圣马力诺-990378', 'Sao Tome and Principe-圣多美和普林西比-990239', 'Saudi Arabia-沙特阿拉伯-990966', 'Senegal-塞内加尔-990221', 'Seychelles-塞舌尔-990248', 'Sierra Leone-塞拉利昂-990232', 'Singapore-新加坡-990065', 'Slovakia-斯洛伐克-990421', 'Slovenia-斯洛文尼亚-990386', 'Solomon Is-所罗门群岛-990677', 'Somali-索马里-990252', 'South Africa-南非-990027', 'Spain-西班牙-990034', 'SriLanka-斯里兰卡-990094', 'Sudan-苏丹-990249', 'Suriname-苏里南-990597', 'Swaziland-斯威士兰-990268', 'Sweden-瑞典-990046', 'Switzerland-瑞士-990041', 'Syria-叙利亚-990963'] },
{ key: 'T', data: ['Tajikstan-塔吉克斯坦-990992', 'Tanzania-坦桑尼亚-990255', 'Thailand-泰国-990066', 'Togo-多哥-990228', 'Tonga-汤加-990676', 'Trinidad and Tobago-特立尼达和多巴哥-991809', 'Tunisia-突尼斯-990216', 'Turkey-土耳其-990090', 'Turkmenistan-土库曼斯坦-990993'] },
{ key: 'U', data: ['Uganda-乌干达-990256', 'Ukraine-乌克兰-990380', 'United Arab Emirates-阿拉伯联合酋长国-990971', 'United Kiongdom-英国-990044', 'United States of America-美国-990001', 'Uruguay-乌拉圭-990598', 'Uzbekistan-乌兹别克斯坦-990233'] },
{ key: 'V', data: ['Venezuela-委内瑞拉-990058', 'Vietnam-越南-990084'] },
{ key: 'Y', data: ['Yemen-也门-990967', 'Yugoslavia-南斯拉夫-990381'] },
{ key: 'Z', data: ['Zimbabwe-津巴布韦-990263', 'Zaire-扎伊尔-990243', 'Zambia-赞比亚-990260'] }
]
var hotCity = ['Thailand-泰国-990066', 'Singapore-新加坡-990065', 'Indonesia-印度尼西亚-990062', 'Malaysia-马来西亚-990060', 'Korea-韩国-990082', 'Japan-日本-990081', 'Vietnam-越南-990084', 'United States of America-美国-990001', 'United Kiongdom-英国-990044', 'Germany-德国-990049', 'France-法国-990033', 'Philippines-菲律宾-990063', 'United Arab Emirates-阿拉伯联合酋长国-990971', 'Australia-澳大利亚-990061', 'Italy-意大利-990039'];

$(function () {
  init();
  // 选择城市
  $('body').on('click', '.city-list p', function () {
    var data = $(this).text();
    var code = $(this).attr('code');
    // saveHistory(data);
    $("#countrySelectInput").val(data);
    $('input[name=gjbm]').val(code)
    $.closeModal();
  });

  $('.hot.hotCity').on('click', 'div', function () {
    var data = $(this).text();
    var code = $(this).attr('code');
    // saveHistory(data);
    $("#countrySelectInput").val(data);
    $('input[name=gjbm]').val(code)
    $.closeModal();
  });
})

function init () {
  $('.city').html('');
  var hotHtml = '';
  hotHtml += '<div class="tips" id="热门1">热门城市</div>';
  hotHtml += '<div class="hot hotCity">';
  $.each(hotCity, function (i, item) {
    var itemSplitList = item.split('-');
    var areaCode = '99-9900-' + itemSplitList[2]
    hotHtml += '<div code="' + areaCode + '">' + itemSplitList[1] + '</div>'
  })
  hotHtml += '</div>';
  hotHtml += '<div class="history"></div>';
  $('.city').append(hotHtml);

  var html = '';
  $.each(cityList, function (i, item) {
    html += '<div class="city-list"><span class="city-letter" id="' + item.key + '1">' + item.key + '</span>';
    $.each(item.data, function (j, data) {
      var dataList = data.split('-');
      var areaCode = '99-9900-' + dataList[2]
      html += '<p code="' + areaCode + '">' + dataList[0] + dataList[1] + '</p>';
    })
    html += '</div>';
  })
  $('.city').append(html);
}

; (function ($) {

  //   $('.letter').bind("touchstart touchmove", function (e) {
  //       var top = $(window).scrollTop();
  //       e.preventDefault();//阻止默认滚动
  //       var touch = e.touches[0];
  //       var ele = document.elementFromPoint(touch.pageX, touch.pageY - top);

  //       if (ele.tagName === 'A') {
  //           var s = $(ele).text();
  //           $(window).scrollTop($('#' + s + '1')[0].offsetTop)
  //           $("#showLetter span").html(s.substring(0, 1));
  //           $("#showLetter").show();
  //       }
  //   });

  //   $('.letter').bind("touchend", function (e) {
  //       $("#showLetter").hide(0);
  //   });

  $('.letter a').each(function (index, item) {
    $(this).click(function () {
      var att = $(this).text();
      if ($("#" + att + "1").length > 0) {
        var curOffsetTop = $("#" + att + "1")[0].offsetTop;
        $(".container").scrollTo({
          toT: curOffsetTop,
          durTime: 0
        });
      }
    });
  });
})(Zepto);
