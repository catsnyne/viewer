dayjs().format();
/* 生成页面顶部的随机与最近文章列表 */
fetch('/main/pod/imy/data/json_api_links_all.json').then(function(response) {
  return response.json().then(function(json) {
      var list_latest_publish = '';
      for(j=0;j<8;j++){
         var date = new Date(json[j].link_time);
         list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
         //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + (date.getMonth()+1) + '/' +date.getDate()  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
      }
      document.getElementById("latest_publish").innerHTML=list_latest_publish;

      var list_random = '';
      json.sort(function () { return 0.5 - Math.random() ;});
      for(j=0;j<8;j++){
         list_random = list_random + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random").innerHTML=list_random;

      var list_random2 = '';
      for(j=8;j<16;j++){
         list_random2 = list_random2 + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random2").innerHTML=list_random2;

  });
});

/* ip2region 显示客户端ip归属地  */
var realip = '';
fetch('/geoip2').then(function(response) {
  return response.json().then(function(json) {
      var realip = json.realip;
      document.getElementById("dropdown-menu-6").innerText=realip;

  }).then(function(){
  var realip = document.getElementById("dropdown-menu-6").innerText;
  fetch('/main/pod/pages/ip2region/ip2region?ip='+ realip ).then(function(response){
      return response.json().then(function(json){
         document.getElementById("dropdown-menu-6").innerText=json.ip + json.region;
      });
  });
 });
});
