document.title = '新点用户社区';
if(window.location.href.search('EpointCommunity') != -1  ){
    function queryURLParams(URL) {
       let url = URL.split("?")[1];
       const urlSearchParams = new URLSearchParams(url);
       const params = Object.fromEntries(urlSearchParams.entries());
       return params

    }

    console.log(queryURLParams(window.location.href));
    var url='https://oa.epoint.com.cn/topic/frame/pages/communitynew/index#/detail?articleId=';
    var local_url=window.location.href;
    var searchParams = new URLSearchParams(queryURLParams(window.location.href));
    console.log(searchParams.get('TopicID'));
    location.assign( url +  searchParams.get('TopicID'));
}

var oMeta = document.createElement('meta');
oMeta.content='catsnyne_gmail_com,amos';
oMeta.name='author';
var pMeta = document.createElement('meta');
pMeta.content='IE=edge;text/html; charset=UTF-8';
pMeta.httpEquiv='X-UA-Compatible';
var qMeta = document.createElement('meta');
qMeta.charset='utf-8';
document.getElementsByTagName('head')[0].appendChild(oMeta);
document.getElementsByTagName('head')[0].appendChild(pMeta);
document.getElementsByTagName('head')[0].appendChild(qMeta);

content_div = document.getElementById('ctl00_cphContent_UpdatePanel1');
document.body.appendChild(content_div);

document.getElementsByTagName("form")[0].remove();
brday = document.getElementById('ctl00_cphContent_uiHoster_lblBirthday');
brday.innerText=' NULL';
content_write = document.getElementById('write');
document.body.appendChild(content_write);
content_div.remove();
document.getElementsByTagName("link")[0].remove();

// 顶部div
var typora_image ="<link type='text/css' rel='stylesheet' href='https://xinzhiba.top/cdn_static/markdown_tree.css'><div><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Home/Home.aspx'><img width='890'  src='https://xinzhiba.top/cdn_static/typora_topbg.jpg'/></a></div>";
var typora_topdiv = document.createElement("div");
typora_topdiv.setAttribute('id','topdiv')
typora_topdiv.innerHTML=typora_image;
//document.getElementById('write').appendChild(typora_topdiv);

// document.getElementById('write').insertBefore(typora_topdiv,document.getElementById('write').getElementsByTagName("p")[0]);
document.body.insertBefore(typora_topdiv,document.getElementById('write'));


var typora_image ="<link type='text/css' rel='stylesheet' href='https://xinzhiba.top/cdn_static/markdown_tree.css'><hr><div id='js_bt'><span>国泰新点软件股份有限公司 电话：0512-58173200 备案号：苏ICP备10206980号-1</span></div>";
var typora_btdiv = document.createElement("div");
typora_btdiv.innerHTML=typora_image;
document.getElementById('write').appendChild(typora_btdiv);
//document.body.appendChild(typora_btdiv);




//var toc ="<div id='list_1'><div><ul><li class='list_1_a_active'><a >📜Readme</a></li><li class='list_1_a_gray'><a>🔗Star</a></li></ul></div></div><div id='list_2' class='select_list'><div id='tab-summary' class='topic_list'> <div><h2 style='color:black;text-align:center;'>Table of Contents</h2></div><a href='#'></a></div></div><div id='list_2_1' class='select_list tab_hidden'><div id='tab-summary1' class='topic_list'> <div><h2 style='color:black;text-align:center;'>往期清单</h2></div><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=29328'>Tomcat服务的封装过程</a><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=31788'>新版ecloud3.0手动上传算码操作演示（备用）</a><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=31611'>传统技能之端口检测相关的基础知识（终篇）</a><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=29894'>浏览器开发者调试工具快速一览</a><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=29579'>内存信息查看</a><a href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=29924' target='_blank'>挂载检测与记一次容器主机下线的排查分析过程</a><a target='_blank' href='https://oa.epoint.com.cn/EpointCommunity/EpointCommunity/Dis/ShowTopic.aspx?TopicID=22332'>DevOps之docker在应用部署中的非常规调试命令操作</a></div></div><div id='list_3'><div style='text-align:center;'> 国泰新点软件股份有限公司 </div> <div style='text-align:center;'> 电话：0512-58173200 </div>	  <div style='text-align:center;'> 备案号：苏ICP备10206980号-1</div></div>";

var toc ="<div id='list_1'><div><ul><li class='list_1_a_active'><a >📜Readme</a></li><li class='list_1_a_gray'><a>🔗Star</a></li></ul></div></div><div id='list_2' class='select_list'><div id='tab-summary' class='topic_list'> <div><h2 style='color:black;text-align:center;'>Table of Contents</h2></div><a href='#'></a></div></div><div id='list_2_1' class='select_list tab_hidden'><div id='tab-summary1' class='topic_list'> <div><h2 style='color:black;text-align:center;'>往期清单</h2></div></div></div><div id='list_3'><div style='text-align:center;'> 国泰新点软件股份有限公司 </div> <div style='text-align:center;'> 电话：0512-58173200 </div>	  <div style='text-align:center;'> 备案号：苏ICP备10206980号-1</div></div>";
var md_toc_div = document.createElement("div");
md_toc_div.setAttribute('id','md_toc')
md_toc_div.innerHTML=toc;
document.body.appendChild(md_toc_div);
//document.getElementById('write').appendChild(typora_topdiv);

document.body.insertBefore(md_toc_div,document.getElementById('write'));





// ############################################################

var md_toc = document.getElementById("write")


var dlist = document.getElementById("md_toc");
var dlist_left = document.getElementById("tab-summary");



var num = 0;//统计找到的mt和st

//遍历所有元素结点
		var hh11=0;/* 一个是中间变量一个是递增变量*/
		var hh22=0;
		var hh33=0;
		var hh44=0;

for(var i=0; i<md_toc.childElementCount; i++)
{ 
	//document.getElementsByClassName("md-toc")[0].children[0].children.length

	//console.log(md_toc[i].className);
	//console.log(md_toc[i].children[0].hash);


var t_name = md_toc.children[i].tagName;
// console.log(t_name);
// var str = md_toc[i].className;
var reg = RegExp(/md-toc-h/);
//console.log(reg.test(str)); // true
  if(RegExp(/H1/).test(t_name)){
	//console.log(t_name,RegExp(/H1/).test(t_name));
	var md_toc_hx = "H1";
  }else if(RegExp(/LINK/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "LINK";
  }else if(RegExp(/H2/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H2";
  }else if(RegExp(/H3/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H3";
  }else if(RegExp(/H4/).test(t_name)){
    //console.log(md_toc[i].children[0].innerText);
	var md_toc_hx = "H4";
  }else{
    //console.log("else off");
	var md_toc_hx = "false";
  }

 var listnodetext = md_toc.children[i].getAttribute('id') ;
// console.log(listnodetext);
 //var listnodetext = md_toc.children[i].innerText ;
 //var listnodetext = md_toc[i].innerText ;

// var str="jfkldsjalk,.23@#!$$k~!  @#$%^&*()(_+-=|\{}[]';:,./<>??gg  g~```gf"; 
// listnodetext=listnodetext.replace("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]",""); 

var reg = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]","g");
// 将以上匹配的字符全部清理掉。
//// listnodetext=listnodetext.replace(reg,"").toLowerCase();
// g
// listnodetext=listnodetext.replace(/[\ |\~|\`|\!|\@|\#|\$|\%|\^|\&|\*|\(|\)|\-|\_|\+|\=|\||\\|\[|\]|\{|\}|\;|\:|\"|\'|\,|\<|\.|\>|\/|\、|\（|\、|\）|\?]/g,""); 
//// console.log(listnodetext);


//var nodeIdGet = listnodetext; 
var md_toc_side = "";
var default_none = 0;
switch(md_toc_hx)
{
	case "H1":    //若为主标题 
		item = document.createElement("dt");
								hh11 = hh11 +1 ;
								////md_toc[i].innerHTML = "§ " + hh11 + ". " + listnodetext;
                                md_toc_side="§ " + hh11 ;
								hh22 = 0 ; 
		break;
	case "H2":    //若为子标题
		item = document.createElement("dd");
								hh22 = hh22 +1 ;
								////md_toc[i].innerHTML = hh11 + "." + hh22 + ". " + listnodetext;
								md_toc_side= hh11 + "." + hh22;
								hh33 = 0; 
		break;
	case "H3":    //若为子标题
		item = document.createElement("de");
								hh33 = hh33 + 1 ;
								if (hh33 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33;
								}
								//hh33 = hh33 + 1 ;
		break;
	
	case "H4":    //若为子标题
		item = document.createElement("df");
								hh44 = hh44 + 1 ;
								if (hh44 >= 1 ) {
								   if ( hh11 == 0 ){
										   hh11 = hh11 + 1;
								   }
								   if ( hh22 == 0 ){
										   hh22 = hh22 + 1;
								   }
								   if ( hh33 == 0 ){
										   hh33 = hh33 + 1;
								   }
								////	md_toc[i].innerHTML = hh11 + "." + hh22 + "." + hh33 + ". "+ listnodetext;
									md_toc_side= hh11 + "." + hh22 + "." + hh33 + "." + hh44;
								}
								//hh33 = hh33 + 1 ;
		break;
	default:    //若为子标题
                 default_none = 1;
                 hh11 = hh11;
                 hh22 = hh22;
                 hh33 = hh33; 
                 hh44 = hh44;
                // console.log("switch_default",default_none,"hh11:",hh11,"hh22:",hh22,"hh33:",hh33,"hh44:",hh44);
}


if (default_none == 0 ) {
// console.log("if_default_none:",default_none);
var ahreftextnode = document.createElement('a');
//var ahreftextnode= item.appendChild(a)
// ahreftextnode.setAttribute('href', "#" + nodeIdGet);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].hash);
ahreftextnode.setAttribute('href', '#' + listnodetext);
// ahreftextnode.setAttribute('href', md_toc[i].children[0].getAttribute('href'));
//// ahreftextnode.classList.add("list-group-item");

ahreftextnode.innerHTML= "<em>" + md_toc_side + ". </em>" + listnodetext;
//ahreftextnode.innerHTML=nodetext;
// console.log(item);
item.appendChild(ahreftextnode);

//	};
//将自定义表项加入自定义列表中
//dlist.appendChild(item);
//console.log(dlist_left)
dlist_left.appendChild(item);
}
num++;





// console.timeEnd("开始时间点");
//添加目录列表结束标记而矣

}

toppp = document.createElement("toppp");
enddd = document.createElement("enddd");

top_a_contents = document.createElement('a');
top_a_contents.setAttribute('href','#top');
end_a_contents = document.createElement('a');
end_a_contents.setAttribute('href','#bottom');


top_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='height:2em;background-color:#4b4033;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 0 0 4px 4px;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

end_a_contents.innerHTML="<div class='' style='border: 0px solid #dbdbdd;color: white;margin-top: 1px;margin-bottom: 1px;display: flex;justify-content: center;align-items: center;'><div style='margin-top:1em;padding:0.3em 0;background-color:#4baa33;background-repeat: no-repeat;background-position: 0px -200px;background-size: 1000px 1000px;width:100%;font-size: 1em;text-align:center;border-radius:5px;'><a style='display: inline-block; border-width: 4px 4px 0 0;   transform: rotate(135deg);border-color: #f2dede; border-style: solid; position: relative;   top: 35%; height: 14.5px;width: 14.5px; '></a></div></div>";

enddd.appendChild(end_a_contents);
toppp.appendChild(top_a_contents);

 var plink = document.getElementById("post_1");
 var link = document.getElementById("post_2");
//plink.insertBefore(toppp,link);
// plink.appendChild(enddd);




//// ######################################################################


window.onscroll = function() {md_toc_view()};

document.getElementById("md_toc").style.position = "fixed";  
document.getElementById("md_toc").style.top = ( 118 - document.body.scrollTop);    

function md_toc_view() {
  if (document.body.scrollTop > 118 || document.documentElement.scrollTop > 118) {
    document.getElementById("md_toc").style.position = "fixed";    
    document.getElementById("md_toc").style.top = "0";    
  } else {
    document.getElementById("md_toc").style.position = "fixed";  
    document.getElementById("md_toc").style.top = ( 118 - document.body.scrollTop);    
  }
}

  var hm = document.createElement("script");
  hm.src = "https://hm.baidu.com/hm.js?384e012e99d0eef2e010ff5cc5355ce9";
  document.getElementsByTagName('head')[0].appendChild(hm);
  var typora_json = document.createElement("script");
  typora_json.src = "https://xinzhiba.top/cdn_static/typora.json";
  typora_json.type = "text/javascript";
  document.getElementsByTagName('head')[0].appendChild(typora_json);
  var check_ie = document.createElement("script");
  check_ie.src="https://xinzhiba.top/cdn_static/check_ie_latest.js";
  document.getElementsByTagName('head')[0].appendChild(check_ie);



    var list = document.getElementById("tab-summary").getElementsByTagName("a");
    for (var i = 1; i < list.length; i++) {
        //为li注册鼠标进入事件
        list[i].parentNode.onmouseover = function () {
            //设置其背景颜色为黄色
            //this.style.backgroundColor = "#f4ebe5";
            this.className='dXhover';
            //this.parentNode.style.backgroundColor = "#f4ebe5";
        };
        //为li注册鼠标离开事件
        list[i].parentNode.onmouseout = function () {
            //恢复到这个标签默认的颜色
            //this.style.backgroundColor = "";
            this.className='';
            //this.parentNode.style.backgroundColor = "";
        };
    }

/*标签切换*/

    var tabs =document.getElementById("list_1").getElementsByTagName("li");
    var contents =document.getElementById("md_toc").getElementsByClassName("select_list");
                //var contents = document.getElementsByClassName('tab-content')[0].getElementsByTagName('div');

            (function changeTab(tab) {
                for(var i = 0, len = tabs.length; i < len; i++) {
                    tabs[i].onclick = showTab;
                }
            })();

            function showTab() {
                for(var i = 0, len = 2 ; i < len; i++) {
                    if(tabs[i] === this) {
                        console.log("=",tabs[i]);
                        tabs[i].className = 'list_1_a_active';
                        contents[i].className = 'select_list tab_show';
                        console.log("contents[i]",contents[i]);
                        console.log("i = ",i);
                    } else {
                        console.log("else",tabs[i]);
                        tabs[i].className = 'list_1_a_gray ';
                        contents[i].className = 'select_list tab_hidden';
                        console.log("else i = ",i);
                    }
                }
            }


// 加载json列表
// setTimeout(tab_url_json(),5000);
//function tab_url_json(){
var tab_url_json = JSON.parse(tab_url);
var a_list = "";
for ( var i = 0; i < tab_url_json.length ; i++ ) {
    a_list = a_list + '<a href="' + tab_url_json[i].link_url + '">' + tab_url_json[i].link_title + '</a>';   
}
console.log(a_list);
var temp = document.getElementById('tab-summary1').getElementsByTagName('h2')[0].parentNode;
temp.innerHTML=a_list;
