
/**
         * 参数：
         * 1. date: 秒级时间；
         * 2. fmt：格式化的格式 'yyyy', 'yyyy-MM-dd', 'yy-MM-dd', 'yyyy-MM-dd hh:mm:ss', ...(按需求自己定义格式)
         *       fmt给一个默认参数
         * RegExp.$1是正则对象里的内置符号，表示与 regexp 中的第1子表达式相匹配的文本。$2、...、$99同理
         * 注意年份与其他时间区分开，因为年份正常的表达是4位，所以单独判断
         */
function formatDate(date, fmt="yyyy/MM/dd hh:mm:ss") {
     if(typeof date === 'string'){
       return date;
     }
     date = new Date(date *1000);
     var o = {
      'M+': date.getMonth() + 1, //月份
      'd+': date.getDate(), // 日
      'h+': date.getHours(), //时
      'm+': date.getMinutes(), // 分
      's+': date.getSeconds(), // 秒
      'q+': Math.floor((date.getMonth()+3) / 3), // 季度
      'S' : date.getMilliseconds(), //毫秒
     }
    
     if (!date || date == null) return null;
    
     if(/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length))
      for(var k in o){
        if(new RegExp('('+k+')').test(fmt)){
          fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00'+o[k]).substr((''+o[k]).length)));
        }
     }
     return fmt;
   
}
dayjs().format();

fetch('/main/pod/imy/data/json_api_links_all.json').then(function(response) {
  return response.json().then(function(json) {
      var list_latest_publish = '';
      for(j=0;j<8;j++){
         var date = new Date(json[j].link_time);
         list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + dayjs(json[j].link_time).format('MM/DD')  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
         //list_latest_publish = list_latest_publish + '<li class="new list list-edit"><span class="date" style="display: block;">' + (date.getMonth()+1) + '/' +date.getDate()  + '</span><span class="title"><i class="icon-flag"></i> <a target="_blank"  href="' + json[j].link_url + '">' + json[j].link_title + '</a></span></li>';;
      }
      document.getElementById("latest_publish").innerHTML=list_latest_publish;

      var list_random = '';
      json.sort(function () { return 0.5 - Math.random() ;});
      for(j=0;j<8;j++){
         list_random = list_random + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random").innerHTML=list_random;

      var list_random2 = '';
      for(j=8;j<16;j++){
         list_random2 = list_random2 + '<li class="new list list-edit"><span class="date" style="display: block;">'+json[j].link_rating +'</span><span class="title"><i class="icon-hand-right"></i> <a target="_blank" href="'+ json[j].link_url +'">' + json[j].link_title + '</a></span></li>';
      }
      document.getElementById("random2").innerHTML=list_random2;

  });
});


var realip = '';
fetch('/geoip2').then(function(response) {
  return response.json().then(function(json) {
      var realip = json.realip;
      document.getElementById("dropdown-menu-6").innerText=realip;
      //document.getElementById("dropdown-menu-6").innerText=realip;

  }).then(function(){
  var realip = document.getElementById("dropdown-menu-6").innerText;
  fetch('/main/pod/pages/php/ip2region?ip='+ realip ).then(function(response){
      return response.json().then(function(json){
         document.getElementById("dropdown-menu-6").innerHtml='<a href="//haudi.top/geoip2" target="_blank">' + json.ip + json.region + '</a>';
         //document.getElementById("dropdown-menu-6").innerText=json.ip + json.region;
      });
  });
 });
});
/*
async function getip2region(){
  var realip = document.getElementById("dropdown-menu-6").innerText;
  fetch('/main/pod/pages/php/ip2region?ip='+ realip ).then(function(response){
      return response.json().then(function(json){
      //var text = json.region;
      document.getElementById("dropdown-menu-6").innerText=json.region;
  });
});
}
*/
/*
var realip = document.getElementById("dropdown-menu-5").innerText
console.log(realip);
const response_ip2 = fetch(document.getElementById("dropdown-menu-5").inner);
realip = response_ip2.text();
document.getElementById("dropdown-menu-4").innerHTML=realip;

*/
