// 类型
var lx = getUrlParam(location.href, "lx") || "";

// 公交
var BUS = 4;
// 地铁
var SUBWAY = 5;
// 出租车
var TAXI = 6;
// 商超
var MALL = 7;
// 公共服务
var PUBLICSERVICE = 8;
// 公司班车
var SHUTTLEBUS = 9;
// 医疗机构
var MEDICAL = 10;
// 农贸市场
var FARMERMARKET = 11;
// 专业市场
var SPECIALIZEDMARKET = 12;
// 大综合——工地、园区、五小门店、产业园等等
var COMPREHENSIVE = 13;
// 景区
var SCENIC = 50;
// 机场
var AIRPORT = 51;


// 4-BUS:公交, 5-SUBWAY:地铁, 6-TAXI:出租, 7-MALL:商超, 8-PUBLICSERVICE:公共服务, 9-SHUTTLEBUS:公司班车,
// 10-MEDICAL:医疗机构, 11-FARMERMARKET:农贸市场, 12-SPECIALIZEDMARKET:专业市场, 13-COMPREHENSIVE:综合, 50-SCENIC:景区
//
//    **** 注意 ****
//    lang-lib.js、index.es5中使用
//
var specialTypes = [
  BUS,
  SUBWAY,
  TAXI,
  MALL,
  PUBLICSERVICE,
  SHUTTLEBUS,
  MEDICAL,
  FARMERMARKET,
  SPECIALIZEDMARKET,
  COMPREHENSIVE,
  SCENIC,
  AIRPORT
];

// 如果类型为特殊类型直接移除页面的无用表单字段
var isSpecialType = lx && specialTypes.includes(Number(lx));

var showWarningCountryList = ['990039', '990082', '990098', '990034', '990033', '990049', '990081', '990001', '990041', '990047'];

function isShowWarningCountry (code) {
  return showWarningCountryList.indexOf(code) > -1;
}

function checkName (name, msg) {
  var pattern = /^[\u4E00-\u9FA5\uf900-\ufa2d·\s\w]{2,64}$/;
  var result = pattern.test(name);

  if (!result) {
    throw msg;
  }

  return result;
}

function checkProv (val) {
  var pattern = /^[1-9][0-9]/;
  var provs = {
    11: "北京",
    12: "天津",
    13: "河北",
    14: "山西",
    15: "内蒙古",
    21: "辽宁",
    22: "吉林",
    23: "黑龙江 ",
    31: "上海",
    32: "江苏",
    33: "浙江",
    34: "安徽",
    35: "福建",
    36: "江西",
    37: "山东",
    41: "河南",
    42: "湖北 ",
    43: "湖南",
    44: "广东",
    45: "广西",
    46: "海南",
    50: "重庆",
    51: "四川",
    52: "贵州",
    53: "云南",
    54: "西藏 ",
    61: "陕西",
    62: "甘肃",
    63: "青海",
    64: "宁夏",
    65: "新疆",
    71: "台湾",
    81: "香港",
    82: "澳门",
  };

  if (pattern.test(val)) {
    if (provs[val]) {
      return true;
    }
  }

  return false;
}

function checkDate (val) {
  var pattern = /^(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)$/;

  if (pattern.test(val)) {
    var year = val.substring(0, 4);
    var month = val.substring(4, 6);
    var date = val.substring(6, 8);
    var date2 = new Date(year + "-" + month + "-" + date);

    if (date2 && date2.getMonth() == parseInt(month) - 1) {
      return true;
    }
  }

  return false;
}

function checkPhone (phone, msg) {
  if (!/^1[3456789]\d{9}$/.test(phone)) {
    throw msg;
  }
}

function checkCodeNew (val, msg) {
  if (!val || !/^[a-z0-9A-Z]{6,20}$/.test(val)) {
    throw msg;
  }
}

function checkCode (val, msg) {
  var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
  var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
  var parity = [1, 0, "X", 9, 8, 7, 6, 5, 4, 3, 2];
  var code = val.substring(17);

  if (p.test(val)) {
    var sum = 0;

    for (var i = 0; i < 17; i++) {
      sum += val[i] * factor[i];
    }

    if (parity[sum % 11] == code.toUpperCase()) {
      return true;
    }
  }

  throw msg;
}

function checkCodeNewWithReturn (val) {
  if (!val || !/^[a-z0-9A-Z]{6,20}$/.test(val)) {
    return false;
  }

  return true;
}

window.checkCodeWithReturn = function (val) {
  var p = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
  var factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
  var parity = [1, 0, "X", 9, 8, 7, 6, 5, 4, 3, 2];
  var code = val.substring(17);

  if (p.test(val)) {
    var sum = 0;

    for (var i = 0; i < 17; i++) {
      sum += val[i] * factor[i];
    }

    if (parity[sum % 11] == code.toUpperCase()) {
      return true;
    }
  }

  return false;
};

function checkID (val, msg) {
  if (typeof val !== "string") {
    throw new Error("ID is not string");
  }

  var result = false;

  if (checkCode(val)) {
    var date = val.substring(6, 14);

    if (checkDate(date)) {
      if (checkProv(val.substring(0, 2))) {
        result = true;
      }
    }
  }

  if (!result) {
    throw msg;
  }

  return result;
}

function checkEmpty (val, msg, len) {
  if (val) {
    if (val.trim() === "") {
      throw msg;
    }
    if (len && val.length > +len) {
      throw msg;
    }
  } else {
    throw msg;
  }
}

function checkAddr () {
  var result = false;
  return result;
}

function checkCarId (vehicleNumber, msg) {
  vehicleNumber = vehicleNumber.toUpperCase();
  var reg = /^(([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z](([0-9]{5}[DF])|([DF]([A-HJ-NP-Z0-9])[0-9]{4})))|([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领][A-Z][A-HJ-NP-Z0-9]{4}[A-HJ-NP-Z0-9挂学警港澳使领]))$/;
  var result = reg.test(vehicleNumber);

  if (!result) {
    throw msg;
  }

  return result;
}

window.qs = {
  parse: function parse (string) {
    var obj = {};
    var keyValues = string.split("&");
    keyValues.map(function (_keyValue) {
      var key = _keyValue.split("=")[0];

      var value = decodeURIComponent(_keyValue.split("=")[1]);
      obj[key] = value;
    });
    return obj;
  },
};

function trim (str) {
  if (typeof str !== "string") {
    // 不是字符串直接返回
    return str;
  }

  return str.replace(/\s*/g, "");
} //  获取当前时间

function getNowFormatDate () {
  var date = new Date();
  var seperator1 = "-";
  var year = date.getFullYear();
  var month = date.getMonth() + 1;
  var strDate = date.getDate();

  if (month >= 1 && month <= 9) {
    month = "0" + month;
  }

  if (strDate >= 0 && strDate <= 9) {
    strDate = "0" + strDate;
  }

  var currentdate = year + seperator1 + month + seperator1 + strDate;
  return currentdate;
}

function getUrlParam (url, id) {
  url = url + "";
  regstr = "/(\\?|\\&)" + id + "=([^\\&]+)/";
  reg = eval(regstr); //eval可以将 regstr字符串转换为 正则表达式
  result = url.match(reg); //匹配的结果是：result[0]=?sid=22 result[1]=sid result[2]=22。所以下面我们返回result[2]
  if (result && result[2]) {
    return result[2];
  }
}

// 身份证号脱敏处理，前prefix位，后suffix位
function desensitization (code, prefix, suffix) {
  var codeStr = String(code);
  var front = Number(prefix);
  var end = Number(suffix);

  if (codeStr.length <= front + end) {
    return code;
  } else {
    var val = "";
    val = codeStr.substr(0, front) + "********" + codeStr.substr(-end);
    return val;
  }
} // 日期格式化

function formatDate (date) {
  if (!date) {
    return "";
  } // 2020-02-07 12:03:04

  var arr = date.split(" ")[0].split("-");
  return arr[0] + "年" + arr[1] + "月" + arr[2] + "日";
}

function checkTyshyxdm (val, msg) {
  var tyshyxdmFlag = checkTyshyxdmLength(val);
  if (!tyshyxdmFlag) {
    throw msg;
  }
}

function checkTyshyxdmLength (str) {
  //必须为字母加数字且长度不小于8位
  if (str == null || !(str.length === 15 || str.length === 18)) {
    return false;
  }
  if (str.length === 15) {
    var reg1 = new RegExp(/^[0-9]+$/);
    if (!reg1.test(str)) {
      return false;
    }
    return true;
  } else {
    var reg2 = new RegExp(/[^_IOZSVa-z\W]{2}\d{6}[^_IOZSVa-z\W]{10}$/g);
    // if (!reg2.test(str)) {
    //   return false;
    // }
    // var numLength = getLength(str);
    // if (numLength < 3 || numLength > 8) {
    //   return false;
    // }
    return reg2.test(str);
  }
}

function getLength (str) {
  if (/[0-9]/i.test(str)) {
    return str.match(/[0-9]/gi).length;
  }
  return 0;
}

//统一社会信用代码
function Tyshyxdm () {
  this.firstarray = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
  ];
  this.firstkeys = [3, 7, 9, 10, 5, 8, 4, 2];
  this.secondarray = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "J",
    "K",
    "L",
    "M",
    "N",
    "P",
    "Q",
    "R",
    "T",
    "U",
    "W",
    "X",
    "Y",
  ];
  this.secondkeys = [
    1,
    3,
    9,
    27,
    19,
    26,
    16,
    17,
    20,
    29,
    25,
    13,
    8,
    24,
    10,
    30,
    28,
  ];
  this.verify = function (str) {
    var code = str.toUpperCase();

    /*
      统一社会信用代码由十八位的阿拉伯数字或大写英文字母（不使用I、O、Z、S、V）组成。
       第1位：登记管理部门代码（共一位字符）
       第2位：机构类别代码（共一位字符）
       第3位~第8位：登记管理机关行政区划码（共六位阿拉伯数字）
       第9位~第17位：主体标识码（组织机构代码）（共九位字符）
       第18位：校验码​（共一位字符）
      */
    if (code.length != 18) {
      return false;
    }
    var reg = /^\w\w\d{6}\w{9}\w$/;
    if (!reg.test(code)) {
      return false;
    }
    /*
       登记管理部门代码：使用阿拉伯数字或大写英文字母表示。​
       机构编制：1​
       民政：5​
       工商：9​
       其他：Y
       */
    reg = /^[1,5,9,Y]\w\d{6}\w{9}\w$/;
    if (!reg.test(code)) {
      return false;
    }
    /*
       机构类别代码：使用阿拉伯数字或大写英文字母表示。​
       机构编制机关：11打头​​
       机构编制事业单位：12打头​
       机构编制中央编办直接管理机构编制的群众团体：13打头​​
       机构编制其他：19打头​
       民政社会团体：51打头​
       民政民办非企业单位：52打头​
       民政基金会：53打头​
       民政其他：59打头​
       工商企业：91打头​
       工商个体工商户：92打头​
       工商农民专业合作社：93打头​
       其他：Y1打头​
       */
    reg = /^(11|12|13|19|51|52|53|59|91|92|93|Y1)\d{6}\w{9}\w$/;
    if (!reg.test(code)) {
      return false;
    }
    /*
       登记管理机关行政区划码：只能使用阿拉伯数字表示。按照GB/T 2260编码。​
       例如：四川省成都市本级就是510100；四川省自贡市自流井区就是510302。​
      */
    reg = /^(11|12|13|19|51|52|53|59|91|92|93|Y1)\d{6}\w{9}\w$/;
    if (!reg.test(code)) {
      return false;
    }
    /*
           主体标识码（组织机构代码）：使用阿拉伯数字或英文大写字母表示。按照GB 11714编码。
           在实行统一社会信用代码之前，以前的组织机构代码证上的组织机构代码由九位字符组成。格式为XXXXXXXX-Y。前面八位被称为“本体代码”；最后一位被称为“校验码”。校验码和本体代码由一个连字号（-）连接起来。以便让人很容易的看出校验码。但是三证合一后，组织机构的九位字符全部被纳入统一社会信用代码的第9位至第17位，其原有组织机构代码上的连字号不带入统一社会信用代码。
           原有组织机构代码上的“校验码”的计算规则是：​
           例如：某公司的组织机构代码是：59467239-9。那其最后一位的组织机构代码校验码9是如何计算出来的呢？
           第一步：取组织机构代码的前八位本体代码为基数。5 9 4 6 7 2 3 9
           提示：如果本体代码中含有英文大写字母。则A的基数是10，B的基数是11，C的基数是12，依此类推，直到Z的基数是35。
           第二步：​​取加权因子数值。因为组织机构代码的本体代码一共是八位字符。则这八位的加权因子数值从左到右分别是：3、7、9、10、5、8、4、2。​
           第三步：本体代码基数与对应位数的因子数值相乘。​
           5×3＝15，9×7＝63，4×9＝36，6×10＝60，
           7×5＝35，2×8＝16，3×4=12，9×2＝18​​
           第四步：将乘积求和相加。​
           15+63+36+60+35+16+12+18=255
           第五步：​将和数除以11，求余数。​​
           255÷11=33，余数是2。​​
        */
    var firstkey = this.calc(
      code.substr(8),
      this.firstarray,
      this.firstkeys,
      11
    );
    /*
       第六步：用阿拉伯数字11减去余数，得求校验码的数值。当校验码的数值为10时，校验码用英文大写字母X来表示；当校验码的数值为11时，校验码用0来表示；其余求出的校验码数值就用其本身的阿拉伯数字来表示。​
       11-2＝9，因此此公司完整的组织机构代码为 59467239-9。​​
      */
    var firstword;
    if (firstkey < 10) {
      firstword = firstkey;
    }
    if (firstkey == 10) {
      firstword = "X";
    } else if (firstkey == 11) {
      firstword = "0";
    }
    if (firstword != code.substr(16, 1)) {
      return false;
    }

    /*
           校验码：使用阿拉伯数字或大写英文字母来表示。校验码的计算方法参照 GB/T 17710。
           例如：某公司的统一社会信用代码为91512081MA62K0260E，那其最后一位的校验码E是如何计算出来的呢？
           第一步：取统一社会信用代码的前十七位为基数。9 1 5 1 2 0 8 1 21 10 6 2 19 0 2 6 0提示：如果前十七位统一社会信用代码含有英文大写字母（不使用I、O、Z、S、V这五个英文字母）。则英文字母对应的基数分别为：A=10、B=11、C=12、D=13、E=14、F=15、G=16、H=17、J=18、K=19、L=20、M=21、N=22、P=23、Q=24、R=25、T=26、U=27、W=28、X=29、Y=30​
           第二步：​​取加权因子数值。因为统一社会信用代码前面前面有十七位字符。则这十七位的加权因子数值从左到右分别是：1、3、9、27、19、26、16、17、20、29、25、13、8、24、10、30、2​8
           第三步：基数与对应位数的因子数值相乘。​
           9×1=9，1×3=3，5×9=45，1×27=27，2×19=38，0×26=0，8×16=128​
           1×17=17，21×20=420，10×29=290，6×25=150，2×13=26，19×8=152​
           0×23=0，2×10=20，6×30=180，0×28=0
           第四步：将乘积求和相加。​9+3+45+27+38+0+128+17+420+290+150+26+152+0+20+180+0=1495
           第五步：​将和数除以31，求余数。​​
           1495÷31=48，余数是17。​​
      */

    var secondkey = this.calc(code, this.secondarray, this.secondkeys, 31);
    /*
       第六步：用阿拉伯数字31减去余数，得求校验码的数值。当校验码的数值为0~9时，就直接用该校验码的数值作为最终的统一社会信用代码的校验码；如果校验码的数值是10~30，则校验码转换为对应的大写英文字母。对应关系为：A=10、B=11、C=12、D=13、E=14、F=15、G=16、H=17、J=18、K=19、L=20、M=21、N=22、P=23、Q=24、R=25、T=26、U=27、W=28、X=29、Y=30
       因为，31-17＝14，所以该公司完整的统一社会信用代码为 91512081MA62K0260E。​​
      */
    var secondword = this.secondarray[secondkey];
    if (!secondword || secondword != code.substr(17, 1)) {
      return false;
    }
    var word = code.substr(0, 16) + firstword + secondword;
    if (code != word) {
      return false;
    }
    return true;
  };
  this.calc = function (code, array1, array2, b) {
    var count = 0;
    for (var i = 0; i < array2.length; i++) {
      var a = code[i];
      count += array2[i] * array1.indexOf(a);
    }
    var remainder = count % b;
    return remainder === 0 ? 0 : b - remainder;
  };
}

function translateDate (text) {
  switch (text) {
    case "一月":
      return "Jan";
    case "二月":
      return "Feb";
    case "三月":
      return "Mar";
    case "四月":
      return "Apr";
    case "五月":
      return "May";
    case "六月":
      return "Jun";
    case "七月":
      return "Jul";
    case "八月":
      return "Aug";
    case "九月":
      return "Sept";
    case "十月":
      return "Oct";
    case "十一月":
      return "Nov";
    case "十二月":
      return "Dec";
    default:
      break;
  }
}

// 微信、支付宝音频Hack方案
(function (win, doc, undefined) {
  // 原理：调用链中的某个事件被标识为用户事件而非系统事件
  // 进而导致浏览器以为是用户触发播放而允许播放
  Audio.prototype._play = Audio.prototype.play;
  HTMLAudioElement.prototype._play = HTMLAudioElement.prototype.play;

  function wxPlay (audio) {
    /// <summary>
    /// 微信播放Hack
    /// </summary>
    /// <param name="audio" type="Audio">音频对象</param>

    WeixinJSBridge.invoke("getNetworkType", {}, function (e) {
      audio._play();
    });
  }

  function alipayPlay (audio) {
    /// <summary>
    /// 支付宝播放Hack
    /// </summary>
    /// <param name="audio" type="Audio">音频对象</param>

    AlipayJSBridge.call("getNetworkType", function (result) {
      audio._play();
    });
  }

  function play () {
    var self = this;

    self._play();

    try {
      wxPlay(self);
    } catch (ex) {
      document.addEventListener(
        "WeixinJSBridgeReady",
        function evt () {
          wxPlay(self);
          document.removeEventListener("WeixinJSBridgeReady", evt, false);
        },
        false
      );
    }

    try {
      alipayPlay(self);
    } catch (ex) {
      document.addEventListener(
        "AlipayJSBridgeReady",
        function evt () {
          alipayPlay(self);
          document.removeEventListener("AlipayJSBridgeReady", evt, false);
        },
        false
      );
    }
  }

  Audio.prototype.play = play;
  HTMLAudioElement.prototype.play = play;
})(window, document);
