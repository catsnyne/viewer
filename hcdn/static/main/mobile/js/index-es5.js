// 获取社区id
var sqid = getUrlParam(location.href, "sqid") || "";
var orgId = getUrlParam(location.href, "orgid") || "";

var isZh = locale == "zh";
if (isSpecialType && typeof Storage !== "undefined") {
  localStorage.currentLang = "zh";
  $("#lang-btn").hide();
  isZh = true;
}

document.querySelector('.page-group').classList.add(locale);

var sqmc = "";
var ZJLX = isZh ? ["", "身份证", "其他"] : ["", "ID card", "others"];
var WDFH = isZh ? ["否", "是"] : ["no", "yes"];
var JTSF = isZh
  ? ["", "航班", "火车", "私家车", "其他"]
  : ["", "Airline", "Train", "Car", "Others"];
var GLFS = isZh
  ? ["", "企业集中", "无需隔离", "居家隔离", "其他"]
  : ["", "In company", "No quarantine", "In home", "Others"];
var SCENICGRADE = ["AAAAA", "AAAA", "AAA", "AA", "A", "无"];
var COLOR = ["", "high-level", "mid-level", "low-level"];

// 小区类成功信息缓存
var submittedPersonList;

var current_select_id = "jg";

// 类型配置项
var texts = {
  4: {
    name: "公交",
    title: "公交乘客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民乘坐公交时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["车牌号", "线路号"],
  },
  5: {
    name: "地铁",
    title: "地铁乘客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民乘坐地铁时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["地铁站名称"],
  },
  6: {
    name: "出租",
    title: "出租乘客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民乘坐出租时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["车牌号", "出租车公司"],
  },
  7: {
    name: "商超",
    title: "商超顾客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民进入商超时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["社会信用代码", "商超名称"],
  },
  8: {
    name: "公共服务",
    title: "公共服务健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民使用公共服务时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["社会信用代码", "单位名称"],
  },
  9: {
    name: "公司班车",
    title: "公司班车乘客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民乘坐公司班车时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["车牌号", "公司名称"],
  },
  10: {
    name: "医疗机构",
    title: "医疗机构健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民进入医疗机构时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["", "医疗机构名称", "所属区县"],
  },
  11: {
    name: "农贸市场",
    title: "农贸市场顾客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民进入农贸市场时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["", "农贸市场名称", "所属区县"],
  },
  12: {
    name: "专业市场",
    title: "专业市场顾客健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民进入专业市场时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["", "专业市场名称", "所属区县"],
  },
  13: {
    name: "$",
    title: "$健康扫码",
    desc:
      "针对疫情防控战役，要求市内居民进入$时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["", "单位名称", "所属区县"],
  },
  50: {
    name: "景区",
    title: "景区游客健康扫码",
    desc:
      "针对疫情防控战役，要求游客进入景区时进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。",
    labels: ["", "景区名称", "景区级别"],
  },
};

var API_HOST = "/disncov";

// 记录当前位置信息
var currPosition = null;

// 从支付宝获取到的用户信息
var alipayUserInfo = {};

// 是否是支付宝打开;
var isAlipay =
  navigator.userAgent.search("Alipay") > -1 ||
  navigator.userAgent.search("alipay") > -1;

var alipay_url = "https://openauth.alipay.com/oauth2/publicAppAuthorize.htm?app_id=2021001139664853&scope=auth_user&redirect_uri=".concat(
  encodeURIComponent(location.href)
);

// 设置登记来源
var ua = window.navigator.userAgent.toLowerCase();
if (isZHBApp()) {
  $('input[name="source"]').val(5);
} else if (isAlipay) {
  $('input[name="source"]').val(1);
} else if (ua.search("micromessenger") > -1) {
  $('input[name="source"]').val(2);
} else {
  $('input[name="source"]').val(4);
}

/**
 * 支付宝鉴权、非支付宝获取存储中信息
 */
if (isZHBApp()) { //郑好办App
  getAppUserInfo(function (userInfo) {
    if (userInfo) {
      $("[name=idCardNum]").val(userInfo.idCode); // 身份证
      $("[name=name]").val(userInfo.realName); // 姓名
      $("[name=phone]").val(userInfo.phone); // 手机
      if (!sqmc && !isSpecialType) {
        getSqInfo(function () {
          preLoad();
        });
        return;
      }
      preLoad();
    } else {
      preLoad(function () {
        // 非支付宝打开时取localStorage中身份证
        checkLocalPersonInfo();
      });
    }
  });
} else if (isAlipay) {
  if (document.location.href.search("auth_code") == -1) {
    document.location.href = alipay_url;
  } else {
    loadIdentityInfo(function () {
      preLoad();
    });
  }
} else {
  preLoad(function () {
    // 非支付宝打开时取localStorage中身份证
    checkLocalPersonInfo();
  });
}

function checkLocalPersonInfo () {
  if (typeof Storage !== "undefined") {
    submittedPersonList = JSON.parse(
      localStorage.submittedPersonList || "[]"
    );
    if (submittedPersonList.length > 0) {
      if (isSpecialType) {
        //公交出租地铁商超不再确认身份,直接跳转成功页面
        var sfzId = submittedPersonList[0].split("-")[2];
        if (sfzId) {
          idCardPreCheck({ sfz: sfzId, sqid: sqid });
        }
      } else {
        var submitPersonOfSqId = [];
        $.each(submittedPersonList, function (index, item) {
          var itemArr = item.split("-");
          //if (itemArr[0] === String(sqid) || isSpecialType) {
          submitPersonOfSqId.push(itemArr[1] + "-" + itemArr[2]);
          //}
        });
        if (submitPersonOfSqId.length > 0) {
          createTable(submitPersonOfSqId);
          initTableClick();
          $(".page-group > .page-current").removeClass("page-current");
          $("#localStageList").addClass("page-current");
        }
      }
    }
  }
}

//进入页面优先判断二维码是否激活,以及人员信息是否登记
function preLoad (callback) {
  function _check () {
    var idCardNum = $("input[name=idCardNum]").val();
    if (idCardNum) {
      idCardPreCheck({
        sfz: idCardNum,
        sqid: sqid,
      });
    } else {
      //当扫码成功时,记录用户位置
      if (isSpecialType) {
        if (window.positionInfo) {
          currPosition = $.extend({}, window.positionInfo);
          $("#input-position").text(currPosition.formattedAddress);
        }
      }
    }
  }
  if (isSpecialType) {
    //隐藏无用表单字段
    var removeIds = [
      "sqwz",
      "destDetail",
      // "jg-picker-show",
      "carNo",
      "lzjtfs-picker",
      "glfs-picker",
    ];
    for (var i = 0, l = removeIds.length; i < l; i++) {
      var removeId = removeIds[i];
      var $removeDom =
        $("#" + removeId).length > 0
          ? $("#" + removeId)
          : $("[name=" + removeId + "]");
      if ($removeDom.length > 0) {
        $removeDom.closest("li").hide();
      }
    }
    //检验二维码是否绑定
    checkQrcodeStatus(function () {
      callback && callback();
      _check();
    });
  } else {
    callback && callback();
    //如果是小区登记直接进入信息校验环节,检查是否登记过信息
    _check();
  }
}

//回显绑定的公交/地铁/出租/商超的信息
function showOrgInfo (data) {
  // 如果二维码已经激活,校验身份证登记信息
  var $orgInfo = $("#orgInfo");
  //成功登记返回页面回显结果
  var $labels = $orgInfo.find(".item-label");
  for (var i = 0, l = texts[lx].labels.length; i < l; i++) {
    // label项不为空时设置text，跳过【编码】字段，后面有特定处理隐藏与显示
    if (texts[lx].labels[i]) {
      $labels.eq(i).text(texts[lx].labels[i]);
    }
  }
  // 根据name属性取data对应数据，填充显示
  $orgInfo.find(".item-value").each(function () {
    $(this).text(data[$(this).attr("name")]);
  });
  var $code = $orgInfo.find("[name=code]").closest("div");
  var $address = $orgInfo.find("[name=address]").closest("div");
  var $area = $orgInfo.find("[name=qymc]").closest("div");
  // var $grade = $orgInfo.find("[name=grade]").closest("div");
  var area = [
    MALL,
    PUBLICSERVICE,
    MEDICAL,
    FARMERMARKET,
    SPECIALIZEDMARKET,
    COMPREHENSIVE,
  ];
  var addr = [MALL, PUBLICSERVICE];
  var code = [MEDICAL, FARMERMARKET, SPECIALIZEDMARKET, COMPREHENSIVE, SCENIC];
  if (area.indexOf(+lx) == -1) {
    //商超/公共服务/医疗/农贸/专业市场显示区域信息
    $area.remove();
  }
  if (addr.indexOf(+lx) == -1) {
    //商超/公共服务显示地址信息
    $address.remove();
  }
  if (code.indexOf(+lx) > -1) {
    // 医疗，农贸，专业市场不显示【编码】
    $code.remove();
  }

  var $name = $orgInfo.find("[name=name]").closest("div");
  if (lx == SUBWAY) {
    //地铁不需要显示name属性
    $name.remove();
    $("#info-dqzm span").text(data.code);
  }
  if (lx == BUS || lx == TAXI || lx == SHUTTLEBUS) {
    $("#info-cph span").text(data.code);
  }
}

//检测二维码是否激活,如果未激活进行用户绑定操作
function checkQrcodeStatus (callback) {
  $.get(
    API_HOST + "/qrcode/check?sqid=" + sqid + "&lx=" + lx + "&orgId=" + orgId,
    function (res) {
      // 大综合类型
      if (lx == 13) {
        texts[lx].name = texts[lx].name.replace("$", res.data.orgMc);
        texts[lx].title = texts[lx].title.replace("$", res.data.orgMc);
        texts[lx].desc = texts[lx].desc.replace("$", res.data.orgMc);
      }

      var title = texts[lx].title || "全市居民小区健康登记管理系统";
      var desc =
        texts[lx].desc ||
        "应疫情防控要求，请郑州返程人员进行填报登记。个人信息不会对外泄露，请如实填写。同时郑州健康码系统是基于郑州市政务一体化服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。";
      if (res.data && res.data.entity) {
        // 成功返回
        sqid = res.data.entity.id; //将二维码id转成真正的社区id
        showOrgInfo(res.data.entity);
        callback && callback();
      } else {
        // sqid not found，进入二维码激活页面
        title = texts[lx]["name"] + "二维码激活";
        desc = "首次扫描二维码需要填写以下信息用来激活二维码,感谢您的配合!";
        var $orgForm = $("#orgForm");
        var $labels = $orgForm.find(".label");
        var $inputs = $orgForm.find("input");

        for (var i = 0, l = texts[lx].labels.length; i < l; i++) {
          // label项不为空
          if (texts[lx].labels[i]) {
            $labels.eq(i).text(texts[lx].labels[i]);
            $inputs.eq(i).attr("placeholder", "请输入" + texts[lx].labels[i]);
          }
        }

        var $code = $orgForm.find("[name=code]").closest("li");
        var $grade = $orgForm.find("[name=grade]").closest("li");
        var $address = $orgForm.find("[name=address]").closest("li");
        var $area = $orgForm.find("[name=qymc]").closest("li");
        // 通用隐藏【级别】【区域】【地址】，特定显示
        $grade.hide();
        $area.hide();
        $address.hide();
        // 需要筛选区域的类型
        var area = [
          MALL,
          PUBLICSERVICE,
          MEDICAL,
          FARMERMARKET,
          SPECIALIZEDMARKET,
          COMPREHENSIVE,
        ];
        var showGrade = [SCENIC];
        var addr = [MALL, PUBLICSERVICE];
        // code 如下处理亦可，目的：医疗，农贸，专业市场不显示【编码】项
        // var code = [MEDICAL, FARMERMARKET, SPECIALIZEDMARKET];
        if (area.indexOf(+lx) > -1) {
          // 不显示code项
          $code.hide();
          //当类型为商超/公共服务时展示区域信息
          $area.show();
        }
        if (showGrade.indexOf(+lx) > -1) {
          // 不显示code项
          $code.hide();
          //当类型为商超/公共服务时展示区域信息
          $grade.show();
          // 显示区域
          $area.show();
        }
        if (addr.indexOf(+lx) > -1) {
          // 显示社会信用代码
          $code.show();
          // 显示地址
          $address.show();
        }
        var $name = $orgForm.find("[name=name]").closest("li");
        if (lx == SUBWAY) {
          $name.hide();
        }
        // name本身就是显示的
        // else {
        //   $name.show();
        // }
        if (lx == BUS) {
          // 线路号非必填
          $name.find(".item-title").removeClass("item-required");
        }
        $orgForm.show();
        $("#userForm").hide();
      }
      document.title = title;
      $("#form .title").text(title);
      $(".card-title").text(desc);
    }
  );
}

function createTable (personList) {
  var nodeStr = "<tr>";
  $.each(personList, function (index, item) {
    var nameIdStr = item.split("-");
    var btn = isZh ? "此身份登记" : "Register with this identity";
    var del = isZh ? "删除" : "Delete";
    nodeStr +=
      "<td>" +
      nameIdStr[0] +
      "</td><td>" +
      nameIdStr[1] +
      '</td><td><a idCard="' +
      nameIdStr[1] +
      '" class="quickSubmit button button-fill">' +
      btn +
      '</a><a nameIdStr="' +
      nameIdStr +
      '" class="delBtn">' +
      del +
      "</a></td></tr>";
  });
  nodeStr += "</tr>";
  $("#listBody").append(nodeStr);
}

function initTableClick () {
  $(".quickSubmit").click(function () {
    var idCard = $(this).attr("idCard");
    idCardPreCheck({
      sfz: idCard,
      sqid: sqid,
    });
    $("#localStageList").removeClass("page-current");
  });
  $(".delBtn").click(function () {
    var nameIdStr = $(this).attr("nameIdStr");
    $(this)
      .parent()
      .parent()
      .remove();
    var index = submittedPersonList.indexOf(nameIdStr);
    submittedPersonList.splice(index, 1);
    localStorage.submittedPersonList = JSON.stringify(submittedPersonList);
  });
}

/**
 * 加载身份信息，使用支付宝扫码时，可以带回来部分身份信息
 *
 * @param {*} callback
 */
function loadIdentityInfo (callback) {
  var search = window.location.search ? window.location.search.slice(1) : "";
  $.each(search.split("&"), function (index, item) {
    var code = "";
    var kv = item.split("=");
    if (kv[0] == "auth_code") {
      code = kv[1];
      if (code != "") {
        $.ajax({
          url: ""
            .concat(API_HOST, "/api/aliPay/queryAliPayUserInfo?authCode=")
            .concat(code),
          success: function success (res, status, xhr) {
            if (res && res.data) {
              alipayUserInfo = res.data;

              if (alipayUserInfo.certNo) {
                $("[name=idCardNum]").val(alipayUserInfo.certNo); // 身份证
                $("[name=name]").val(alipayUserInfo.userName); // 姓名
                $("[name=phone]").val(alipayUserInfo.mobile); // 手机
              }

              if (!sqmc && !isSpecialType) {
                getSqInfo(callback);
                return;
              }
              callback && callback();
            }
          },
          error: function error (xhr, type) {
            console.log("alipay get user detail err");
          },
        });
      }
    }
  });
}

/**
 * 获取社区信息——社区位置，社区名称
 */
function getSqInfo (callback) {
  if (sqid) {
    $.ajax({
      url: API_HOST + "/community/getCommunityById?id=" + sqid,
      success: function success (res) {
        if (res && res.data) {
          $("#sqwz").text(res.data.sqwz);
          sqmc = res.data.mc;
        }
        callback && callback();
      },
    });
  }
}

/**
 * 调郑好办登录
 */
function loginApp (callback) {
  JSBridgeCall('login', function (e) {
    if (e && (e.result === true || e.result === 'true')) {
      window.location.reload();
    } else {
      callback();
    }
  });
}

/**
 * 调郑好办人脸识别
 */
function getonverified (callback) {
  JSBridgeCall('getonverified', {}, function (e) {
    if (e && (e.result === 'true' || e.result === true) &&
      (e.authLevel === 2 || e.authLevel === '2')) {
      window.location.reload();
    } else {
      callback();
    }
  })
}

function ready (callback) {
  if (window.AlipayJSBridge) {
    callback && callback();
  } else {
    document.addEventListener('AlipayJSBridgeReady', callback, false);
  }
}

function JSBridgeCall (name, params, func) {
  ready(function () {
    if (params === undefined) {
      window.AlipayJSBridge.call(name);
    } else if (typeof params === "function") {
      window.AlipayJSBridge.call(name, params);
    } else {
      window.AlipayJSBridge.call(name, params, func);
    }
  });
}

/**
 * 获取客户端用户信息
 */
function getAppUserInfo (callback) {
  JSBridgeCall('getAppUserInfo', function (result) {
    const { userInfo } = result
    if (!result) {
      callback();
    } else if (result.success === 'false') {
      // Toast({
      //   message: '请先完成登录',
      //   duration: 3000
      // })
      setTimeout(function () {
        loginApp(callback);
      }, 500)
    } else if (userInfo.authLevel !== 2) {
      // Toast({
      //   message: '请先完成实名与人脸识别认证',
      //   duration: 3000
      // })
      setTimeout(function () {
        getonverified(callback)
      }, 500)
    } else {
      callback(userInfo)
    }
  })
}

// 判断浏览器内核、手机系统等，使用
function isZHBApp () {
  var sUserAgent = navigator.userAgent.toLowerCase();
  return sUserAgent.indexOf('izzzwfwapp') > -1;
}

// 解决苹果微信的问题
window.isIOSWeChat = function () {
  var ua = window.navigator.userAgent.toLowerCase();
  return ua.includes("micromessenger") && ua.includes("iphone");
};

window.inputBlur = function (e) {
  document.body.scrollTop = document.body.scrollTop;
};

// onload
window.onload = function () {
  if (isIOSWeChat()) {
    // 解决苹果微信显示问题
    $("body").on("touchend", function (el) {
      if (el.target.tagName == "INPUT") {
        $("input").blur(inputBlur);
      }
    });
  }

  // 将下拉列表没选择的颜色置灰
  $("select").each(function (_index, _select) {
    if (_select.value === "") {
      _select.style.color = "#7a7a7a";
    }
  });

  // 设置来郑日期 设置min
  // $("input[name='destDate'").attr('min', getNowFormatDate())

  //如果是公交/地铁/商超或存在值时，不查询社区信息
  if (!sqmc && !isSpecialType) {
    getSqInfo();
  }

  // 设置地址textarea固定宽度，实现自适应换行
  $("#sqwz").attr("style", "width: ".concat($("#sqwz").outerWidth(), "px"));

  // 特殊类型提交
  $("#bindBtn").click(function () {
    var data = {
      sqid: sqid,
      lx: lx,
      orgid: orgId,
    };
    $("#orgForm input[name]:not([type=hidden])").each(function () {
      data[$(this).attr("name")] = $(this).val();
    });
    var $labels = $("#orgForm .label");
    try {
      // 医疗，农贸，专业市场，大综合不使用code项，隐藏显示，不需要校验必填
      var area = [MEDICAL, FARMERMARKET, SPECIALIZEDMARKET, COMPREHENSIVE];
      var showGrade = [SCENIC];
      if (area.indexOf(+lx) == -1 && showGrade.indexOf(+lx) == -1) {
        checkEmpty(data.code, "请输入" + $labels.eq(0).text()); // 校验联系人
      } else if (showGrade.indexOf(+lx) > -1) {
        checkEmpty($("#grade-picker-show").val(), "请选择级别");
        data.code = data.grade;
      } else {
        // 上送参数需要code字段，这四个类型用code = name
        data.code = data.name;
      }

      if (lx == MALL || lx == PUBLICSERVICE) {
        // 校验统一社会代码
        checkTyshyxdm(data.code, $labels.eq(0).text() + "格式不正确");
      }
      if (lx == PUBLICSERVICE || lx == COMPREHENSIVE) {
        // 校验区域必选
        checkEmpty($("#area-picker-show").val(), "请选择区域");
      }
      if (lx != SUBWAY && lx != BUS) {
        //如果类型为地铁不校验name信息
        checkEmpty(data.name, "请输入" + $labels.eq(1).text());
      }
      // 综合，送name为单位名称、园区名称
      // if (lx == COMPREHENSIVE) {
      //   data.name = data.code;
      // }
      checkEmpty(data.username, "请输入联系人"); // 校验联系人
      checkPhone(data.mobile, "联系方式格式有误"); // 校验手机号
    } catch (error) {
      $.toast(error);
      return false;
    }
    $.showIndicator(); // 开始遮罩 防止重复点击
    $.ajax({
      type: "POST",
      url: API_HOST + "/qrcode/activate",
      data: JSON.stringify(data),
      async: true, // 所有请求均为异步。如果需要发送同步请求，置true
      cache: false, // 浏览器是否应该被允许缓存GET响应。
      contentType: "application/json",
      dataType: "json",
      withCredentials: true,
      success: function success (res) {
        $.hideIndicator();
        if (res.errCode == 0) {
          $("#complete-icon")
            .removeClass("iconsystem-complete")
            .addClass("iconShape");
          $(".complete-info").text("您的二维码激活成功，请使用!");
          $(".showGuards").remove();
          $("#orgInfo")
            .next()
            .remove();
          $("#success #submit-time").html(moment().format("YYYY-MM-DD HH:mm")); // 填报信息，精确到分
          showOrgInfo(res.data);
        } else {
          $("#complete-icon")
            .removeClass("iconsystem-complete")
            .addClass("iconreminder");
          $(".complete-info").text("激活失败!");
          $("#orgInfo")
            .next()
            .remove();
          $("#orgInfo").remove();
          var userInfo = res.data; //绑定该二维码的人员信息
          var name = data.code || texts[lx].labels[0] || texts[lx].labels[1];
          if (+lx == SCENIC) {
            name = data.name;
          }
          $(".showGuards").text(
            userInfo.username +
            "(手机号:" +
            userInfo.mobile +
            ")已使用" +
            name +
            "进行激活。请联系管理员，联系电话:" +
            userInfo.managers
          );
          // .text(
          //   userInfo.username +
          //     "(手机号:" +
          //     userInfo.mobile +
          //     ")已使用" +
          //     data.code +
          //     texts[lx].labels[0] +
          //     "进行激活.请联系管理员,联系人电话:" +
          //     userInfo.managers +
          //     ")"
          // );
          $(".submit-info").remove();
        }
        $(".page-group > .page-current").removeClass("page-current");
        $("#success").addClass("page-current");
      },
    });
  });
};

// 是否外地返回
window.wdfhChange = function (radio) {
  $(".peer_temp_split")[radio.value === "0" ? "hide" : "show"]();
};
window.hwfhChange = function (radio) {
  if (radio.value === "0") {
    $("#cfdWrapper").show();
    $("#countrySelectItem").hide();
    $("#countrySelectInput").val('');
    $('input[name="gjbm"]').val('');
  } else {
    $("#cfdWrapper").hide();
    $("#countrySelectItem").show();
  }
};

$("#datetime-picker").calendar({
  // toolbarTemplate: `<header class="bar bar-nav">
  //     <button class="button button-link pull-left close-picker">取消</button>
  //     <button class="button button-link pull-right close-picker">确定</button>
  //     <h1 class="title">日期选择</h1>
  //     </header>`,
  minDate: moment()
    .subtract(3, "days")
    .format("YYYY-MM-DD"),
  maxDate: moment().format("YYYY-MM-DD"),
  onOpen: function (p) {
    if (!isZh) {
      $(".current-month-value").text(
        translateDate($(".current-month-value").text())
      );
      var week = ["Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"];
      for (var i = 1; i < week.length + 1; i++) {
        $(".picker-calendar-week-days div:nth-child(" + i + ")").text(
          week[i - 1]
        );
      }
    }
  },
});
createPicker("picker-idCardType", isZh ? "证件类型" : "ID type", ZJLX.slice(1));
createPicker(
  "lzjtfs-picker",
  isZh ? "交通方式" : "Transportation Method",
  JTSF.slice(1)
);
createPicker(
  "glfs-picker",
  isZh ? "隔离方式" : "Quarantine Manner",
  GLFS.slice(1)
);
createPicker("area-picker", isZh ? "区域" : "Districts", [
  "二七区",
  "航空港区",
  "郑东新区",
  "新郑市",
  "荥阳市",
  "经开区",
  "惠济区",
  "上街区",
  "中原区",
  "巩义市",
  "金水区",
  "管城区",
  "高新区",
  "登封市",
  "新密市",
  "中牟县",
]);
// 发现grade更符合语义
createPicker("grade-picker", isZh ? "级别" : "Grade", SCENICGRADE);
createPicker("gwfh-picker", '国外返回', WDFH)

function createPicker (id, title, values, onChange) {
  $("#".concat(id)).picker({
    toolbarTemplate: '<header class="bar bar-nav"><button class="button button-link pull-left close-picker">'.concat(
      isZh ? "取消" : "Cancel",
      '</button><button class="button button-link pull-right close-picker">'.concat(
        isZh ? "确定" : "Confirm",
        '</button><h1 class="title">'.concat(title, "</h1></header>")
      )
    ),
    cols: [
      {
        textAlign: "center",
        values: values,
      },
    ],
    onClose: function onClose () {
      $("#" + id + "-show").val($("#" + id).val());
      if (id === 'gwfh-picker') {
        if ($("#" + id).val() === "是") {
          $("#countrySelectItem").show();
          $("#cfdWrapper").hide();
        } else {
          $("#countrySelectItem").hide();
          $("#cfdWrapper").show();
        }
      }
    },
    onChange: onChange,
  });
}

function createCityPicker (id, title) {
  $("#".concat(id, "-picker")).cityPicker({
    toolbarTemplate: '<header class="bar bar-nav"><button class="button button-link pull-right close-picker">'.concat(
      isZh ? "确定" : "Confirm",
      '</button><h1 class="title">'.concat(title, "</h1></header>")
    ),
    onOpen: function onOpen () {
      if (!$("#".concat(id, "-picker")).val()) {
        $("#".concat(id, "-picker")).picker("updateValue");
      }
    },
    onClose: function onClose () {
      // if (current_select_id === "jg") {
      //   $("#jg-picker-show").val($("#jg-picker").val());
      // }

      if (current_select_id === "cfd") {
        $("#cfd-picker-show").val($("#cfd-picker").val());
      }
    },
  });
}

// createCityPicker("jg", "选择籍贯");
createCityPicker("cfd", isZh ? "选择出发地" : "Departure point");

// $("#jg-picker-show").click(function() {
//   current_select_id = "jg";
//   $("#jg-picker").picker("open");
// });
$("#cfd-picker-show").click(function () {
  current_select_id = "cfd";
  $("#cfd-picker").picker("open");
});
$("#lzjtfs-picker-show").click(function () {
  $("#lzjtfs-picker").picker("open");
});
$("#glfs-picker-show").click(function () {
  $("#glfs-picker").picker("open");
});
$("#area-picker-show").click(function () {
  $("#area-picker").picker("open");
});
$("#grade-picker-show").click(function () {
  $("#grade-picker").picker("open");
});
$("#djContinue").click(function () {
  $(".page-group > .page-current").removeClass("page-current");
  $("#form").addClass("page-current");
});
$("#agreement").on("click", function () {
  $.modal({
    title: isZh ? "承诺告知书" : "Commitment & Notification",
    afterText: '<div class="agreement-content">'.concat(
      isZh
        ? "<p>根据中华人民共和国国家卫生健康委员会2020年第1号公告的要求以及《国际卫生条例（2005）》、《中华人民共和国传染病防治法》等有关法律规定，为全力做好新型冠状病毒感染的肺炎疫情防控工作，有效切断病毒传播路径，坚决遏制疫情蔓延势头，确保人民群众生命安全和身体健康。</p>" +
        "<p>根据河南省郑州市新型冠状病毒感染的肺炎疫情防控领导小组办公室通告（第9号）第五条 所有进出人员要主动配合疫情防控工作，如实登记信息，接受体温检测，及时报告异常情况。对于在非常时期不听劝阻、寻衅滋事等行为，公安机关将坚决予以打击，维护良好的社会秩序。请您积极配合填写各项信息，如有瞒报情况可能造成您的不便，甚至可能将依法承担相关的法律责任，敬请配合！</p>" +
        '<p>郑州市健康码系统是基于郑州市一体化政务服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。</p>'
        : "<p>According to No. 1 Annoucement of National Health Commission of the Peoples Republic of China in 2020, International Health Regulations(2005), Law of the People's Republic of China on the Prevention and Treatment of Infections Diseases and relevant laws and regulations, Zhengzhou will spare no effort to accomplish epidemic prevention and control of COVID-19 and take effective measures to cut off the main route of transmission to stop the spread of the disease, with the purpose of safeguarding people's lives and property.</p>" +
        "<p>According to article 5 of the No.9 annoucement by COVID-19 Prevention and Control Leading Group of Zhengzhou, Henan personnel entering and leaving must cooperate with epidemic prevention and control, truthfully register information. take temperature check and report abnormal condition in time. People who disregard-warning or cause trouble, will be resolutely punished by public security organs, in order to maintain good order of sociaty. Please cooperate with the registration, providing falsein formation could cause your inconvenience, even legal responsibility. Your cooperation and support will be appreciated.</p>" +
        "<p>Zhengzhou City Health Code System is developed based on Zhengzhou City’s integrated government service platform.For the prevention and control of the epidemic, I agree to become a Zheng Haoban user of Zhengzhou's integrated government service platform.</p>",
      "</div>"
    ),
    extraClass: "notify-modal",
    buttons: [
      {
        text: isZh ? "我同意" : "I Agree",
      },
    ],
  });
});

$("#countrySelectInput").click(function () {
  // $(".page-group > .page-current").removeClass("page-current");
  // $("#countrySelectWrapper").addClass("page-current");
  $.popup('.popup-country');
})

window.clearPlaceholder = function (input) {
  $(input).attr("placeholder", input.value != "" ? null : "填写到达郑州日期");
}; // 表单校验

window.checkData = function (data) {
  try {
    if (isZh) {
      checkName(data.name, "姓名格式有误"); // 校验 name
    } else {
      checkEmpty(data.name, "Please fill in your name", 32);
    }
    checkEmpty(
      data.idCardType,
      isZh ? "请选择证件类型" : "Please choose your ID type"
    );
    if (data.idCardType === "1") {
      checkCode(
        (data.idCardNum || "").toUpperCase(),
        isZh ? "身份证格式有误" : "ID number format incorrect"
      ); // 校验身份证
    } else {
      checkCodeNew(
        data.idCardNum,
        isZh ? "护照/回乡证/台胞证号码格式有误" : "number format incorrect"
      ); // 校验身份证
    }
    checkPhone(
      data.phone,
      isZh ? "手机号格式有误" : "mobile number format incorrect"
    ); // 校验手机号
    // checkEmpty($("#xzdz-picker-show").val(), `请选择现住地址`);
    if (!isSpecialType) {
      //特殊类型不做校验
      checkEmpty(
        data.destDetail,
        isZh
          ? "请输入现住地址详细信息"
          : "Please fill in your current address detail"
      );
      // checkEmpty($("#jg-picker-show").val(), `请选择籍贯地址`);
    }

    checkEmpty(
      data.sfwdfh,
      isZh
        ? "请选择是否外地返回"
        : "please choose whether return from outside Zhengzhou"
    );
    if (data.sfwdfh === "1") {
      checkEmpty(
        $("#datetime-picker").val(),
        isZh
          ? "请选择来郑日期"
          : "please choose the date of arrival in Zhengzhou"
      );
      if (data.sfhwfh === "0") {
        checkEmpty(
          $("#cfd-picker-show").val(),
          isZh ? "请选择出发地地址" : "Please choose the point of your departure"
        );
      } else {
        checkEmpty(data.gj, isZh ? "请选择出发国家" : "Please select Departure country");
      }

      if (!isSpecialType) {
        //特殊类型不做校验
        checkEmpty(
          data.lzjtfs,
          isZh
            ? "请选择来郑交通方式"
            : "Please choose your method of transportation"
        );
        checkEmpty(
          data.glfs,
          isZh ? "请选择隔离方式" : "Please choose your quarantine manner"
        );
      }
    }
  } catch (error) {
    $.toast(error);
    return false;
  }
  return true;
};

$('input[name="idCardNum"]').blur(function () {
  var idCard = $(this).val();
  if (
    ($("input[name=idCardType]")[0].checked && checkCodeWithReturn(idCard)) ||
    ($("input[name=idCardType]")[1].checked && checkCodeNewWithReturn(idCard))
  ) {
    idCardPreCheck({
      sfz: idCard,
      sqid: sqid,
    });
  }
});

function encrypt (src, cryptoKey) {
  var key = window.CryptoJS.enc.Utf8.parse(cryptoKey);
  var encPassword = window.CryptoJS.AES.encrypt(src, key, {
    mode: window.CryptoJS.mode.ECB,
    padding: window.CryptoJS.pad.Pkcs7,
  });
  return encPassword.ciphertext.toString().toUpperCase();
}

function idCardPreCheck (data) {
  var cryptoKey = "8QONwyJtHesysWpN";
  var body = data;
  body.token = encrypt("".concat(data.sfz, "_").concat(Date.now()), cryptoKey);
  body.lylx = $('input[name="source"]').val();
  $.ajax({
    type: "POST",
    url: "".concat(API_HOST, "/code/preCheck"),
    data: JSON.stringify(body),
    async: true, // 所有请求均为异步。如果需要发送同步请求
    cache: false, // 浏览器是否应该被允许缓存GET响应。
    contentType: "application/json",
    dataType: "json",
    withCredentials: true,
    success: function success (res) {
      var result = res.data || {};
      // 郑好办拉新增加弹框提示
      if (result.zhbShowFlag) {
        $.modal({
          title: "",
          afterText: '<div>'.concat(
            isZh
              ? "<p>郑州健康码系统是基于郑州市一体化政务服务平台开发，为疫情防控，本人同意成为郑州市一体化政务服务平台“郑好办”用户。</p>"
              : "<p>Zhengzhou City Health Code System is developed based on Zhengzhou City’s integrated government service platform.For the prevention and control of the epidemic, I agree to become a Zheng Haoban user of Zhengzhou's integrated government service platform</p>" +
              "</div>"
          ),
          buttons: [
            {
              text: isZh ? "我知道了" : "I Know",
            },
          ],
        });
      }

      if (result.flag === 0) {
        if (!$("#form").hasClass("page-current")) {
          $(".page-group > .page-current").removeClass("page-current");
          $("#form").addClass("page-current");
        }

        var record = result.record;
        if (record && record.lzxxly) {
          // 需要回填信息，脱敏，不回显姓名
          // if (record.rymc) $('input[name="name"]').val(record.rymc);
          if (record.ryzjlx === 2)
            $("input[name=idCardType]")[1].checked = true;
          if (record.ryzjhm) $('input[name="idCardNum"]').val(record.ryzjhm);
          if (record.lxfs) $('input[name="phone"]').val(record.lxfs);

          // if (record.jgsf && record.jgds && record.jgxq) {
          //   $("#jg-picker-show").val(
          //     record.jgsf + " " + record.jgds + " " + record.jgxq
          //   );
          // }
          // 非外地返回，选中否，隐藏下方输入项
          if (String(record.sfwdfh) === "0") {
            $('input[name="sfwdfh"]')
              .eq(1)
              .attr("checked", "true");
            $(".peer_temp_split").hide();
          } else {
            if (record.lzrq) $("#datetime-picker").val(record.lzrq);
            if (record.cfdsf && record.cfdds && record.cfdxq) {
              $("#cfd-picker-show").val(
                record.cfdsf + " " + record.cfdds + " " + record.cfdxq
              );
            }
            if (record.lzjtfs)
              $("#lzjtfs-picker-show").val(JTSF[record.lzjtfs]);
            if (record.glfs) $("#glfs-picker-show").val(GLFS[record.glfs]);
            if (record.lzxxly && String(record.lzxxly) === "2") {
              $("#datetime-picker").attr("disabled", "disabled");
              $('#sfhwfh-item').hide();
              $("#cfd-picker-show").attr("disabled", "disabled");
            }
            // // 国外返回，赋值国家选择相关输入框，否则提交表单无法校验
            // if (record.cfdsfbm === "99" && record.cfddsbm === "9900" && record.cfdxq !== "其他") {
            //   $("#countrySelectInput").val(record.cfdxq);
            //   $('input[name=gjbm]').val(record.cfdsfbm + record.cfddsbm + record.cfdxqbm)
            // }
          }
        }
        if (isSpecialType) return; // 如果不是小区不再执行以下操作
        // 0：可以填写
        // $.modal({
        //   title: isZh
        //     ? "请问您是否为".concat(sqmc, "的住户")
        //     : "Do you live in ".concat(sqmc),
        //   text: '<p class="confimtipred">'.concat(
        //     isZh
        //       ? "谨慎填写，防控期间只允许有一个居住地，请如实填写，如有瞒报情况可能造成您的不便，甚至可能将依法承担相关的法律责任。敬请配合！"
        //       : "Please fill it carefully. During the prevention and control period, only one place of residence is allowed. Please cooperate!",
        //     "</p>"
        //   ),
        //   buttons: [
        //     {
        //       text: isZh ? "否" : "No",
        //       onClick: function onClick() {
        //         showSuccessResult(result.record, 2);
        //       },
        //     },
        //     {
        //       text: isZh ? "是" : "Yes",
        //       onClick: function onClick() {},
        //     },
        //   ],
        // });
      } else if (
        result.flag === 1 ||
        (result.flag === 2 && isSpecialType) ||
        result.flag === 3 ||
        result.flag === 4 ||
        result.flag === 5 ||
        result.flag === 6
      ) {
        // 0：可以填写，
        // 1：本小区正常人，回显填写的信息，
        // 2：非本小区，
        // 3：本小区隔离期人员（未满7天）
        // 4：本小区隔离期人员（未满14天）
        showSuccessResult(result.record, result.flag);

        if (!isAlipay && typeof Storage !== "undefined") {
          var storageStr =
            sqid + "-" + result.record.rymc + "-" + result.record.ryzjhm;
          // if (submittedPersonList.indexOf(storageStr) === -1) {
          //   submittedPersonList.push(storageStr);
          //   localStorage.submittedPersonList = JSON.stringify(
          //     submittedPersonList
          //   );
          // }
          if (JSON.stringify(submittedPersonList) === "[]") {
            localStorage.submittedPersonList = JSON.stringify([storageStr]);
          }
        }
        // 当扫码成功时, 统计扫码信息、记录用户位置
        if (isSpecialType) {
          var params = {
            sfz: data.sfz,
            sqid: sqid,
            token: body.token,
          };
          if (window.positionInfo) {
            var position = window.positionInfo.position; //经纬度信息
            var address = window.positionInfo.formattedAddress;
            params.location = address;
            params.longitude = position.lng;
            params.latitude = position.lat;
          }
          savePositionInfo(params);
        }
      } else if (result.flag === 2 && !isSpecialType) {
        // 提示非本小区
        // showSuccessResult(result.record, result.flag);
      } else {
        // $.alert(
        //   isZh
        //     ? "您处于隔离期，请自行隔离！"
        //     : "You are in quarantine, please isolate yourself!",
        //   isZh ? "提示" : "Tips",
        //   function () { }
        // );
      }
    },
    error: function error (_error) { },
  });
} // 表单提交

function savePositionInfo (body) {
  var param = body;
  param.lylx = $('input[name="source"]').val();
  $.ajax({
    type: "POST",
    url: "".concat(API_HOST, "/code/save/location"),
    data: JSON.stringify(param),
    async: true, // 所有请求均为异步。如果需要发送同步请求
    cache: false, // 浏览器是否应该被允许缓存GET响应。
    contentType: "application/json",
    dataType: "json",
    withCredentials: true,
    success: function success (res) {
      if (res.errCode === 0) {
        $("#input-position").text(body.location);
      }
    },
    error: function error (_error) { },
  });
}

/*
 * 点击提交，表单数据处理
 */
$("#submit-form").on("click", function () {
  var data = qs.parse($("#userForm").serialize());
  console.log(data)
  data.name = trim(data.name); // 姓名
  data.phone = trim(data.phone); // 手机号码
  data.idCardNum = trim(data.idCardNum); // 身份证/其他
  if (!checkData(data)) {
    return;
  }
  if (!$("input[name='agreement']").is(":checked")) {
    $.toast(
      isZh
        ? "请阅读并同意《承诺告知书》"
        : "<div class='warings'>Please read and comply with the commitment notification<div>"
    );
    return;
  }

  // var jgStrList = $("#jg-picker-show")
  //   .val()
  //   .split(" "); // 籍贯
  // var jgBmList = $.getCodeOfArea($("#jg-picker-show").val()).split(",");

  var cfdStrList = $("#cfd-picker-show")
    .val()
    .split(" "); // 出发地

  var cfdBmList = [];// 出发地

  if ($("#cfd-picker-show").val() === "其他 其他 其他") {
    cfdBmList = [99, 9900, 990000];
  } else {
    cfdBmList = $.getCodeOfArea($("#cfd-picker-show").val()).split(",");
  }

  var submitData = {};
  var recordData = {
    sqid: sqid, // 社区ID
    sqmc: sqmc, // 社区名称
    lylx: $('input[name="source"]').val(), // 来源类型
    rymc: data.name, // 人员名称
    ryzjlx: Number(data.idCardType), // ZJLX.indexOf(data.idCardType), // 人员证件类型
    ryzjhm: data.idCardNum, // 人员证件号码
    lxfs: data.phone, // 联系方式
    jzdxxdz: $("#sqwz").text() + data.destDetail, // 现居住地详细信息
    // jgxx: $("#jg-picker-show")
    //   .val()
    //   .replace(/\s+/g, ""), // 籍贯信息
    // jgsf: jgStrList[0],
    // jgds: jgStrList[1],
    // jgxq: jgStrList[2],
    // jgsfbm: jgBmList[0],
    // jgdsbm: jgBmList[1],
    // jgxqbm: jgBmList[2],
    sfwdfh: Number(data.sfwdfh), // WDFH.indexOf(data.sfwdfh), // 是否外地返回
    lzrq: $("#datetime-picker").val() ? $("#datetime-picker").val() : undefined,
    lzjtfs: JTSF.indexOf(data.lzjtfs),
    // lzjtbm: data.lzjtbm,

    glfs: data.sfwdfh === "1" ? GLFS.indexOf(data.glfs) : 2,
    carNo: data.carNo,
    remark: ""
  };

  if (data.sfhwfh === "0") {
    recordData.cfdsfbm = data.sfwdfh === "1" ? cfdBmList[0] : "41";
    recordData.cfddsbm = data.sfwdfh === "1" ? cfdBmList[1] : "4101";
    recordData.cfdxqbm = data.sfwdfh === "1" ? cfdBmList[2] : "";
    recordData.cfdsf = data.sfwdfh === "1" ? cfdStrList[0] : "河南省";
    recordData.cfdds = data.sfwdfh === "1" ? cfdStrList[1] : "郑州市";
    recordData.cfdxq = data.sfwdfh === "1" ? cfdStrList[2] : "";
  } else { // 海外返回
    var hwfhCodeList = $('input[name=gjbm]').val().split('-');
    recordData.cfdsfbm = hwfhCodeList[0];
    recordData.cfddsbm = hwfhCodeList[1];
    recordData.cfdxqbm = hwfhCodeList[2];
    recordData.cfdsf = "其他";
    recordData.cfdds = "其他";
    recordData.cfdxq = $("#countrySelectInput").val();
  }
  submitData.record = recordData;
  if (currPosition) {
    var position = currPosition.position; //经纬度信息
    var address = currPosition.formattedAddress;
    submitData.location = address;
    submitData.longitude = position.lng;
    submitData.latitude = position.lat;
  }
  $.showIndicator(); // 开始遮罩 防止重复点击
  window.submitAjax(submitData); // 开始提交表单
});

// 提交表单，
window.submitAjax = function (data) {
  var cryptoKey = "8QONwyJtHesysWpN";
  var body = data;
  body.token = encrypt(
    "".concat(data.record.ryzjhm, "_").concat(Date.now()),
    cryptoKey
  );
  $.ajax({
    type: "POST",
    url: "".concat(API_HOST, "/code/record"),
    data: JSON.stringify(body),
    async: true, // 所有请求均为异步。如果需要发送同步请求
    cache: false, // 浏览器是否应该被允许缓存GET响应。
    contentType: "application/json",
    dataType: "json",
    withCredentials: true,
    success: function success (res) {
      // $("#link-success").click();
      $.hideIndicator();
      if (res.errCode === 0 && res.errMsg === "SUCCESS") {
        var flagReturn = res.data.flag;
        // var color = res.data.color;
        // 0：可以填写，
        // 1：本小区正常人，回显填写的信息，
        // 2：非本小区，
        // 3：本小区隔离期人员（未满7天）
        // 4：本小区隔离期人员（未满14天）
        if (
          flagReturn === 1 ||
          flagReturn === 3 ||
          flagReturn === 4 ||
          flagReturn === 5 ||
          flagReturn === 6
        ) {
          showSuccessResult(res.data.record, flagReturn);
          // 非支付宝打开时，缓存成功登记的信息
          if (!isAlipay) {
            if (typeof Storage !== "undefined") {
              // var tempList = JSON.parse(
              //   localStorage.submittedPersonList || "[]"
              // );
              // tempList.push(
              //   sqid + "-" + data.record.rymc + "-" + data.record.ryzjhm
              // );
              // 新增成功覆盖 缓存中身份登记信息
              localStorage.submittedPersonList = JSON.stringify([
                sqid + "-" + data.record.rymc + "-" + data.record.ryzjhm,
              ]);
            }
          }
        } else if (flagReturn === 2) {
          // 提示非本小区
          showSuccessResult(res.data.record, flagReturn);
        } else {
          showSuccessResult(res.data.record, flagReturn);
        }
      } else {
        $.toast(res.errMsg);
      }
    },
    error: function error (_error2) {
      var response = _error2.response;
      response = JSON.parse(response);
      $.hideIndicator();

      if (response.errors && response.errors.length > 0) {
        $.toast(response.errors[0].defaultMessage);
      } else {
        $.toast(isZh ? "提交信息失败!" : "Failed to submit");
      }
    },
  });
};

/*
 * 关闭picker，有bug不关闭切换页面的bug
 */
function closePicker () {
  $("#jg-picker").picker("close");
  $("#datetime-picker").picker("close");
  $("#lzjtfs-picker").picker("close");
  $("#glfs-picker").picker("close");
  $("#area-picker").picker("close");
}

/**
 * 显示登记结果
 * @param {*} data
 */
function showSuccessResult (data, flagReturn) {
  // 关闭picker
  closePicker();
  var audio = new Audio("images/warning.mp3");
  audio.loop = "loop";
  if (isSpecialType) {
    $(".page-group > .page-current").removeClass("page-current");
    $("#specialTypesResult").addClass("page-current");
    // 填报信息，精确到分
    $("#specialTypesResult #submit-time").html(
      moment().format("YYYY-MM-DD HH:mm")
    );

    var resMessages = {
      4: "欢迎乘坐!",
      5: "欢迎乘坐!",
      6: "欢迎乘坐!",
      7: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      8: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      9: "欢迎乘坐!",
      10: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      11: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      12: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      13: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
      50: "欢迎来到" + $("#orgInfo span[name=name]").text() + "!",
    };
    var $completeInfo = $(".complete-info");
    // $("#info-name").text(data.rymc);
    // $("#info-zjhm").text(desensitization(data.ryzjhm, 6, 4));
    $("#info-lzrq span").text(data.lzrq);
    $("#info-lzrq").show();

    // 1 正常扫码-绿色
    // 3 禁止进入-红色
    // 5 待确认隔离扫码-绿色带提醒
    // 6 红色带提醒
    if (flagReturn == 1 || flagReturn == 5) {
      $completeInfo.text(resMessages[lx]);
      $(".showGuards").show();
      if (flagReturn == 5) {
        $("#warning-tips").text("如您在郑州停留，请尽快前往所在社区疫情防控部门报到，谢谢配合！");
        // $("#warning-tips").addClass("red-tip");
      }
    } else if (flagReturn == 3 || flagReturn == 6) {
      $(".finish-header").removeClass("specialType-success").addClass("specialType-warning");
      $(".finish-icon").removeClass("iconsucceed").addClass("iconmistake");
      $completeInfo.text("如您在郑州停留，请尽快前往所在社区疫情防控部门报到，谢谢配合！");
      if (flagReturn == 6) {
        // $completeInfo.text("您在隔离期");
        // $("#warning-tips").text("请到居住地报到");
        // $("#warning-tips").addClass("black-tip");
      }
      if (isShowWarningCountry(data.cfdxqbm)) {
        audio.play();
        $("#warningMask").addClass("warning-animation");
        $("#warningMask").show();
      }
      // $(".content-block").hide();
    } else if (flagReturn == 2) { // 地铁黄码：需要进行核酸检测
      $(".finish-header").removeClass("specialType-success").addClass("specialType-yellow");
      $(".finish-icon").removeClass("iconsucceed").addClass("iconmistake").addClass("yellow");
      $completeInfo.text("如您在郑州停留，请尽快前往所在社区疫情防控部门报到，谢谢配合！");
    }
    if (lx == SCENIC) {
      $("#info-name span").text(data.rymc);
      $("#info-name").show();
    }
    // 公交，出租，公司班车显示车牌号
    if (lx == BUS || lx == TAXI || lx == SHUTTLEBUS) {
      $("#info-cph").show();
    }
    // 当前站名
    if (lx == SUBWAY) {
      $("#info-dqzm").show();
    }
    return false;
  } else {
    $(".page-group > .page-current").removeClass("page-current");
    $("#sqResult").addClass("page-current");

    $("#sqResult #submit-time").text(moment().format("YYYY-MM-DD HH:mm"));
    if (flagReturn === 2) {
      // $(".finish-header").addClass("forbid-fbxq-bg");
      // $(".complete-info p").text(
      //   isZh ? "您非本小区住户!" : "You are not a resident of this community"
      // );
      // $(".success-detail").html("".concat(sqmc, isZh ? "小区" : ""));
      // $(".success-detail").show();
    } else if (flagReturn === 1) {
      $("#success-sqmc").text("".concat(sqmc, isZh ? "小区" : "community"));
    } else {
      $(".complete-info p").text(
        isZh ? "如您在郑州停留，请尽快前往所在社区疫情防控部门报到，谢谢配合！" : "If you stay in Zhengzhou, please report to the epidemic prevention and Control Department of your community as soon as possible"
      );
      $("#host-glts").text(
        data.gljcsj
          ? moment(data.gljcsj).format(isZh ? "YYYY年MM月DD日" : "YYYY-MM-DD")
          : isZh
            ? "无"
            : "none"
      );
      $("#host-glts")
        .parent()
        .show();
      if (flagReturn === 3) {
        // $("#success-sqmc").text(
        //   isZh
        //     ? "您可向小区管理员提供《新冠肺炎健康申报证明》解除隔离"
        //     : "Please provide the community administrator with health declaration of COVID-19 to end your quarantine"
        // );
        $(".finish-header").addClass("warning-ng");
        if (!isZh) $("#success-sqmc").attr("style", "font-size:12px");
      }
      if (flagReturn === 4) {
        // $("#success-sqmc").text(
        //   isZh
        //     ? "请在" + sqmc + "小区居家隔离"
        //     : "Please observe home quarantine in " + sqmc
        // );
        $(".finish-header").addClass("forbid-glq-bg");
        if (isShowWarningCountry(data.cfdxqbm)) {
          audio.play();
          $("#warningMask").addClass("warning-animation");
          $("#warningMask").show();
        }
      }
    }
    $("#host-rymc").html(data.rymc);
    $("#host-ryzjhm").html(desensitization(data.ryzjhm, 6, 4));
  }
}

// 等待倒计时
window.waitSecond = function () {
  setTimeout(function () {
    var second = Number($("#wait-second").text());
    $("#wait-second").text(second - 1);

    if (second > 1) {
      waitSecond();
    } else {
      $("#wait-text").text("点击刷新");
    }
  }, 1000);
};
// refresh
window.refresh = function () {
  if ($("#wait-text").text() === "点击刷新") {
    window.location.reload();
  }
};

var random = Math.random();
var busyIndex = 0; // 拥堵系数

if (random < busyIndex) {
  $(".page-group > .page-current").removeClass("page-current");
  $("#busy").addClass("page-current");
  window.waitSecond();
}

$.fn.scrollTo = function (options) {
  var defaults = {
    toT: 90, //滚动目标位置
    durTime: 500, //过渡动画时间
    delay: 30, //定时器时间
    callback: null //回调函数
  };
  var opts = $.extend({}, defaults, options),
    timer = null,
    _this = this,
    curTop = _this.scrollTop(), //滚动条当前的位置
    subTop = opts.toT - curTop, //滚动条目标位置和当前位置的差值
    index = 0,
    dur = Math.round(opts.durTime / opts.delay),
    smoothScroll = function (t) {
      index++;
      var per = Math.round(subTop / dur);
      if (index >= dur) {
        _this.scrollTop(t);
        window.clearInterval(timer);
        if (opts.callback && typeof opts.callback == 'function') {
          opts.callback();
        }
        return;
      } else {
        _this.scrollTop(curTop + index * per);
      }
    };
  timer = window.setInterval(function () {
    smoothScroll(opts.toT);
  }, opts.delay);

  return _this;
};

function handleFontSizeChange () {
  var noticeHeight = document.querySelector('.notice-info').offsetHeight;
  var fontSize = parseFloat(getComputedStyle(document.querySelector('html')).fontSize);
  var top = parseFloat(getComputedStyle(document.querySelector('.bar-nav ~ .content')).top);
  document.querySelector('.list-block').style.marginTop = ((noticeHeight + 4.3 * fontSize) - top + fontSize) + 'px';
}

if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
  WeixinJSBridge.on('menu:setfont', function (result) {
    console.log('menu:setfont', result);
    WeixinJSBridge.invoke('setFontSizeCallback', result);
    setTimeout(function () {
      handleFontSizeChange();
    }, 10);
  });
}
